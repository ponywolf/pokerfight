-- stats module

local M = {}

local settings = require "com.ponywolf.settings"
local json = require "json"

M.stats = {}
M.session = {}

function M.load()
  M.stats = json.decode(settings:get("stats", "[]"))
end

function M.save()
  settings:set("stats", json.encode(M.stats))
end

function M.clear()
  M.session = {}
end

function M.reset()
  M.stats = {} 
  M.session = {}
  M.save()
end

function M.inc(k,v)
  if tonumber(v) or v == nil then
    v = v or 1
    -- add stat to persistent
    M.stats[k] = (M.stats[k] or 0) + v
    -- add stat to session
    M.session[k] = (M.session[k] or 0) + v
    --settings:save() -- uncomment to save with each inc() probably not a good idea
    return M.stats[k]
  else
    print ("WARNING: Unable to add", v ,"to stat", k)
    return false
  end
end

function M.get(k, session)
  if session then
    if M.session[k] then
      return M.session[k]
    else
      print ("WARNING: No such stat as", k)
      return 0
    end
  else
    if M.stats[k] then
      return M.stats[k]
    else
      print ("WARNING: No such stat as", k)
      return 0
    end
  end
end
function M.getSession(k)
  return M.get(k, true)
end

function M.dump()
  print("--STATS---------------------")
  for k,v in pairs(M.stats) do
    print (k,v)
  end
  print("--SESSION-------------------")
  for k,v in pairs(M.session) do
    print (k,v)
  end
  print ("--STORE--------------------")
  print (settings:get("stats"))
end


local function stat(event)
  if event.phase == "inc" or event.phase == "add" then
    return M.inc(event.stat, event.val or event.num)
  elseif event.phase == "save" then
    M.save()
    return true
  end
end

Runtime:addEventListener("stat",stat)
M.load()

return M