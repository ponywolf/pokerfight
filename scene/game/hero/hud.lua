-- tiled Plugin template

-- Use this as a template to extend a tiled object with functionality
local M = {}
local fx = require "com.ponywolf.ponyfx"
local tiled = require "com.ponywolf.ponytiled"
local button = require "com.ponywolf.plugins.button"
local stats = require "com.ponywolf.ponystats"
local json = require "json"

function M.newLeft()
  local statsDataLeft = json.decodeFile(system.pathForFile("gfx/hudLeft.json"))  -- load from json export
  local instance = tiled.new(statsDataLeft, "gfx")
  instance:centerAnchor()
  instance:extend("label", "decor")

  function instance:setHealth(hp, max)
    self:findObject("hp"):update((hp or "0") .. "/" .. (max or "25"))
    self:findObject("healthBar").xScale = math.max(0.01, (hp or 0) / (max or 25))
  end

  function instance:setMana(mana, max)
    self:findObject("mana"):update((mana or "0") .. "/" .. (max or "25"))
    self:findObject("manaBar").xScale = math.max(0.01, (mana or 0) / (max or 25))
  end

  function instance:setCoin(coin)
    self:findObject("coin"):update(coin or "0")
  end

  return instance
end

function M.newRight()
  local statsDataRight = json.decodeFile(system.pathForFile("gfx/hudRight.json"))  -- load from json export
  local instance = tiled.new(statsDataRight, "gfx")
  instance:centerAnchor()
  instance:extend("label", "decor")

  function instance:setXP(lvl, xp, max)
    self:findObject("lvl"):update("LVL " .. (lvl or "1"))
    self:findObject("xp").xScale = math.min(1,math.max(0.01, (xp or 0) / (max or 25)))
  end

  function instance:setAttack(atk)
    self:findObject("attack"):update(atk or "0")
  end

  function instance:setDefend(def)
    self:findObject("defend"):update((def or "0") .. "%")
  end

  return instance
end

return M