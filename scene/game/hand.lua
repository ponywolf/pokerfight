-- a hand of cards from deck.lua

local libcard = require "scene.game.card"
local libdeck = require "scene.game.deck"
local button = require "com.ponywolf.plugins.button"

-- Define module
local M = {}
local suits = { "hearts", "diamonds", "clubs", "spades" }

function M.new(toDeal)

  toDeal = toDeal or 5
  local half = (toDeal-1) / 2

  local instance = display.newGroup()
  local deck = libdeck.new()

  local function deal()
    deck:reset()
    deck:shuffle()

    instance.values = {}
    for i = 1, toDeal do
      local count = i - 1
      local dealt = deck:pop()
      --print("dealing",dealt)
      local suit, num = deck.decode(dealt)
      local card = libcard.new(suit, num)
      card.value = dealt
      instance:insert(card)
      card:showHold()
      card:translate((32 * (count - half)), math.abs(2 * (half - count)))
      card:rotate((4 * count) - (4 * half))
      instance.phase = "dealing"
      instance.values[i] = dealt
    end
  end

  function instance:keep(toKeep)
    if toKeep then
      for i = 1, self.numChildren do
        for j = 1, #toKeep do
          if self[i].value:find(toKeep[j]) or (toKeep[j] == "*") then
            print("Keeping", self[i].value)
            self[i]:showHold(true)
          end
        end
      end
    end
  end

  function instance:reveal(onComplete)
    for i = 1, self.numChildren do
      transition.from (self[i], { delay = i*66, time = 1, transition = easing.outBounce, 
          onComplete =  function ()
            self[i]:reveal()
            if i == toDeal then
              instance.phase = "dealt"
              if onComplete then onComplete() end
            end
          end})
    end
  end

  function instance:hide(onComplete)
    print("hiding hand")
    for i = 1, toDeal do
      if self[i] then
        self[i]:hide()
        if i == toDeal then
          self[i]:hide(onComplete)
        end
      end
    end
  end

  function instance:draw(onComplete)
    instance.phase = "drawing"
    local function finished()
      -- pipe new cards into hand values
      if onComplete then
        onComplete() 
      end
      onComplete = nil
      instance.phase = "drawn"
    end
    local drawCount = 0
    for i = 1, toDeal do
      self[i].enabled = false
      if not self[i].hold then
        drawCount = drawCount + 1
        local function draw()
          local drawn = deck:pop()
          local suit, num = deck.decode(drawn)
          print("swapping", self.values[i], "for", drawn)
          self[i]:setCard(suit,num)
          self.values[i] = drawn
          self[i]:reveal(finished)
        end
        self[i]:hide(draw)
      end
      self[i]:hideHold()
    end
    if drawCount == 0 then 
      finished()
    end
    return drawCount
  end

  function instance:shuffle()
    for i = self.numChildren,1,-1 do
      display.remove(self[i])
    end
    deal()
  end

  deal()

  return instance
end

return M