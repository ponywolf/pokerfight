local M = {}

local settings = require "com.ponywolf.settings"
local json = require "json"

M.registeredStats = {}
-- Faking some enums here, not sure this is the best way to handle this
M.mathStyle = { add = 'addition', mult = 'multiplication' }
M.stackStyle = { base = 'base', override = 'override', max = 'maximum', sum = 'summation' }
M.order = 0

function M.load()
  M.registeredStats = json.decode(settings:get("derivedStats", "[]"))
end

function M.save()
  settings:set("derivedStats", json.encode(M.registeredStats))
end

function M.reset()
  M.registeredStats = {} 
  M.order = 0
  M.save()
end

-- http://lua-users.org/wiki/CopyTable
local function shallowCopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

-- https://stackoverflow.com/questions/28298358/how-to-remove-all-nil-values-from-an-array
local function cleanNils(t)
  local ans = {}
  for _,v in pairs(t) do
    ans[ #ans+1 ] = v
  end
  return ans
end

function M.register(options)
  if options.statName == nil or options.value == nil or options.deregisterTags == nil then
    print "Missing statName, value, affects, or deregisterEvent in derivedStats.register"
    return
  end
  
  local dStat = {}
  -- required parameters (checked for above)
  -- the name of the stat (strength, health, etc)
  dStat.statName = options.statName
  -- the value of this influence (1, 1.25, 72)
  dStat.value = options.value
  -- the deregister event value that will nullify this 
  dStat.deregisterTags = options.deregisterTags
  if type(dStat.deregisterTags) ~= "table" then
    dStat.deregisterTags = { options.deregisterTags }
  end
  
  M.order = M.order + 1
  
  -- optional parameters
  -- unique id for lookup later
  dStat.id = options.id or ("generated-" .. M.order)
  -- print("id = " .. dStat.id .. " options was " .. (options.id or "empty"))
  -- inserted order
  dStat.order = M.order
  -- whether this is a stat that should be added or multiplied
  dStat.mathStyle = options.mathStyle or M.mathStyle.add
  -- how this stat plays together with other stats of the same mathStyle
  dStat.stackStyle = options.stackStyle or M.stackStyle.additive
  -- the name of the object being affected
  dStat.affects = options.affects or 'global'
  dStat.effectName = options.effectName or ''
  
  M.registeredStats[#M.registeredStats + 1] = dStat
  
  Runtime:dispatchEvent({ name = "statChange", operation = "added", stat = shallowCopy(dStat) })
  Runtime:dispatchEvent({ name = "statChange" .. dStat.statName, operation = "added", stat = shallowCopy(dStat) })
  
  return dStat.id
end

function M.registerUpdate(options)
  if (options.id == nil) then
    print ("ERROR: Missing id for registerUpdate")
    return false
  end
  
  if M.deregister(options) then
    return M.register(options)
  else
    print ("Error: can't register update for id '" .. options.id .. "' because it doesn't exist") 
  end
  
  return false
end

function M.deregister(options)
  if options.id ~= nil then    
    -- print ("looking for "  .. options.id)
    -- print ("#M.registeredStats = " .. #M.registeredStats)
    for i = 1, #M.registeredStats do
--      if M.registeredStats[i] ~= nil then
--        print ("checking "  .. M.registeredStats[i].id)
--      end
      if M.registeredStats[i] ~= nil and M.registeredStats[i].id == options.id then
        Runtime:dispatchEvent({ name = "statChange", operation = "removed", stat = shallowCopy(M.registeredStats[i]) })
        Runtime:dispatchEvent({ name = "statChange" .. M.registeredStats[i].statName, operation = "removed", stat = shallowCopy(M.registeredStats[i]) })
        -- print("Deregistering " .. M.registeredStats[i].effectName)
        
        M.registeredStats[i] = nil
        M.registeredStats = cleanNils(M.registeredStats)
        return true
      end
    end
  end
  
  return false
end


function M.deregisterEvent(event)
  if event == nil or event.tag == nil then
    print ("ERROR: No deregister tag (event.tag) provided in deregister event")
    return
  end
  
  local modified = false
  
  -- print("Deregistering '" .. event.tag .. "' maybe")
  for i = 1, #M.registeredStats do
    if M.registeredStats[i] ~= nil then
      -- print(M.registeredStats[i].effectName)
      -- print(json.prettify(M.registeredStats[i].deregisterTags))
      for j = 1, #M.registeredStats[i].deregisterTags do
        if M.registeredStats[i].deregisterTags[j] == event.tag then
          -- print("FOUND TAG, deregistering id '" .. M.registeredStats[i].id .. "'")
          modified = M.deregister({ id = M.registeredStats[i].id })
          
          if modified then
            i = i - 1
            break
          end
        end
      end
    end
    
    if modified then
      M.deregisterEvent(event)
      break;
    end
  end
  
  -- print(json.prettify(M.registeredStats))
end

-- s = stat name, a = affected object name (can be nil)
function M.get(s, a)
  if s == nil then
    print "ERROR: no key (stat name) provided for derivedstats.get"
    return false
  end
  
  local affects = a or "global"
  local adds = {}
  local mults = {}
  
  for i = 1, #M.registeredStats do
    if M.registeredStats[i].affects == affects and M.registeredStats[i].statName == s then
      if M.registeredStats[i].mathStyle == M.mathStyle.add then
        adds[#adds + 1] = M.registeredStats[i]
      elseif M.registeredStats[i].mathStyle == M.mathStyle.mult then
        mults[#mults + 1] = M.registeredStats[i]
      end
    end
  end
  
  local value = 0
  local baseValue = 0;
  local max, overrideOrder  = -1, -1
  local overrideFound, maxFound  = false, false
    
  for i = 1, #adds do
    -- print (adds[i].effectName .. "-" .. i .. "'s addition (" .. adds[i].value .. ") stackStyle = " .. adds[i].stackStyle)
    
    -- overrides will take the last value registered and ignore everything else
    if adds[i].stackStyle == M.stackStyle.override and adds[i].order > overrideOrder then
      overrideFound = true
      overrideOrder = adds[i].order
      value = adds[i].value
      --print("override, value = " .. value)
    elseif adds[i].stackStyle == M.stackStyle.base then
      baseValue = baseValue + adds[i].value
      -- print("base, baseValue = " .. baseValue)
    -- max style doesn't stack, so we'll use it's greatest value
    elseif not overrideFound and adds[i].value > max and adds[i].stackStyle == M.stackStyle.max then
      maxFound = true
      max = adds[i].value
      -- print("max, max = " .. max)
    -- the default vase is just to add things together
    elseif not overrideFound and adds[i].stackStyle == M.stackStyle.sum then
      value = value + adds[i].value
      -- print("additive, value = " .. value)
    else
      print("ERROR: unknown additon stackStyle '" .. adds[i].stackStyle .. "' for " .. adds[i].effectName)
    end
  end
  
  -- If you have something that doesn't stack we'll check that it's more than calculating results without it
  if not overrideFound then
    if maxFound and max > value then
      value = baseValue + max
      -- print ("max found, new value is: " .. value .. " from " .. baseValue .. " + " .. max)
    else
      value = baseValue + value
    end
  end
  -- print ("Value = " .. value)
  
  local multValue = 1
  max, overrideOrder  = -1, -1
  overrideFound, maxFound  = false, false
  
  for i = 1, #mults do
    -- print (mults[i].effectName .. "-" .. i .. "'s multiplication (" .. mults[i].value .. ") stackStyle = " .. mults[i].stackStyle)
    
    if mults[i].stackStyle == M.stackStyle.override and mults[i].order > overrideOrder then
      overrideFound = true
      overrideOrder = mults[i].order
      multValue = mults[i].value
      -- print("override, multValue = " .. multValue)
    elseif not overrideFound and mults[i].value > max and mults[i].stackStyle == M.stackStyle.max then
      maxFound = true
      max = mults[i].value
      -- print("max, max = " .. max)
    elseif not maxFound and not overrideFound and mults[i].stackStyle == M.stackStyle.sum then
      -- TODO there are a lot of different ways to handle this, going to do multiplication for now
      -- values should be percentage based (0.25 - quarter, 2.0 - double, 1.5 - 150%
      multValue = multValue * mults[i].value
      -- print("additive, multValue = " .. multValue)
    else
      print("ERROR: unknown multiplication stackStyle '" .. mults[i].stackStyle .. "' for " .. mults[i].effectName)
    end
  end
  
  if not overrideFound and maxFound and max > multValue then
    multValue = max
  end
  
  -- print ("MultValue = " .. multValue)
  -- print ("Final Value = " .. (value * multValue))
  
  return value * multValue
end

function M.dump()
  print "========= Derived Stats Dump ========"
  for i = 1, #M.registeredStats do
    print (M.registeredStats[i].effectName .. "-" .. i .. "'s " .. M.registeredStats[i].mathStyle  .. " (" .. M.registeredStats[i].value .. ") stackStyle = " .. M.registeredStats[i].stackStyle)
  end
  print "======================================"
end

if not M.registered then
  M.registered = true
  Runtime:addEventListener("deregister", M.deregisterEvent)
end
M.load()

return M