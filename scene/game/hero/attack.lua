local M = {}

-- TODO
-- I like what you are doing here. What I'll need this function to do 
-- is return a # of strikes and the damage dealt
-- Let's let the hero module do the animation and state change

function M.basicAttack(handRank)
  return handRank.score, handRank.score * handRank.score
end

return M