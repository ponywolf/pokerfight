-- Requirements
local composer = require "composer"
local json = require "json"

local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local snap = require "com.ponywolf.snap"
local stats = require "com.ponywolf.ponystats"
local settings = require "com.ponywolf.settings"
local device = require "com.ponywolf.device"
local fx = require "com.ponywolf.ponyfx"
local alert = require "scene.alert"

-- Vars/Objects local to scene
local scene = composer.newScene()
local menu, ui, musicThumb, soundThumb

local musicVolume = settings:get("musicVolume", 0.5)
local volume = settings:get("volume", 0.75)

function scene:create(event)
  local view = self.view

  view:insert(fx.dim())

  function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    if phase == "released" and (tag == "alert" or tag == "android") then
      snd:play("click")
      if name == "ok" or name == "back" then
        composer.hideOverlay("fade", 133)	
      end
    end
  end

  -- load dialog
  local mapData = json.decodeFile(system.pathForFile("gfx/settings.json"))  -- load from json export
  menu = tiled.new(mapData, "gfx")
  menu:centerAnchor()
  menu:extend("button", "label", "slider", "thumb", "decor")

  view:insert(menu)
  snap(menu)

  musicThumb = menu:findObject("musicThumb")
  soundThumb = menu:findObject("soundThumb")
  local soundButton = menu:findObject("sound")
  local musicButton = menu:findObject("music")

  menu:findObject("build"):update(device.build)

  -- menu listener
  function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    --print(name,phase)
    if phase == "released" then
      if name == "close" then
        composer.hideOverlay()
      end
      if name == "sound" then
        if not event.target:getToggle() then 
          volume = 0
          audio.setVolume(volume)
        else
          volume = 0.75
          audio.setVolume(0.75)				
        end
        musicThumb:set(musicVolume)
        soundThumb:set(volume)
        settings:set("volume", volume)
      end
      if name == "music" then
        if not event.target:getToggle() then 
          musicVolume = 0
          snd:setMusicVolume(musicVolume)
          snd:pauseMusic()
        else
          musicVolume = 0.5
          snd:setMusicVolume(musicVolume)
          snd:resumeMusic()
        end
        musicThumb:set(musicVolume)
        soundThumb:set(volume)
        settings:set("musicVolume", musicVolume)
      end
      if name == "soundThumb" or name == "musicThumb" then
        settings:set("volume", volume)
        settings:set("musicVolume", musicVolume)
      end
      if name == "trash" then
        local function ok()
          local noAds = settings:get("noAds")
          stats:reset()
          settings:clear()
          settings.floor = nil
          settings.xp = nil
          settings:set("noAds", noAds)
          composer.gotoScene("scene.refresh")
        end
        alert.new({ filename = "gfx/confirm.json", dim = true,
            text = "Replay RogueJack2: Poker Knight from the beginging!||This will reset all save data back to defaults. Are you sure?", 
            ok = ok })

      end
      if name == "about" then

      end
      if name == "rate" then
        --review()
      end
      if name == "title" then 
--        local function ok()
--          stats.set("intro",1)
--          fx.irisOut(function ()
--              scene:reset()
--              composer.gotoScene("scene.title")
--            end)
--        end
--        alert.new({ filename = "gfx/confirm.json", dim = true,
--            text = "Abandon game and go back to title screen?", 
--            ok = ok })
      end
      if name == "store" then 
--        composer.showOverlay("scene.store")
      end
    elseif phase == "slid" and name =="musicThumb" then
      musicVolume = event.value			
      snd:setMusicVolume(musicVolume)
      musicButton:setToggle(not (musicVolume > 0))
      if musicVolume > 0 then
        snd:resumeMusic()
      else
        snd:pauseMusic()
      end
    elseif phase == "slid" and name =="soundThumb" then
      volume = event.value
      snd:setVolume(volume)
      soundButton:setToggle(not (volume > 0))
    end
  end
end

function scene:show(event)
  local phase = event.phase
  local params = event.params or {}
  if phase == "will" then
    callback = params.callback

    -- pre-set audio settings
    musicVolume = settings:get("musicVolume", 0.5)
    volume = settings:get("volume", 0.75)
    musicThumb:set(musicVolume)
    soundThumb:set(volume)

    local function finished() snap(menu) end

    transition.from(menu, {y = -menu.designedHeight + display.screenOriginY, time = 666, delay = 0,
        onCancel = finished, onComplete = finished, transition = easing.outBounce 
      })
    snd:play("swoosh")
  elseif phase == "did" then
    Runtime:addEventListener("ui", ui)				
  end
end

function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then
    snd:play("swooshClose")
    Runtime:removeEventListener("ui", ui)				
  elseif phase == "did" then
    if callback then timer.performWithDelay(1, callback) end
  end
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")

return scene