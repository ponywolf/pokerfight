-- bar module

-- define module
local M = {}

-- local/module based variables

function M.new(options)

  -- default options for instance
  options = options or {}
  local x, y = options.x or 0, options.y or 0
  local w,h = options.w or 76, options.h or 6
  local background = options.background or { 0.1 }
  local foreground = options.foreground or { 190/255, 149/255, 92/255, 1 }
  local border = options.border or { 204/255 }
  local align = options.align or "left"
  local value = options.value or 0.0
  align = string.lower(align)

  --  create display group to hold visuals 
  local instance = display.newGroup()
  instance.outline = display.newRect(instance,x, y,w+2,h+2)
  instance.outline:setFillColor(unpack(border))
  instance.backgroundBar = display.newRect(instance,x, y,w,h)
  instance.foregroundBar = display.newRect(instance,x, y,w,h)

  if align == "left" then
    instance.foregroundBar.anchorX = 0
    instance.foregroundBar:translate(-w * 0.5, 0)
  elseif align == "right" then
    instance.foregroundBar.anchorX = 1
    instance.foregroundBar:translate(w * 0.5, 0)
  elseif align == "center" then 
    -- do nothing
  end

  instance.percent = options.percent or 0
  instance.backgroundBar:setFillColor(unpack(background))
  instance.foregroundBar:setFillColor(unpack(foreground))  

  function instance:set(percent, speed)
    self.percent = percent or 0
    self.percent = math.min(1,math.max(0, self.percent))
    if speed == 0 then
      self.foregroundBar.xScale = math.max(0.001, self.percent)
    else
      transition.to(self.foregroundBar, { xScale = math.max(0.001, self.percent), time = speed or 500, transition=easing.outQuad })
    end
    return self.percent
  end
  instance:set(value,0)

-- return insantnce
  return instance
end

-- return module
return M