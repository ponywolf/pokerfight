-- ponywolf pixel particles

-- Date: Dec 28, 2019
-- Updated: Dec 28, 2019

local color = require "com.ponywolf.ponycolor"

local M = {}
local stage = display.getCurrentStage()
local json = require "json"

local maxParticles = 64
local maxFrames = 180
local bounce = 0.75
local friction = 0.975
local minimum = 0.25
local gravity = 3

function M.new(x, y, options)
  options = options or {}
  local instance = options.partent and display.newRect(options.partent, x, y, 16, 16) or display.newRect(x, y, 16, 16)
  local particles = {}

  local function nextParticle(p)
    for i = maxParticles, 1, -1 do
      if p[i] ==nil then
        return p[i]
      end
    end
    return false
  end
  
  function instance:spurt(startAngle, endAngle, startSpeed, endSpeed, floor)
    local angle = math.random(startAngle, endAngle)
    local speed = math.random(math.floor(startSpeed * 256), math.floor(endSpeed * 256))/256
    local dx, dy = math.cos(math.rad(angle)) * speed, math.sin(math.rad(angle)) * speed
    local p = nextParticle(particles)
    if p then
      p = display.newRect(self.parent, self.x, self.y, 1, 1)
      p.ax, p.ay = p.x, p.y
      p:setFillColor(unpack(color.hex2rgb(options.color or "FFFFFF")))
      p.dx, p.dy = dx, dy
      p.floor = floor or options.floor or 99999999
    end
  end

  local function enterFrame()
    for i = 1, #particles do
      particles[i].dx = particles[i].dx * friction 
      particles[i].dy = particles[i].dy * friction
      -- frames
      particles[i].frame = (particles[i].frame or 0) + 1
      -- bounce?
      local distFromFloor = particles[i].floor - (particles[i].ay - instance.y)
      if distFromFloor < -minimum then
        particles[i].ay = instance.y + particles[i].floor
        particles[i].dy = -bounce * (particles[i].dy + 2 * (options.gravity or gravity))
      end
      -- move w/ gravity & friction
      particles[i].dx = (math.abs(particles[i].dx) > minimum) and particles[i].dx or 0
      particles[i].dy = (math.abs(particles[i].dy) > minimum) and particles[i].dy or 0
      particles[i].ax, particles[i].ay = particles[i].ax + particles[i].dx, particles[i].ay + particles[i].dy + (options.gravity or gravity)
      -- set on pixel
      particles[i].x, particles[i].y = math.floor(particles[i].ax + 0.5), math.floor(particles[i].ay + 0.5)
      -- delete after maxframes
      if particles[i].frame > maxFrames then
        display.remove(particles[i])
        particles[i] = nil
      elseif particles[i].frame > maxFrames - 60 then 
        particles[i].alpha = (maxFrames - particles[i].frame) / 60
      end
    end
  end

  function instance:finalize()
    Runtime:removeEventListener("enterFrame", enterFrame)
  end

  instance.isVisible = false
  Runtime:addEventListener("enterFrame", enterFrame)
  return instance
end

function  M.explode(x, y, options)
  local instance = display.newGroup()
  local spark = M.new(display.contentCenterX, display.contentCenterY, { parent = instance })
  for i = 1, 24 do
    spark:spurt(270-30, 270+30, 4, 5, math.random(12,20))
  end
  return instance
end

return M