-- Module/class for enemy

local M = {}

-- Use this as a template to build an in-game enemy

local composer = require "composer"
local json = require "json"

local fx = require "com.ponywolf.ponyfx"
local pfx = require "scene.game.pokerfx"
local snd = require "com.ponywolf.ponysound"
local csv = require "com.ponywolf.csv"

local attacks = require "scene.game.enemy.attack"
local equipment = require "scene.game.animations"
local emotes = require "scene.game.hero.emotes"

local gearSheetOptions = { width = 24, height = 24, numFrames = 100 }

-- data
local enemyData = csv.load("enemies.csv")

local function getRandom()
  -- TODO add chance, floors, etc
  return enemyData:find(enemyData.ids[math.random(#enemyData.ids)])
end

function M.new()
  -- Get the current scene
  local scene = composer.getScene("scene.game")
  local class = "random"

  local enemy
  if class == "random" then
    enemy = getRandom()
  else
    enemy = enemyData:find(class)
  end

  -- load frame animations
  local enemySheetOptions = { width = 20, height = 20, numFrames = 27 }
  local enemySheet = graphics.newImageSheet("gfx/" .. enemy.spriteSheet .. ".png", enemySheetOptions)
  local sequences = require("scene.game.animations")[enemy.animation]

  -- build enemies's view
  local instance = display.newGroup()
  instance.sprite = display.newSprite(enemySheet, sequences)
  instance:insert(instance.sprite)
  instance.sprite.anchorY = 1
  instance.sprite:translate(0,8)
  instance.sprite:setSequence("idleSouth")
  instance.sprite:play()
  fx.breath(instance.sprite)

  -- load emotes
  instance.emote = emotes.new(instance, 0, -30)
  scene.hud:insert(instance.emote)

  --shadow
  local shadow = display.newImage(instance, "gfx/shadow.png")
  shadow:translate(0,7)
  shadow:toBack()
  shadow.alpha = 0.5

  -- vars
  instance.name = enemy.id
  instance.type = "enemy"
  instance.phase = "idle"
  instance.movement = enemy.movement
  instance.isAttackable = true
  instance.hp = enemy.hp
  instance.maxhp = instance.hp

  -- movement
  local directions = { up = "North", down = "South", left = "West", right = "East" }
  local movement = { up = { y = -1 }, down = { y = 1 }, left = { x = -1 }, right = { x = 1 } }
  local moves = { "up" , "down", "left", "right"}

  function instance:setSequence(sequence)
    if not (self.sequence == sequence) or self.sequence == nil then
      self.sequence = sequence or self.sequence or "idleSouth"
      self.equipSequence = self.sequence:gsub("walk",""):gsub("idle",""):lower()
      self.sprite:setSequence(self.sequence)
      instance.sprite:play()
    end
  end
  instance:setSequence()

  function instance:face(obj) -- face toward an object
    if obj and obj.x and obj.y then
      if math.abs(obj.x-self.x) >= math.abs(obj.y-self.y) then
        if obj.x > self.x then
          self:setSequence("idleEast")
        else
          self:setSequence("idleWest")
        end
      else
        if obj.y > self.y then
          self:setSequence("idleSouth")
        else
          self:setSequence("idleNorth")
        end
      end
    end
  end

  function instance:move()
    if self.isDead then return end

    -- set animation
    local newSequence
    local points = scene.points
    local hx, hy = scene.hero.mx or self.mx, scene.hero.my or self.my
    local ox, oy = self.mx, self.my
    local dx, dy = hx - self.mx, hy - self.my
    local direction

    -- movement styles
    if self.movement == "hunt" then
      -- hunt player better
      if math.abs(dx) > math.abs(dy) then -- farther away left to right
        if dx < 0 then
          direction = "left"
        elseif dx > 0 then
          direction = "right"
        end
      else
        if dy < 0 then
          direction = "up"
        elseif dy > 0 then
          direction = "down"
        end
      end
    else
      direction = moves[math.random(1,4)]
    end

    local dir = directions[direction]
    if dir and self.phase == "idle" then
      self.phase = "moving"
      newSequence = "walk" .. dir
      self:setSequence(newSequence)
      -- move
      local x, y = self.x, self.y
      local mx, my = self.mx, self.my
      -- do we have nav points?
      mx = mx + (movement[direction].x or 0)
      my = my + (movement[direction].y or 0)
      if not scene.map.inside[mx][my] then
        print (enemy.name, "has no move!")
        mx, my = self.mx, self.my
      end

      -- check for collisions
      if mx == hx and my == hy then
        -- attacking hero
        print (enemy.name, "is attacking hero!")
        mx, my = ox, oy -- revert
        scene.queue = self
      end

      local nx, ny = scene.map.mapToContent(mx,my)
      local time = 166

      -- back to idle
      local function finished()
        self.mx, self.my = mx, my -- for sure set these
        self.phase = "idle"
        newSequence = self.sequence:gsub("walk", "idle")
        self:setSequence(newSequence)
        --pfx.popText(self, self.mx .. "-" .. self.my)
        return time
      end

      if not (mx == self.mx and my == self.my) then -- we found a move
        --slide
        newSequence = self.sequence:gsub("walk", "idle")
        transition.to(self, { x = nx, y = ny, time = time, transition = easing.outQuad, onComplete = finished })
      else
        finished()
      end
    end
  end

  function instance:hurt(num)
    num = num or 1
    self.hp = self.hp - num
    pfx.popText(self, "-" .. num)
    if self.hp <= 0 then
      self:die()
    else
      fx.flash(self.sprite)
    end
  end

  function instance:die(fast)
    fx.flash(self.sprite)
    self.sprite:setSequence("die")
    self.sprite:play()
    self.isDead = true
    self:toBack()
  end

  function instance:fight(damage)
    local function attacked()
      snd:play("attacked")
      local min, max = (enemy.minDamage or 1), (enemy.maxDamage or enemy.minDamage or 1)
      if scene.hero:hurt((min == max) and min or math.random(min, max)) then
        --hero is dead...
      else
        -- walk
        scene:toggle("w")
      end
    end

    self.sprite._y = self.sprite.y
    self.sprite:play("attack")
    transition.from(self.sprite, { y=self.sprite._y-16, transition=easing.outBounce, time=500, onComplete = attacked })
  end

  function instance:setStats()
    print("setting stats", json.prettify(enemy))
    local stats = scene.enemyStats
    local min, max = (enemy.minDamage or 1), (enemy.maxDamage or enemy.minDamage or 1)
    stats:setDamage(min, max)
    stats:setHealth(self.hp, self.maxhp)
    stats:setBonus(enemy.bonus or "none", enemy.bonusValue)
  end

  local function enterFrame(event)
    -- Do this every frame
    shadow.isVisible = instance.isVisible

    -- set map x,y
    instance.mx, instance.my = math.floor(0.5+instance.x/16),math.floor(0.5+instance.y/16)
    instance.room = scene.map.findRoom(instance.mx, instance.my) or 0

    -- This stuff won't happen if things are paused/dead
    if instance.isPaused or scene.isPaused or instance.isDead then return false end

  end

  function instance:finalize()
    -- On remove, cleanup instance, or call directly for non-visual
    Runtime:removeEventListener("enterFrame", enterFrame)
  end

-- Add a finalize listener (for display objects only, comment out for non-visual)
  instance:addEventListener("finalize")

-- Add our enterFrame listener
  Runtime:addEventListener("enterFrame", enterFrame)

  return instance
end

return M