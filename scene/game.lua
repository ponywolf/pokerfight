-- Requirements
local composer = require "composer"
local json = require "json"

local fx = require "com.ponywolf.ponyfx"
local tiled = require "com.ponywolf.ponytiled"
local snap = require "com.ponywolf.snap"
local csv = require "com.ponywolf.csv"
local color = require "com.ponywolf.ponycolor"
local snd = require "com.ponywolf.ponysound"
local settings = require "com.ponywolf.settings"
local stats = require "com.ponywolf.ponystats"
local device = require "com.ponywolf.device"

-- game specific libs
local worlds = require "scene.game.world"
local heros = require "scene.game.hero"
local hands = require "scene.game.hand"
local taps = require "scene.game.tap"
local analysers = require "scene.game.analyser"
local pfx = require "scene.game.pokerfx"
local dungeons = require "scene.game.dungeon"
local counts = require "scene.game.count"
local choices = require "scene.game.choose"
local spells = require "scene.game.spells"

-- UI libs
local alert = require "scene.alert"

-- Variables local to scene
local scene = composer.newScene()
local dungeon, heart, shield, sword, coin, gear, inventory, showMenu, spellBook

function scene:create(event)
  local view = self.view -- add display objects to this group
  local params = event.params or {}

  -- create an hud layer on top
  self.hud = display.newGroup()
  view:insert(self.hud)
  local hud = self.hud

  -- load world
  scene:loadWorld()
  scene.world:toBack()

  -- dialog text
  local dialog = require("scene.game.dialog").new()
  hud:insert(dialog)

  -- tap anywhere to draw
  scene.draw = taps.new()
  hud:insert(scene.draw)

  if not device.isDesktop then
    local keyData = json.decodeFile(system.pathForFile("gfx/keys.json")) -- load from json export
    scene.keys = tiled.new(keyData, "gfx")
    scene.keys:extend("button", "swipeKey")
    scene.keys:centerAnchor()
    hud:insert(scene.keys)
    scene.keys.isVisible = true

    -- swipe or not
    local function swipeOn()
      settings:set("swipe", true)
      scene.keys:findLayer("keys").isVisible = false
      scene.keys:findObject("swipe").enabled = true
    end
    local function swipeOff()
      settings:set("swipe", false)
      scene.keys:findLayer("keys").isVisible = true
      scene.keys:findObject("swipe").enabled = false
    end
    if settings:get("swipe") then
      swipeOn()
    else
      swipeOff()
    end
  end

  -- on screen cards
  scene.hand = hands.new()
  scene.handRank = analysers.default
  hud:insert(scene.hand)
  scene.hand.isVisible = false

  -- battle stats ui
  scene.heroStats = require("scene.game.hero.stats").new()
  hud:insert(scene.heroStats)

  scene.enemyStats = require("scene.game.enemy.stats").new()
  hud:insert(scene.enemyStats)

  -- all the time HUD
  scene.hudLeft = require("scene.game.hero.hud").newLeft()
  hud:insert(scene.hudLeft)
  scene.hudRight = require("scene.game.hero.hud").newRight()
  hud:insert(scene.hudRight)

  -- corner buttons
  gear = display.newImage(view, "gfx/gearButton.png") -- drop these two on top of the hud layer
  inventory = display.newImage(view, "gfx/itemsButton.png") -- drop these two on top of the hud layer
  showMenu = display.newImage(hud, "gfx/menuButton.png")
  spellBook = display.newImage(hud, "gfx/spellBook.png")
  gear.name = "gear"
  inventory.name = "items"
  showMenu.name = "menu"
  spellBook.name = "spells"

  -- enable them
  local button = require "com.ponywolf.plugins.button"
  gear = button.new(gear)
  inventory = button.new(inventory)
  showMenu = button.new(showMenu)
  spellBook = button.new(spellBook)

  -- chooser
  scene.choose = require("scene.game.choose").new()
  scene.choose.isVisible = false
  view:insert(scene.choose)

  -- spells
  scene.spells = require("scene.game.spells").new()
  scene.spells.isVisible = false
  view:insert(scene.spells)

  -- status text
  local status = require("scene.game.status").new()
  hud:insert(status)

  -- tap helper
  scene.tapHelp = display.newImage(hud, "gfx/tapHelp.png")
  fx.bounce3D(scene.tapHelp)
  scene.tapHelp.isVisible = false

  -- update stats
  scene.hero:updateStats()
end

function scene:continue(event)
  -- everything that needs to happen when a user "continues"
end

function scene:reset(event)
  -- everything that needs to happen when a scene resets or "dies"
  settings.session = {}
end

function scene:pause()
  -- everything that needs to happen when the physics is paused
  self.isPaused = true
end

function scene:resume()
  -- everything that needs to happen when the physics is resumed
  self.isPaused = false
end


local navSheetOptions = { width = 16, height = 16, numFrames = 10 }
local navSheet = graphics.newImageSheet("gfx/nav.png", navSheetOptions)

function scene:loadWorld(params)
  -- build a world
  local data = dungeons.new()
  local walls = graphics.newImageSheet("gfx/walls.png", fx.extrudedTileset(16,16,8,4,1,2))
  self.world = worlds.new()
  self.map = data.render({sheet = walls})
  self.world:insert(self.map)
  self.nav = display.newGroup()
  self.map:insert(self.nav)
  self.game = display.newGroup()
  self.map:insert(self.game)
  self.view:insert(self.world)
  self.hero = heros.new()
  self.game:insert(self.hero)
  scene.world:centerObj(self.hero)

  -- place hero

  local start = data.room[1]
  local finish = data.room[#data.room]
  if math.random(2) == 1 then
    start, finish = finish, start
  end

  local spawn = {}
  local timeout = 1000
  repeat
    timeout = timeout - 1
    local open = true
    spawn.x = math.random(start.left, start.left + start.width)
    spawn.y = math.random(start.top, start.top + start.height)
  until scene.map.inside[spawn.x][spawn.y]
  self.hero.x, self.hero.y = self.map.mapToContent(spawn.x, spawn.y)

  -- drop monsters in
  local enemies = require "scene.game.enemy"
  self.enemy = {}
  for i = 1, 5 do
    local room = data.room[math.random(#data.room)]
    local spawn = {}
    local timeout = 1000
    repeat
      timeout = timeout - 1
      local open = true
      spawn.x = math.random(room.left, room.left + room.width)
      spawn.y = math.random(room.top, room.top + room.height)
      if not scene.map.inside[spawn.x][spawn.y] then
        open = false
      end
      for i = 1, #self.enemy do
        if self.enemy[i].mx == spawn.x and self.enemy[i].my == spawn.y then
          open = false
        end
      end
    until open or timeout <= 0 -- not on top of another enemy
    local num = #self.enemy+1
    self.enemy[num] = enemies.new()
    self.game:insert(self.enemy[num])
    self.enemy[num].x, self.enemy[num].y = self.map.mapToContent(spawn.x, spawn.y)
    self.enemy[num].mx, self.enemy[num].my = spawn.x, spawn.y
  end

end

function scene:moved() -- hero has moved, move enemies
  local moved, wait = 0, 0
  local hero = self.hero
  for i = 1, #self.enemy do
    if self.enemy[i].room == self.hero.room then
      moved = moved + 1
      wait = math.max(wait, self.enemy[i]:move() or 0)
    end
  end
  if moved > 0 then
    transition.to(self, { time = wait, onComplete = function ()
          if self.queue then -- attacks in the queue
            hero.attacking = self.queue
            print ("Attacked by", hero.attacking.name)
            hero:face(hero.attacking)
            hero.attacking:face(hero)
            scene:toggle("deal")
          else
            self.hero.phase = "idle"
          end
        end })
  else
    self.hero.phase = "idle"
  end
end

function scene:resetWorld()

end

function scene:nextFloor()
  local floor = settings:get("floor",1)

end

-- ui listener
local function ui(event)
  local phase, name, tag = event.phase, event.buttonName or "none", event.tag
  --print(name,phase)
  if phase == "released" then
    if name == "gear" or name == "items" or name == "spells" then
      composer.showOverlay("scene.inventory", { isModal = true, params = { inventory = name }})
    elseif name == "tap" and event.tag == "game" then
      print("Draw tap")
      scene:toggle("draw")
    elseif name == "menu" then
      composer.showOverlay("scene.settings")
    end
  end
end

function scene:stats(mode)
  if mode == "open" and not (self.heroStats.isVisible or self.enemyStats.isVisible) then
    self.heroStats:open()
    self.enemyStats:open(self.hero.attacking.id)
    self.hero.attacking:setStats()
    self.hudRight.isVisible = false
    self.hudLeft.isVisible = false
  elseif mode == "close" and (self.heroStats.isVisible or self.enemyStats.isVisible) then
    self.heroStats:close()
    self.enemyStats:close()
    self.hudRight.isVisible = true
    self.hudLeft.isVisible = true
  end
end

function scene:toggle(mode)
  print("toggle", mode)
  if mode == "deal" then
    stats.inc("hands")
    self.spells:open()
    gear:disable()
    inventory:disable()
    spellBook:disable()
    --self.keys.isVisible = false
    local function postDeal()
      print("preRanking...")
      local handRank = analysers.rank(self.hand.values)
      -- keep dealt hand if it's any good
      if handRank.keep then
        self.hand:keep(handRank.keep)
      end
      print(json.prettify(handRank))
      self.draw:activate()
    end
    self:pause()
    scene:stats("open")
    self.hand:shuffle()
    self.hand.isVisible = true
    self.hand:reveal(postDeal)
    self.hero.phase = "fighting"
    snd:play("start")
  elseif mode == "reshuffle" then
    local function postHide()
      self:toggle("deal")
    end
    self.hand:hide(postHide) -- onComplete do defeated()
  elseif mode == "draw" then
    if self.hand.phase == "dealt" then -- cards are just dealt
      local function rank()
        print("Ranking...")
        local handRank = analysers.rank(self.hand.values)
        -- show final hand only if it deals damage
        if handRank.score > 0 then
          local finalHand = pfx.rankText(handRank.name)
          snap(finalHand)
          finalHand:translate(0,24)
          self.hud:insert(finalHand)
        end
        -- call attack
        self.hero:fight(handRank)
        print(json.prettify(handRank))
        self.draw:deactivate()
      end
      self.hand:draw(rank) -- onComplete do rank()
      self.spells:close()
    else
      print("Can't draw cards")
    end
    snd:play("click")
  elseif mode == "win" then -- you won
    local function postVictory()
      local function walk()
        self:toggle("walk")
      end
      self.hand:hide(walk) -- onComplete do defeated()
    end
    local victory = pfx.victoryText("VICTORY", postVictory)
    snap(victory)
    self.hud:insert(victory)
    self.hero:addXP(1)
    snd:play("levelup")
  else --default to walking
    self:stats("close")
    self.draw:deactivate()
    self.hand.isVisible = false
    --self.keys.isVisible = true
    self:resume()
    gear:enable()
    inventory:enable()
    spellBook:enable()
    self.hero.phase = "idle"
    self.hero.attacking = nil
    self.queue = nil
  end
end

local function resize(event)
  -- move, size all hud and game objects
  snap(scene.hand, "bottomcenter", 36)
  snap(scene.draw, "bottomcenter", 8)
  snap.safe(scene.heroStats,"topleft", 10)
--  scene.heroStats.x = display.contentCenterX
  scene.heroStats.opened = scene.heroStats.x
  snap.safe(scene.enemyStats,"topright", 0)
--  scene.enemyStats.x = display.contentCenterX
  scene.enemyStats.opened = scene.enemyStats.x
  scene.enemyStats:translate(0, scene.heroStats.height)

  gear.anchorX, gear.anchorY = 0, 1
  inventory.anchorX, inventory.anchorY = 1, 1
  spellBook.anchorX, spellBook.anchorY = 0, 0
  showMenu.anchorX, showMenu.anchorY = 1, 0
  gear.x, gear.y = display.screenOriginX, display.actualContentHeight + display.screenOriginY
  inventory.x, inventory.y = display.actualContentWidth + display.screenOriginX, display.actualContentHeight + display.screenOriginY
  spellBook.x, spellBook.y = display.screenOriginX, display.screenOriginY
  showMenu.x, showMenu.y = display.actualContentWidth + display.screenOriginX, display.screenOriginY

  -- split HUD
  snap(scene.hudLeft, "topleft", 2)
  snap(scene.hudRight, "topright", 2)
  scene.hudLeft:translate(0,spellBook.height)
  scene.hudRight:translate(0,spellBook.height)

  snap.safe(scene.tapHelp, "lowerRight", 16)
  scene.tapHelp:translate(-32,0)
  snap.safe(scene.keys, "bottomcenter", 8)
end


local function enterFrame(event)
  -- Do these every frame regardless of pause
  local hero = scene.hero
  if hero.attacking then
    scene.world:centerObj(hero.attacking, true)
  else
    scene.world:centerObj(hero, true)
  end

  -- Do these only when not paused
  if scene.isPaused == true then return false end
end

function scene:refresh(event)
  resize()
end

function scene:show(event)
  local phase = event.phase
  local params = event.params or {}
  if phase == "will" then
    display.setDefault("background", 0)
    resize()
    fx.irisIn()
  elseif phase == "did" then
    Runtime:addEventListener("ui", ui)
    Runtime:addEventListener("enterFrame", enterFrame)
    Runtime:addEventListener("resize", resize)
    scene:toggle("walk")
  end
end

function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then
    Runtime:removeEventListener("ui", ui)
    Runtime:removeEventListener("enterFrame", enterFrame)
    Runtime:removeEventListener("resize", resize)
  elseif (phase == "did") then

  end
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")

return scene