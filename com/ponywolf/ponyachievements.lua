local M = {}

local settings = require "com.ponywolf.settings"
local device = require "com.ponywolf.device"
local csv = require "com.ponywolf.csv"
local json = require "json"

M.achievements = {}
local data = csv.load("achievements.csv")
local steamworks
if device.isSteam then
  steamworks = require "plugin.steamworks"
end

function M.load()
  settings:load()
  M.achievements = json.decode(settings:get("achievements", "[]"))
end

function M.doAchievement(id, title, description)
  print("Unlocked achievement:", id, title, description)
  -- device specific achievements
  if device.isSteam and steamworks then
    steamworks.setAchievementUnlocked(id)
  else
    -- another platform goes here
  end
  Runtime:dispatchEvent({name = "unlocked", id = id, title = title, description = description})
end

function M.complete(id)
  print("Achievements completing:", id)
  local achievement = data:find(id)

  if achievement.id then -- yep, we're an achievement
    M.achievements[id] = true
    return true
  end
end

function M.test(stat,value)
  print("Achievements testing:", stat, value)
  --print(json.encode(M.achievements))
  local achievement = data:findAll("stat", stat)
  --print("Looking up:", json.encode(achievement))
  if achievement then
    for i = 1, #achievement do
      local locked = not M.achievements[achievement[i].id]
      if value >= achievement[i].val and locked then -- we've hit the threshold and it's not alrerady unlocked
        M.achievements[achievement[i].id] = true -- mar
        M.save()
        M.doAchievement(achievement[i].id, achievement[i].title, achievement[i].description)
      end
    end
  else
    print ("WARNING: No achievement associated with ", stat)
    return false
  end
end

function M.save()
  settings:set("achievements", json.encode(M.achievements))
  settings:save()
end

function M.clear()
  M.achievements = {}
end

function M.reset()
  M.achievements = {}
  M.save()
end

M.load()
return M