-- Sliding dungeons

-- From scratch lua algrothims for procedual tile maps
local color = require "com.ponywolf.ponycolor"
local json = require "json"

local M = {}

local wallMap = {
  w = 2,
  e = 4,
  s = 5,
  n = 13,
  ew = 3,
  ns = 9,
  sw = 6,
  nw = 14,
  se = 8,
  ne = 16,
  sew = 7,
  ens = 12,
  nws = 10,
  new = 15,
  nsew = 11,
}

local dirs = { "n", "s", "e", "w" }
local delta = { n = { y = -1 }, s = { y = 1 }, e = { x = -1 }, w = { x = 1 } }
local eatn = { "e", "a", "n" }

local function array(w,h)
  local data = {}
  for x = 0, (w or 1) do
    data[x] = {}
    for y = 0, (h or 1) do
      data[x][y] = 0
    end
  end
  return data
end

local function dump(data, w, h)
  for y = 1, h do
    local str = ""
    for x = 1, w do
      if data[x][y] == 1 then
        str = str .. "[ ]"
      else
        str = str .. ": :"
      end
    end
    print(str)
  end
end

local function cleared(data, w, h)
  local total = 0
  for y = 1, h do
    for x = 1, w do
      total = total + ((data[x][y] == 0) and 1 or 0)
    end
  end
  return total / (w*h)
end

local function isClear(data, x, y, w, h, margin)
  margin = margin or 0
  for i = x, w+x do
    for j = y, h+y do
      if (data[i] and data[i][j]) then
        if data[i][j] == 0 then return false end
        if i <= margin then return false end
        if j <= margin then return false end
        if i >= data.w-margin then return false end
        if j >= data.h-margin then return false end
      else
        return false
      end
    end
  end
  print (x,y,w,h,"is Clear")
  return true
end

local function rect(data, x, y, w, h, val)
  val = val or 1
  for i = x, w+x do
    for j = y, h+y do
      if (data[i] and data[i][j]) then
        data[i][j] = val or 1
      end
    end
  end
end

local function box(data, x, y, w, h, val)
  val = val or 1
  for i = x, w+x do
    for j = y, h+y do
      local edge = (i==x or j==y or i==w+x or j==y+h)
      if (data[i] and data[i][j] and edge) then
        data[i][j] = val or 1
      end
    end
  end
end

local function column(data, x, y, w, h)
  rect(data, x+1, y+1, 1, 1, 1)
  rect(data, x+w-3, y+h-3, 1, 1, 1)
  rect(data, x+1, y+h-3, 1, 1, 1)
  rect(data, x+w-3, y+1, 1, 1, 1)
end

local function tower(data, x, y, w, h)
  rect(data, x, y, 1, 1, 0)
  rect(data, x+w-1, y, 1, 1, 0)
  rect(data, x, y+h-1, 1, 1, 0)
  rect(data, x+w-1, y+h-1, 1, 1, 0)
end

local function connect(data, x1, y1, x2, y2, val)
  local dx = x1 - x2
  local dy = y1 - y2
  local xd = dx < 0 and 1 or -1
  local yd = dy < 0 and 1 or -1

  if math.abs(dx) > math.abs(dy) then
    for i = x1, x2, xd do
      data[i][y1] = val or 0
    end
    for i = y1, y2, yd do
      data[x2][i] = val or 0
    end
  else
    for i = y1, y2, yd do
      data[x2][i] = val or 0
    end
    for i = x1, x2, xd do
      data[i][y1] = val or 0
    end
  end
end

local function add(data, rooms, x, y, w, h, margin)
  print("trying to adding room", #rooms + 1, margin)
  if isClear(data, x, y, w, h, margin) then
    -- find a good "center"
    rect(data, x, y, w, h, 0)
    --local cx, cy = math.random(x+1, x+w-2), math.random(y+1, y+h-2)
    local cx, cy = math.floor(0.5+x+w/2), math.floor(0.5+y+h/2)
    rooms[#rooms+1] = { x = cx, y = cy, top = y, left = x, width = w, height = h, num = #rooms+1}
    print("added", json.encode(rooms[#rooms]))
    return true
  end
end

function M.new(options)
  options = options or {}

  -- seed
  local seed = options.seed or os.time()
  print("SEED:", seed)
  math.randomseed(seed)

  -- defaults
  local w,h = options.width or 64, options.height or 64
  local roomW, roomH = options.roomWidth or 7, options.roomHeight or 7 -- max room w/h
  local minW, minH = options.minWidth or 4, options.minHeight or 4 -- min room w/h
  local margin = options.margin or 3 -- spacing around map
  local spacing = 2 + (options.spacing or 2) -- minimum spacing between rooms
  local allowEven = options.allowEven
  local square = options.square
  local targetRooms = options.rooms or 24
  local method = options.method or "eat"
  local digPercent = options.digPercent or 0.333

  -- public variables
  local map = {}
  map.width, map.height = w, h
  map.data = array(w,h)
  map.data.w = w
  map.data.h = h
  map.room = {}
  map.inside = array(w,h)

  local timeout = 10000
  local tries = 10
  local roomNum = 1 -- starting room
  local roomsX, roomsY = math.floor((w+margin)/roomW)-1, math.floor((h+margin)/roomH)-1

  rect(map.data, 0, 0, w, h) -- fill

  if method == "eat" then
    local current, max = 1, 1
    -- build first room
    --local rx, ry = math.random(margin, w-roomW-margin),math.random(margin, h-roomH-margin)
    local rw, rh = math.random(minW,roomW), math.random(minH,roomH)
    local rx, ry = math.floor(w/2 - rw/2), math.floor(h/2 - rh/2)
    add(map.data, map.room, rx, ry, rw, rh)
    -- start building
    repeat
      timeout = timeout - 1
      local added
      -- choose a direction
      for j = 1, 4 do
        added = false
        local dir = dirs[math.random(#dirs)]
        local kind = eatn[math.random(#eatn)]
        if not map.room[current][dir] then
          if kind == "e" then -- extend
            -- build an new x,y,w,h starting from the previous map
            tries = 10
            rx, ry = map.room[current].x,map.room[current].y
            repeat
              tries = tries - 1
              rx = rx + (delta[dir].x or 0)
              ry = ry + (delta[dir].y or 0)
            until isClear(map.data, rx, ry, rw, rh, margin) or tries <= 0
            if tries > 0 then
              add(map.data, map.room, rx, ry, rw, rh, margin)
              map.room[current][dir] = true
              added = true
            end
          elseif kind == "a" then
            -- build an new x,y,w,h starting from the previous map
            tries = 10
            rx, ry = map.room[current].x,map.room[current].y
            rw, rh = math.random(minW,roomW), math.random(minH,roomH)
            repeat
              tries = tries - 1
              rx = rx + (delta[dir].x or 0)
              ry = ry + (delta[dir].y or 0)
            until isClear(map.data, rx, ry, rw, rh, margin) or tries <= 0
            if tries > 0 then -- move one more space
              rx = rx + (delta[dir].x or 0)
              ry = ry + (delta[dir].y or 0)
              add(map.data, map.room, rx, ry, rw, rh)
              local last = #map.room
              connect(map.data, map.room[current].x, map.room[current].y, map.room[last].x, map.room[last].y)
              map.room[current][dir] = true
              added = true
            end
          elseif kind == "t" then
            -- build an new x,y,w,h starting from the previous map
            rx, ry = map.room[current].x,map.room[current].y
            rw, rh = math.random(minW,roomW), math.random(minH,roomH)
            tries = 10
            repeat
              tries = tries - 1
              rx = rx + (delta[dir].x or 0) * math.floor(rw * 1.5 )
              ry = ry + (delta[dir].y or 0) * math.floor(rh * 1.5 )
            until isClear(map.data, rx, ry, rw, rh, margin) or tries <= 0
            if tries > 0 then -- drop it
              add(map.data, map.room, rx, ry, rw, rh, margin)
              local last = #map.room
              connect(map.data, map.room[current].x, map.room[current].y, map.room[last].x, map.room[last].y)
              map.room[current][dir] = true
              added = true
            end
          elseif kind == "n" then
            -- skip building that room
            map.room[current][dir] = true
            added = true
          end
        end
      end
      current = current + 1
      if current > #map.room then
        current = 1
      end
    until (#map.room >= targetRooms) or (timeout < 0)
    for i = 1, #map.room do
      local y, x = map.room[i].top, map.room[i].left
      local rw, rh = map.room[i].width, map.room[i].height
      if math.random() > 0.75 and rw < roomW and rh < roomH  then
        tower(map.data, x-1, y-1, rw+2, rh+2)
        map.room[i].top, map.room[i].left = y-1, x-1
        map.room[i].width, map.room[i].height = rw+2, rh+2
        map.room[i].hasTower = true
      end
    end
  elseif method == "roguejack" then
    repeat
      map = {}
      map.width, map.height = w, h
      map.data = array(w,h)
      map.data.w = w
      map.data.h = h
      map.room = {}
      map.inside = array(w,h)
      tries = tries - 1
      timeout = 10000
      rect(map.data, 0, 0, w, h) -- fill
      local i = 1
      repeat
        timeout = timeout - 1
        local rw, rh, dx, dy
        repeat
          timeout = timeout - 1
          if (timeout < 0) then print ("Map timeout:", 10 - tries) end
          if (minW < roomW) and (minH < roomH) then
            rw = math.random(minW,roomW)
            rh = math.random(minH,roomH)
          else
            rw, rh = roomW, roomH
          end
          if math.random(1) == 0 then
            dx, dy = math.min(w - rw - 4, (dx or 1) + 3), math.random(h - rh - 4) + 3
          else
            dx, dy = math.random(w - rw - 4) + 3, math.min(h - rh - 4, (dy or 1) + 3)
          end
        until isClear(map.data, dx - spacing, dy - spacing, rw + (spacing * 2), rh + (spacing * 2), margin) or (timeout < 0)
        if timeout > 0 then
          rect(map.data, dx, dy, rw, rh, 0)
          map.room[i] = { x = math.floor(dx + rw / 2), y = math.floor(dy + rh / 2), top = dy, left = dx, width = rw+1, height = rh+ 1}
          if i > 1 then
            connect(map.data, map.room[i-1].x, map.room[i-1].y, map.room[i].x, map.room[i].y)
          end
          i = i + 1
        end
      until cleared(map.data, w, h) > digPercent or (timeout < 0)
      for i = 1, #map.room do
        local y, x = map.room[i].top, map.room[i].left
        local rw, rh = map.room[i].width, map.room[i].height
        if math.random() > 0.5 and rw >= 7 and rh >= 7 then
          column(map.data, x, y, rw, rh)
          map.room[i].hasColumn = true
        end
        if math.random() > 0.5 and rw < roomW and rh < roomH  then
          tower(map.data, x-1, y-1, rw+1, rh+1)
          map.room[i].top, map.room[i].left = y-1, x-1
          map.room[i].width, map.room[i].height = rw+2, rh+2
          map.room[i].hasTower = true
        end
      end
    until (cleared(map.data, w, h) > (digPercent/1.75)) or (tries < 0)
  elseif method == "dungeon" then
    local x, y, rw, rh = margin, margin -- start at top left
    local wide, tall
    repeat
      -- random room sizes
      if (minW < roomW) and (minH < roomH) then
        rw = math.random(minW,roomW)
        rh = math.random(minH,roomH)
      else
        rw, rh = roomW, roomH
      end
      -- square and odd rooms
      if not allowEven then
        if (rw % 2 == 0) then
          rw = math.min(roomW, rw+1)
        end
        if (rh % 2 == 0) then
          rh = math.min(roomH, rh+1)
        end
      end
      if square then
        rw = rh
      end

      if (x + roomW) < (w-margin) and (y + roomH) < (h-margin) then
        local ox, oy = math.floor((roomW-rw)/2), math.floor((roomH-rh)/2)
        rect(map.data, x+ox, y+oy, rw-1, rh-1, 0)
        map.room[#map.room+1] = { x = math.floor(x + ox + rw / 2), y = math.floor(y + oy + rh / 2), top = y + oy, left = x + ox, width = rw, height = rh }
        print (roomNum, json.encode( map.room[roomNum]))
        if roomNum > 1 then
          connect(map.data, map.room[roomNum-1].x, map.room[roomNum-1].y, map.room[roomNum].x, map.room[roomNum].y)
        end
        roomNum = roomNum + 1
        x = x + roomW + spacing
      else
        wide = wide or (roomNum - 1)
        tall = (tall or 0) + 1
        y = y + roomH + spacing
        x = margin
      end
    until (y + minH) > (h-margin)
    roomNum = #map.room
    if roomNum < (tall * wide) then tall = tall - 1 end -- we didn't build a last row

    print ("Wide", wide, "tall", tall, "num", roomNum)
    connect(map.data, map.room[wide].x, map.room[wide].y, map.room[roomNum].x, map.room[roomNum].y)

    print ("connecting random rooms")
    for i = 1, math.floor(roomNum * 0.25) do
      local one, two = math.random(roomNum),math.random(roomNum)
      connect(map.data, map.room[one].x, map.room[one].y, map.room[two].x, map.room[two].y)
    end

    for i = 1, #map.room do
      local y, x = map.room[i].top, map.room[i].left
      local rw, rh = map.room[i].width, map.room[i].height
      if math.random() > 0.5 and rw >= 7 and rh >= 7 then
        column(map.data, x, y, rw, rh)
        map.room[i].hasColumn = true
      end
      if math.random() > 0.5 and rw < roomW and rh < roomH  then
        tower(map.data, x-1, y-1, rw+1, rh+1)
        map.room[i].top, map.room[i].left = y-1, x-1
        map.room[i].width, map.room[i].height = rw+2, rh+2
        map.room[i].hasTower = true
      end
    end
  end

  for x = 1, w do
    for y = 1, h do
      map.inside[x][y] = map.data[x][y] == 0
    end
  end

  dump(map.data, w, h)

-- render a map via spritesheet
  function map.simpleRender(options)

    options = options or {}

    local w, h = map.width, map.height
    local instance = options.parent or display.newGroup()
    local size, spacing = options.size or 16, options.spacing or 16

    local sheet = options.sheet
    local index = options.index or 1

    for y = 2, h-1 do
      for x = 2, w-1 do
        if map.data[x][y] == 1 and ((map.data[x-1][y]==0) or (map.data[x+1][y]==0) or (map.data[x][y-1]==0) or (map.data[x][y+1]==0)) then
          local node
          if sheet then
            node = display.newImageRect(instance, sheet, index, size, size)
          else
            node = display.newRect(instance, (x-w/2) * spacing, (y-h/2) * spacing, 16, 16)
          end
          node.x, node.y = (x-w/2) * spacing, (y-h/2) * spacing
          node._x, node._y = node.x, node.y
        end
      end
    end
    return instance
  end

  function map.render(options)

    options = options or {}

    local w, h = map.width, map.height
    local instance = options.parent or display.newGroup()
    local size, spacing = options.size or 16, options.spacing or 16
    local wallColor, floorColor = options.wallColor or "FFFFFF", options.floorColor or "FFFFFF"
    local data = map.data
    local rooms = map.room

    local sheet = options.sheet

    -- helpers
    function instance.findRoom(x, y)
      for i = 1, #rooms do
        local ry, rx = rooms[i].top, rooms[i].left
        local rw, rh = rooms[i].width, rooms[i].height
        if x >= rx and x <= (rx + rw) and y >= ry and y <= (ry + rh) then
          return i
        end
      end
    end

    function instance.mapToContent(x,y)
      return x * 16 - 8, y * 16 - 8
    end

    instance.room = map.room
    instance.inside = map.inside

    -- Pass 1
    for x = 1, w do
      for y = 1, h do
        -- cases of walls
        local c = data[x][y] > 0 -- center
        local n = (data[x][y-1] or 0) > 0 -- north
        local s = (data[x][y+1] or 0) > 0 -- south
        local e = data[x-1] and (data[x-1][y] > 0) -- east
        local w = data[x+1] and (data[x+1][y] > 0) -- west
        local ne = data[x-1] and (data[x-1][y-1] or 0) > 0 -- north
        local se = data[x-1] and (data[x-1][y+1] or 0) > 0 -- south
        local nw = data[x+1] and (data[x+1][y-1] or 0) > 0 -- east
        local sw = data[x+1] and (data[x+1][y+1] or 0) > 0 -- west
        if n and s and e and w and ne and nw and se and sw and c then data[x][y] = 99 end -- remove
      end
    end

    -- Pass 2
    for x = 1, w do
      for y = 1, h do
        if data[x][y] == 99 then data[x][y] = -1 end -- remove
        if x==1 or y ==1 or x==w or y==h then data[x][y] = -1 end
      end
    end

    -- Pass 3
    for x = 1, w do
      for y = 1, h do
        local wallType = 0
        -- cases of walls
        local c = data[x][y] > 0 -- center
        local n = (data[x][y-1] or 0) > 0 -- north
        local s = (data[x][y+1] or 0) > 0 -- south
        local e = data[x-1] and (data[x-1][y] > 0) -- east
        local w = data[x+1] and (data[x+1][y] > 0) -- west

        -- cases of walls
        if c then
          wallType = 1
          if w then wallType = wallMap.w end
          if e then wallType = wallMap.e end
          if s then wallType = wallMap.s end
          if n then wallType = wallMap.n end
          if e and w then wallType = wallMap.ew end
          if n and s then wallType = wallMap.ns end
          if s and w then wallType = wallMap.sw end
          if n and w then wallType = wallMap.nw end
          if s and e then wallType = wallMap.se end
          if n and e then wallType = wallMap.ne end
          if s and e and w then wallType = wallMap.sew end
          if e and n and s then wallType = wallMap.ens end
          if n and w and s then wallType = wallMap.nws end
          if n and e and w then wallType = wallMap.new end
          if n and s and e and w then wallType = wallMap.nsew end
        end

        local cell
        if wallType > 0 and wallType < 17 then
          cell = display.newImageRect(instance, sheet, wallType, size, size)
          cell:setFillColor(unpack(color.hex2rgb(wallColor)))
          cell.name = "wall"
          cell.type = "solid"
        elseif wallType == 0 and map.inside[x][y] then -- floor
          local room, tile = instance.findRoom(x,y), nil
          local even = (x % 2) == (y % 2)
          if room then -- in a room, drop a 4x set
            if even then
              tile = (math.random(2) + 16) + ((1 + room % 3) * 4)
            else
              tile = (math.random(3,4) + 16) + ((1 + room % 3) * 4)
            end
          else -- hallway
            tile = math.random(4) + 16
          end
          cell = display.newImageRect(instance, sheet, tile, size, size)
          cell:setFillColor(unpack(color.hex2rgb(floorColor)))
          cell.type = "floor"
        end

        -- put in group
        if cell then
          instance:insert(cell)
          cell.x, cell.y = x*spacing - size/2, y*spacing - size/2
          if cell.type == "floor" then
            cell:rotate(90 * (math.random(4)-1))
          end
        end
      end
    end

    return instance

  end

  return map
end

return M