local M = {}
local json = require "json"
local tiled = require "com.ponywolf.ponytiled"
local fx = require "com.ponywolf.ponyfx"

-- load a tiled map
local emoteData = json.decodeFile(system.pathForFile("gfx/emotes.json"))  -- load from json export

function M.new(object, xOffset, yOffset)
  local instance = tiled.new(emoteData, "gfx")
  instance:extend("decor")
  --local bubble = instance:findObject("bubble")
  local emotes = instance:listTypes("emote")
  local ox, oy = xOffset or 0, yOffset or 0
  instance:centerAnchor()

  for i = 1, #emotes do
    emotes[i].isVisible = false
  end

  local function enterFrame()
    if instance and instance.isVisible then
      if object then
        local x, y = object:localToContent(0,0)
        --print(x, y,xOffset,yOffset)
        instance.x, instance.y = x, y
        instance:translate(ox, oy)
      else
        display.remove(instance)
      end
    end
  end

  function instance:show(emote)
    --fx.breath(bubble, 0.005)
    for i = 1, #emotes do
      transition.cancel(emotes[i])
      emotes[i].isVisible = emotes[i].name == emote
      if emotes[i].name == emote then
        fx.bounce(emotes[i])
      else
        transition.cancel(emotes[i])
      end
    end
    self.isVisible = true
    enterFrame()
  end

  function instance:hide()
    --transition.cancel(bubble)
    for i = 1, #emotes do
      transition.cancel(emotes[i])
    end
    self.isVisible = false
  end

  function instance:finalize()
    -- On remove, cleanup instance
    Runtime:removeEventListener("enterFrame", enterFrame)
  end

  instance:addEventListener("finalize")
  Runtime:addEventListener("enterFrame", enterFrame)

  instance:hide()

  return instance
end

return M