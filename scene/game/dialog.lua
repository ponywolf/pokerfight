
local json = require "json"
local composer = require "composer"
local snap = require "com.ponywolf.snap"
local snd = require "com.ponywolf.ponysound"
local tiled = require "com.ponywolf.ponytiled"
local fx = require "com.ponywolf.ponyfx"

local M = {}
local statusData = json.decodeFile(system.pathForFile("gfx/dialog.json"))  -- load from json export
local queue = {}

local function split(inputstr, sep)
  sep = sep or "|"
  inputstr = inputstr or ""
  local tbl={}
  local i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    tbl[i] = str
    i = i + 1
  end
  return tbl
end

local kinds = {"paper", "stone" }

function M.new()
  local scene = composer.getScene("scene.game")
  local parent = scene.view.hud or scene.view

  -- make the ui
  local instance = tiled.new(statusData, "gfx")
  instance:extend("label", "textBox")
  instance:centerAnchor()
  instance.x = display.contentCenterX
  instance.y = display.screenOriginY - instance.height
  instance.alpha = 0
  parent:insert(instance)

  -- bounce more
  fx.bounce(instance:findObject("more"), 0.15)

  function instance:add(text)
    local lines = split(text)
    for i = 1, #lines do
      table.insert(queue,lines[i])
    end
    self:show()
    return true
  end

  local function close()
    if instance.closing then return true end
    snd:play("step")
    if instance then
      transition.cancel(instance)
    end

    if #queue > 1 then -- we have more to say
      -- remove it
      queue[1] = nil
      table.remove(queue, 1)
      instance:show()
    elseif instance.open then -- close it
      queue = {}
      instance.closing = true
      instance.open = false
      snap.safe(instance, "bottomCenter", 16)
      transition.to(instance, { time = 66, alpha = 0 })
      transition.to(instance, { time = 250, xScale = 0.75, yScale = 0.75, transition = easing.outBounce,
          onComplete = function()
            instance.closing = false
            scene:resume()
          end})
    end
  end

  function instance:show()
    scene:pause()

    -- text is the top of queue
    local text = queue[1] or "Error"

    -- show the text
    local label = instance:findObject("text")
    label:update(text)

    -- timeout based on text length
    transition.to(self, { delay = 5000, onComplete = close })

    -- am I open?
    if not self.open then
      self.open = true
      snd:play("step")
      self.xScale = 1
      self.yScale = 1
      snap.safe(self, "bottomCenter", 16)
      transition.to(instance, { time = 66, alpha = 1 })
      transition.from(self, { time = 250, xScale = 0.75, yScale = 0.75, transition = easing.outBounce })
    end
    self:toFront()

    -- more?
    self:findObject("more").isVisible = #queue > 0

  end

  -- touch to close
  function instance:touch(event)
    if event.phase == "ended"  or event.phase == "cancelled" then
      close()
    end
    return true
  end

  -- listen for status event
  local function dialog(event)
    local background = event.background or "paper"
    for i = 1, #kinds do
      instance:findLayer(kinds[i]).isVisible = false
    end
    instance:findLayer(background).isVisible = true
    instance:add(event.text)
    instance:toFront()
  end

  -- remove status listener
  function instance:finalize()
    if instance then
      transition.cancel(instance)
    end
    Runtime:removeEventListener("dialog", dialog)
    instance = nil
    queue = {}
  end
  instance:addEventListener("finalize")

  instance:addEventListener("touch")
  Runtime:addEventListener("dialog", dialog)
  return instance

end

return M
