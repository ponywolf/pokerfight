-- Requirements
local composer = require "composer"
local snap = require "com.ponywolf.snap"
local snd = require "com.ponywolf.ponysound"
local card = require "scene.inventory.card"
local stats = require "com.ponywolf.ponystats"
local tiled = require "com.ponywolf.ponytiled"
local fx = require "com.ponywolf.ponyfx"
local settings = require "com.ponywolf.settings"
local csv = require "com.ponywolf.csv"
local libtap = require "scene.inventory.tap"
local libmanager = require "scene.inventory.manager"

local json = require "json"

local itemData = csv.load("items.csv")
local gearData = csv.load("gear.csv")
local spellsData = csv.load("spells.csv")

-- assume everything is unlocked
local defaultSpells = spellsData.ids
local defaultGear = gearData.ids

-- only show items that have a "storage" tag
local defaultItems = {}
for i = 1, #itemData do
  if itemData[i].store then 
    defaultItems[#defaultItems+1] = itemData[i].id
  end
end

-- Variables local to scene
local scene = composer.newScene()
local ui, dim, tap, item, hand, isPaused
local inTransition = false

-- are we running on a simulator?
local isSimulator = "simulator" == system.getInfo("environment")
local isMobile = ("ios" == system.getInfo("platform")) or ("android" == system.getInfo("platform"))

function scene:create(event)
  local view = self.view -- add display objects to this group
  dim = fx.dim()
  view:insert(dim)

  tap = libtap.new()
  snap(tap)
  view:insert(tap)

  function ui(event)
    local phase, name = event.phase, event.buttonName or "none"
    print ("ui", phase, name)
    if phase == "released" then
      if name == "tap" and event.tag == "inventory" then -- we don't have an overlay
        composer.hideOverlay()
      end
      return true
    end
  end

  self.playarea = display.newGroup()
  view:insert(self.playarea)
end

function scene:setupCards(event)
  event = event or {}
  local params = event.params or {}

  -- disable cards
  for i = self.playarea.numChildren, 1, -1 do
    self.playarea[i]:finalize()
  end

  -- clear playarea
  display.remove(scene.playarea)
  scene.playarea = display.newGroup()
  scene.view:insert(scene.playarea)
  scene.playarea.isVisble = false
  scene.playarea:toFront()

  -- what kind of inventory
  local inventory, inventoryData
  if params.inventory == "gear" then
    inventory = defaultGear
    inventoryData = gearData
  elseif params.inventory == "items" then
    inventory = defaultItems
    inventoryData = itemData
  elseif params.inventory == "spells" then
    inventory = defaultSpells
    inventoryData = spellsData
  end

  libmanager:dump()

  -- deal
  local hand = {}
  for i = 1, #inventory do
    local stats = inventoryData:find(inventory[i])
    item = card.new(params.inventory, stats, libmanager.count(inventory[i]))
    item.x = display.contentCenterX
    if i > 1 then 
      item:set(hand[i-1].x + hand[i-1].designedWidth * 1.25)
    end
    item.hand = hand
    item:toBack()
    hand[i] = item
    scene.playarea:insert(item)
    if params.inventory == "gear" then
      hand[i].slideX = display.actualContentWidth * 2
    elseif params.inventory == "items" then
      hand[i].slideX = -display.actualContentWidth * 2
    end

  end

  -- exit
  tap:toBack()
  dim:toBack()
end


local function resize(event)
  -- re-layout the app's contents here

end


local function enterFrame(event)
  local elapsed = event.time

end

function scene:show(event)
  local phase = event.phase
  if (phase == "will") then
    scene:setupCards(event)
    isPaused = event.parent.isPaused
    if not isPaused then event.parent:pause() end
    Runtime:addEventListener("ui", ui)
    Runtime:addEventListener("enterFrame", enterFrame)
    Runtime:addEventListener("resize", resize)	
  elseif (phase == "did") then
    scene.playarea.isVisble = true			
    resize()
  end
end

function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then
    Runtime:removeEventListener("ui", ui)
    Runtime:removeEventListener("enterFrame", enterFrame)
    Runtime:removeEventListener("resize", resize)
    -- save gold to disk
  elseif (phase == "did") then
    if not isPaused then event.parent:resume() end
    display.remove(scene.view)
  end
end

function scene:destroy(event)

end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene