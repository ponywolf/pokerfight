-- tiled Plugin template

-- Use this as a template to extend a tiled object with functionality
local M = {}
local composer = require "composer"
local json = require "json"
local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local fx = require "com.ponywolf.ponyfx"
local libitems = require "scene.game.items"
local libmanager = require "scene.inventory.manager"
local alert = require "scene.alert"

local sensitivity = 8
local spacing, rotation, curve, speed = 1.25, 0.25, 0.0015, 0.9
local touching = false

local function distance (objA, objB)
  local dx = objA.x - objB.x
  local dy = objA.y - objB.y
  return math.sqrt (dx * dx + dy * dy)
end

function M.new(kind, item, count)
  local scene = composer.getScene("scene.game")

  item = item or {} 
  local mapData = json.decodeFile(system.pathForFile("gfx/" .. kind .. ".json"))  -- load from json export
  local instance = tiled.new(mapData, "gfx")
  instance:extend("label", "button", "textBox")
  instance.isShowing = true
  instance.isEnabled = true
  instance.slideX = 0
  instance:centerAnchor()

  local image -- will be the item image

  -- extra hit area
  instance:findObject("hit").isHitTestable = true
  instance:findObject("hit").isVisible = false

  local name = instance:findObject("name")
  if name then
    name.text = item.name or "Unknown"
  end

  -- rarity
  local banners = instance:listTypes("banner")
  for i = 1, #banners do banners[i].isVisible = false end
  instance:findObject(item.rarity or "normal").isVisible = true

  -- turn on actor
  local actors = instance:listTypes("actor")
  if actors and #actors > 1 then
    for i = 1, # actors do
      actors[i].isVisible = item.image == actors[i].name
      if actors[i].isVisible then
        image = actors[i]
        fx.float(image)
        fx.breath(instance:findObject("shadow"), 0.025, 1666)
      end
    end
  end

  -- stats
  local damage = instance:findObject("damage") or {}
  local damageBadge = instance:findObject("damageBadge") or {}
  local damageIcon = instance:findObject("damageIcon") or {}
  if item.damage then
    damage.text = item.damage
  else
    damage.isVisible = false
    damageBadge.isVisible = false
    damageIcon.isVisible = false
  end

  local defense = instance:findObject("defense") or {}
  local defenseBadge = instance:findObject("defenseBadge") or {}
  local defenseIcon = instance:findObject("defenseIcon") or {}
  if item.defense then
    defense.text = 100 * (tonumber(item.defense) or 0) .. "%"
  else
    defense.isVisible = false
    defenseBadge.isVisible = false
    defenseIcon.isVisible = false
  end

  local hp = instance:findObject("hp") or {}
  local hpBadge = instance:findObject("hpBadge") or {}
  local hpIcon = instance:findObject("hpIcon") or {}
  if item.hp then
    hp.text = "+" .. (item.hp or 1)
  else
    hp.isVisible = false
    hpBadge.isVisible = false
    hpIcon.isVisible = false
  end

  local mana = instance:findObject("mana") or {}
  local manaBadge = instance:findObject("manaBadge") or {}
  local manaIcon = instance:findObject("manaIcon") or {}
  if item.mana then
    mana.text = (kind == "spells" and "" or "+") .. (item.mana or 1)
  else
    mana.isVisible = false
    manaBadge.isVisible = false
    manaIcon.isVisible = false
  end

  -- spell level
  local level = instance:findObject("level") or {}
  local levelBadge = instance:findObject("levelBadge") or {}
  if level and item.level then
    level.text = item.level
  else
    level.isVisible = false
    levelBadge.isVisible = false
  end

  -- cool down
  local cool = instance:findObject("cool") or {}
  local coolBadge = instance:findObject("coolBadge") or {}
  if cool and item.cooldown then
    cool.text = item.cooldown
  else
    cool.isVisible = false
    coolBadge.isVisible = false
  end

  -- bonus
  local bonus = instance:listTypes("bonus")
  for i = 1, #bonus do bonus[i].isVisible = false end
  local value = instance:findObject("bonusValue") or {}
  if item.bonus and instance:findObject(item.bonus) then
    instance:findObject(item.bonus).isVisible = true
    instance:findObject("bonusBadge").isVisible = true
    value.text = "+" .. (item.bonusValue or "0")
  else
    value.isVisible = false
  end

  local function update(count)
    if kind == "items" then
      -- tag buttons
      local sell = instance:findObject("sell") or {}
      local use = instance:findObject("use") or {}
      sell.tag = item.id 
      use.tag = item.id 
      -- count
      local countText = instance:findObject("countValue") or {}
      count = count or 0
      if count == 0 then
        sell:disable()
        use:disable()
      end
      if countText then
        countText.text = "x" .. (count or 0)
      end
      -- special for health
      if item.use == "heal" then
        if not scene.hero:canHeal() then
          use:disable()
        end
      end
      -- special for mana
      if item.use == "mana" then
        if not scene.hero:canMana() then
          use:disable()
        end
      end
      -- can't use?
      if not item.use then
        use:disable()
      end
    elseif kind == "spells" then
      -- tag buttons
      local equipLeft = instance:findObject("equipLeft") or {}
      local equipRight = instance:findObject("equipRight") or {}
      equipLeft.tag = item.id 
      equipRight.tag = item.id
      -- show equiped
      local handLeft = instance:findObject("handLeft") or {}
      handLeft.isVisible = libmanager.slots["left"] == item.id
      local handRight = instance:findObject("handRight") or {}
      handRight.isVisible = libmanager.slots["right"] == item.id
    elseif kind == "gear" then
      --tag buttons
      local equip = instance:findObject("equip") or {}
      local sell = instance:findObject("sell") or {}
      equip.tag = item.id 
      sell.tag = item.id
      -- already equipped?
      if libmanager.slots["attack"] == item.id or libmanager.slots["defend"] == item.id then
        equip:disable()
        sell:disable()
      end
    end
  end
  update(count)

-- sell value
  local cost = instance:findObject("value") or {}
  if item.value then
    item.value = item.value or 0
    local displayCount = ""
    if item.value > 9999 then
      displayCount = math.floor(item.value / 1000) .. "k"
    else 
      displayCount = item.value
    end

    cost.text = displayCount or "0"
    if cost.text == "0" then
      cost.isVisible = false
      local gold = instance:findObject("gold")
      if gold then
        gold.isVisible = false
      end
    end
  end

  local description = instance:findObject("description")
  if description then
    description:update(item.description or "Unknown")
  end

  function instance:bounce()
--    BOUNCE a CARD
--    Maybe for new?
    if image then fx.bounce(image) end
  end

  function instance:unBounce()
--    BOUNCE a CARD
--    Maybe for new?
    if image then transition.cancel(image) end   
  end

  function instance:hide()
    -- Black out a CARD
    -- Maybe for hidden?
    if image then image:setFillColor(0) end
  end

  function instance:show()
    -- un-Black out a CARD
    -- Maybe for hidden?
    if image then image:setFillColor(1) end
  end

  function instance:lock()
    self:findLayer("locked").isVisible = true
  end

  function instance:unlock()
    self:findLayer("locked").isVisible = false
  end

  local dx, lastx, dy, lasty
  function instance:set(x, y)
    self.x = x or self.x
    instance.rotation = (self.x - display.contentCenterX) * rotation -- spacing, rotation, curve
    instance.y = (display.contentCenterY * 1.5) + (instance.x - display.contentCenterX) * (instance.x - display.contentCenterX) * curve
    self.markX, lastx = self.x, self.x
  end

  function instance:touch(event)
    if event.phase == "began" and self.isEnabled then
      display.getCurrentStage():setFocus(self, event.id)
      self.isFocus = true
      touching = true
      dx = 0
      -- freeze inertia
      for i = 1, #self.hand do
        self.hand[i].slideX = 0
        self.hand[i]:set()
      end
      self.moved = false
      self:toFront()
    elseif self.isFocus then
      if event.phase == "moved" then
        if self.moved then
          self.x = event.x - event.xStart + self.markX
          dx = (dx*9 + (self.x - lastx))/10
          for i = 1, #self.hand do
            event.phase = "spin"
            self.hand[i]:touch(event)
          end
          lastx, lasty = self.x, self.y   
        end
        -- Don't move unless we move 8px 
        if distance(event, { x = event.xStart, y = event.yStart }) > sensitivity and not self.moved then
          self.xScale, self.yScale = 1.05, 1.05
          self.moved = true
        end
      elseif event.phase == "ended" or event.phase == "cancelled" then
        display.getCurrentStage():setFocus(self, nil)
        self.isFocus = false
        touching = false
        self.xScale, self.yScale = 1,1 
        if not self.moved then
          if self.itemType ~= "currency" then
            if self.isShowing then
              self:help()
            else
              self:reveal()
            end
          end
        else
          -- gimme inertia
          for i = 1, #self.hand do
            self.hand[i].slideX = dx * 2
          end
          self:boundsCheck() 
        end
      end
    elseif event.phase == "spin" then
      self.x = event.x - event.xStart + self.markX
    end
    return true
  end

  function instance:boundsCheck()
    -- test if we're offscreen
    local max = #self.hand
    local firstX = self.hand[1].x
    local lastX = self.hand[max].x
    local w = self.designedWidth * spacing
    if firstX > display.contentCenterX then -- too far right/left
      self.hand[1].x = (speed * self.hand[1].x) + ((1-speed) *  display.contentCenterX)
      -- reset
      for i = 1, max do
        self.hand[i].slideX = 0
        if i > 1 then 
          self.hand[i].x = self.hand[i-1].x + w
        end
      end
      return true
    elseif lastX < display.contentCenterX then
      self.hand[max].x = (speed * self.hand[max].x) + ((1-speed) *  display.contentCenterX)
      for i = max,1,-1 do
        self.hand[i].slideX = 0
        if i < max then 
          self.hand[i].x = self.hand[i+1].x - w
        end
      end
      return true
    else 
      return false
    end
  end

  function instance:help(fast)
    -- Already hidden
    if not self.isShowing or not self.isEnabled then return end
    self.isShowing = false
    if fast then
      self:soloLayer("help", "name", "banners", "base", "buttons", "buttonIcons", "hit")
    else
      self.isEnabled = false
      transition.to (self, { time = 133, xScale = 0.01,
          onComplete = function ()
            self:soloLayer("help", "name", "banners", "base", "buttons", "buttonIcons", "hit")
            transition.to (self, { time = 133, xScale = 1,
                onComplete = function () self.isEnabled = true end
              })
          end })
    end
  end

  function instance:reveal(fast)
    -- Already showing
    if self.isShowing or not self.isEnabled then 
      return
    end
    self.isShowing = true
    if fast then
      self:defaultLayers()
    else
      self.isEnabled = false
      transition.to (self, { time = 133, xScale = 0.01,
          onComplete = function ()
            self:defaultLayers()
            transition.to (self, { tag = "cards", time = 133, xScale = 1, onComplete = function ()
                  snd:rnd("cardPlace1","cardPlace2","cardPlace3")
                  transition.to (self, { time = 133, onComplete = function () self.isEnabled = true end })
                end })
          end })
    end
  end

  -- ui listener
  local function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    if tag ~= item.id then return false end
    print(name,phase,tag)
    if phase == "released" and tag == item.id then
      if name == "use" then
        print("Trying to use:", item.id, item.use, libmanager.count(item.id))
        if libmanager.use(item.id) then
          scene.hero:execute(item.use, { })
          update(libmanager.count(item.id) or 0)
        end
      elseif name == "sell" then
        if kind == "items" then 
          if libmanager.use(item.id) then
            print("Selling", item.id, "for", item.value )
            scene.hero:execute("coin", { num = item.value })
            update(libmanager.count(item.id) or 0)
          end
        elseif kind == "gear" then
          -- TODO check we have 1 sword at least, equip next best
          local function ok()
            scene.hero:execute("coin", { num = item.value })
          end
          alert.new({ filename = "gfx/alert.json", dim = true, text = "Selling gear is not working yet!", ok = "cancel" })
        end
      elseif name == "equip" then
        if libmanager.slots["attack"] == item.id or libmanager.slots["defend"] == item.id then
          alert.new({ filename = "gfx/alert.json", dim = true, text = "Weapon already equipped!", ok = "cancel" })
        else
          local function ok()
            libmanager.slots[item.slot] = item.id
            scene.hero:equip(item.slot, item.icon)
          end
          alert.new({ filename = "gfx/confirm.json", dim = true, text = "Equip " .. item.name .. "?", ok = ok })
        end
      elseif name == "equipLeft" then
        if libmanager.slots["left"] == item.id or libmanager.slots["right"] == item.id then
          alert.new({ filename = "gfx/alert.json", dim = true, text = "Spell already equipped!", ok = "cancel" })
        else
          local function ok()
            libmanager.slots["left"] = item.id
          end
          alert.new({ filename = "gfx/confirm.json", dim = true, text = "Equip " .. item.name .. " to your left spell slot?", ok = ok })
        end
      elseif name == "equipRight" then
        if libmanager.slots["left"] == item.id or libmanager.slots["right"] == item.id then
          alert.new({ filename = "gfx/alert.json", dim = true, text = "Spell already equipped!", ok = "cancel" })
        else
          local function ok()
            libmanager.slots["right"] = item.id
          end
          alert.new({ filename = "gfx/confirm.json", dim = true, text = "Equip " .. item.name .. " to your right spell slot?", ok = ok })
        end
      end
    end
  end

  local function enterFrame()
    instance.x = instance.x + instance.slideX
    instance.slideX = instance.slideX * 0.825
    -- boundcheck the cards
    if not touching then 
      instance:boundsCheck()
    end
    -- rotate and move down
    instance.rotation = (instance.x - display.contentCenterX) * rotation -- spacing, rotation, curve
    instance.y = (display.contentCenterY * 1.5) + (instance.x - display.contentCenterX) * (instance.x - display.contentCenterX) * curve
  end

  function instance:finalize()
    self:removeEventListener("touch")
    Runtime:removeEventListener("enterFrame", enterFrame)
    Runtime:removeEventListener("ui", ui)
  end

  Runtime:addEventListener("enterFrame", enterFrame)
  Runtime:addEventListener("ui", ui)
  instance:addEventListener("finalize")
  instance:addEventListener("touch")

  return instance
end

return M