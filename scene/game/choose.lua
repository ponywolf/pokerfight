-- Requirements
local composer = require "composer"
local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local snap = require "com.ponywolf.snap"
local fx = require "com.ponywolf.ponyfx"
local json = require "json"

-- Vars/Objects local to scene
local M = {}

local libmanager = require "scene.inventory.manager"

function M.new(event)

  -- Get the current scene
  local scene = composer.getScene("scene.game")

  local mapData = json.decodeFile(system.pathForFile("gfx/choose.json"))  -- load from json export

  local instance = tiled.new(mapData, "gfx")
  instance:centerAnchor()
  instance:extend("button", "label", "decor")

  local function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    if phase == "released" and (tag == "choose" or tag == "android") then
      snd:play("click")
      if name == "fight" then
        scene:toggle("reshuffle")
      elseif name == "flee" then
        scene:toggle("flee")
      end
    end
    return true
  end

  function instance:open()
    if self.isOpen then return end
    transition.cancel(self)
    snap(instance, "bottomcenter", 24)
    local function finished()
      snap(instance, "bottomcenter", 24)
      Runtime:addEventListener("ui", ui)
    end
    transition.from(instance, {y = instance.designedHeight + display.actualContentHeight, time = 666, delay = 0,
        onCancel = finished, onComplete = finished, transition = easing.outBounce
      })
    snd:play("swoosh")
    self.isVisible = true
    self.isOpen = true
  end

  function instance:close()
    if not self.isOpen then return end
    transition.cancel(self)
    snap(instance, "bottomcenter", 24)
    local function finished()
      self.isVisible = false
    end
    transition.to(instance, {y = instance.designedHeight + display.actualContentHeight, time = 333, delay = 0,
        onCancel = finished, onComplete = finished, transition = easing.inQuad
      })
    snd:play("swoosh")
    Runtime:removeEventListener("ui", ui)
    self.isOpen = false
  end

  return instance
end

return M