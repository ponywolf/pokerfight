-- roguelike hero module

local M = {}

local json = require "json"

local fx = require "com.ponywolf.ponyfx"
local tiled = require "com.ponywolf.ponytiled"
local color = require "com.ponywolf.ponycolor"
local stage = display.getCurrentStage()

-- load map

local cardData = json.decodeFile(system.pathForFile("gfx/card.json")) -- load from json export

local function inBounds(event, object)
  local ex, ey = event.x or 0, event.y or 0
  local bounds = object.contentBounds or {}
  local border = 4
  if bounds then
    if ex < bounds.xMin + border or ex > bounds.xMax - border or ey < bounds.yMin + border or ey > bounds.yMax - border then   
      return false
    else 
      return true
    end
    return false
  end
end

function M.new(suit, num)
  local instance = tiled.new(cardData, "gfx")
  instance:centerAnchor()
  instance:extend("label")
  instance.extensions = "scene.game."
  instance:extend("decor")
  instance.suit, instance.num = suit, num

  -- start with the back
  instance:soloLayer("back")

  local function sizeButton(scale)
    instance.xScale, instance.yScale = (scale or 1), (scale or 1)
  end

  -- hold/fold
  instance.hold = false 
  function instance:showHold(hold)
    if hold==nil then hold = self.hold end
    self:findLayer("hold").isVisible = hold
    self:findLayer("fold").isVisible = not hold
    self.hold = hold
  end

  function instance:hideHold()
    self:findLayer("hold").isVisible = false
    self:findLayer("fold").isVisible = false
    self.hold = false
  end

  -- touch code
  function instance:touch(event)
    local phase, name = event.phase, event.name
    if self.enabled == false then return false end
    -- press in animation
    if phase == "began" then
      stage:setFocus(self)
      self.isFocus = true
      sizeButton(0.95)
    elseif phase == "moved" and self.isFocus then
      if inBounds(event, self) and not self.isCancelled then -- inside
        sizeButton(0.95)
      else -- outside
        sizeButton()    
      end        
      -- release animation
    elseif self.isFocus and phase == "ended" or phase == "cancelled" then
      if inBounds(event, self) and not self.isCancelled then -- inside
        self:showHold(not self.hold)
      end        
      sizeButton()
      self.isFocus = false
      self.isCancelled = false
      stage:setFocus(nil)
    end
    return true  
  end

  -- set a card (usually while hidden)
  function instance:setCard(suit, num, onComplete)
    -- if we were anothe card, hix that suit
    self:hideLayer(instance.suit)
    -- set new suit/number
    suit, num = suit or instance.suit, num or instance.num
    -- set num
    if num == "T" then num = 10 end
    local numberText = self:findObject("text")
    local upsideDownText = self:findObject("upsideDown")
    numberText:update(num)
    upsideDownText:update(num)    
    -- color text
    if suit == "hearts" then
      numberText:setTextColor(unpack(color.hex2rgb("#dd2431")))
      upsideDownText:setTextColor(unpack(color.hex2rgb("#dd2431")))
    elseif suit == "clubs"  then
      numberText:setTextColor(unpack(color.hex2rgb("#94911b")))
      upsideDownText:setTextColor(unpack(color.hex2rgb("#94911b")))
    elseif suit == "spades"  then
      numberText:setTextColor(unpack(color.hex2rgb("#2a5963")))
      upsideDownText:setTextColor(unpack(color.hex2rgb("#2a5963")))
    elseif suit == "diamonds"  then
      numberText:setTextColor(unpack(color.hex2rgb("#4885aa")))
      upsideDownText:setTextColor(unpack(color.hex2rgb("#4885aa")))
    end
    --set properties
    self.num = num
    self.suit = suit
    self.name = "card"
    -- load special cards TODO from a CSV 
    self.mana = num == "3" -- mana threes
    self.health = (suit == "hearts" and num == "2")
    self.poison = (suit == "clubs" and num == "2")
    self.attack = num == "J" -- mana threes
    self.defend = (suit == "spades" and num == "2")
  end

  function instance:hide(onComplete)
    -- Already hidden
--    if not self.isShowing then
--      print ("Already hidden", self.value)
--      if onComplete then onComplete() end
--      return
--    end
    -- hide me
    self:hideHold()  
    self.isShowing = false
    transition.to (self, { tag = "cards", time = 66, xScale = 0.01,
        onComplete = function ()
          self:showLayer("back")
          transition.to (self, { tag = "cards", delay = 33, time = 66, xScale = 1, onComplete = onComplete})
        end })
  end

  function instance:reveal(onComplete)
    -- Already showing
    if self.isShowing then
      print ("Already showing", self.value)
      if onComplete then onComplete() end
      return
    end

    self.isShowing = true
    transition.to (self, { tag = "cards", time = 66, xScale = 0.01,
        onComplete = function ()
          self:hideLayer("back")
          self:showLayer(self.suit)
          self:showLayer("face")
          self:showLayer("text")
          self:findLayer("mana").isVisible = self.mana
          self:findLayer("health").isVisible = self.health
          self:findLayer("poison").isVisible = self.poison
          self:findLayer("attack").isVisible = self.attack
          self:findLayer("defend").isVisible = self.defend
          self.isShowing = true
--            snd:rnd("cardPlace1", "cardPlace2", "cardPlace3")
          transition.to (self, { tag = "cards", time = 66, xScale = 1, 
              onComplete = onComplete })
        end })
  end

  if suit and num then 
    instance:setCard(suit, num)
  end

  instance:addEventListener("touch")

  return instance
end

return M