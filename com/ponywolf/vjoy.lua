
-- Project: vjoy 0.3
--
-- Date: Mar 3, 2015
-- Updated: Nov 21, 2016

local M = {}
local stage = display.getCurrentStage()

function M.newButton(radius, key)

  local instance
  radius = radius or 64
  key = key or "buttonA"

  if type(radius) == "number" then
    instance = display.newCircle(0,0, radius)
    instance:setFillColor(0.2, 0.2, 0.2, 0.9)
    instance.strokeWidth = 6
    instance:setStrokeColor(1, 1, 1, 1)
  else
    instance = display.newImage(radius, 0,0)
  end

  function instance:touch(event)
    local phase = event.phase
    if phase=="began" then
      if event.id then stage:setFocus(event.target, event.id) end
      instance._xScale, instance._yScale = instance.xScale, instance.yScale
      instance.xScale, instance.yScale = instance.xScale * 0.95, instance.yScale * 0.95
      local keyEvent = {name = "key", phase = "down", keyName = key or "none"}
      Runtime:dispatchEvent(keyEvent)
    elseif phase=="ended" or phase == "cancelled" then
      if event.id then stage:setFocus(nil, event.id) end
      instance.xScale, instance.yScale = instance._xScale, instance._yScale
      local keyEvent = {name = "key", phase = "up", keyName = key or "none"}
      Runtime:dispatchEvent(keyEvent)
    end
    return true
  end

  function instance:activate()
    self:addEventListener("touch")
  end

  function instance:deactivate()
    self:removeEventListener("touch")
  end

  instance:activate()
  return instance
end

function M.newStick(startAxis, innerRadius, outerRadius)

  startAxis = startAxis or 1
  innerRadius, outerRadius = innerRadius or 48, outerRadius or 96
  local instance = display.newGroup()

  local outerArea 
  if type(outerRadius) == "number" then
    outerArea = display.newCircle(instance, 0,0, outerRadius)
    outerArea.strokeWidth = 2
    outerArea:setFillColor(0.2, 0.2, 0.2, 0.9)
    outerArea:setStrokeColor(1, 1, 1, 1)
  else
    outerArea = display.newImage(instance, outerRadius, 0,0)
    outerRadius = (outerArea.contentWidth + outerArea.contentHeight) * 0.25
  end

  local joystick 
  if type(innerRadius) == "number" then
    joystick = display.newCircle(instance, 0,0, innerRadius)
    joystick:setFillColor(0.4, 0.4, 0.4, 0.9)
    joystick.strokeWidth = 1
    joystick:setStrokeColor(1, 1, 1, 1)
  else
    joystick = display.newImage(instance, innerRadius, 0,0)
    innerRadius = (joystick.contentWidth + joystick.contentHeight) * 0.25
  end  

  -- where should joystick motion be stopped?
  local stopRadius = outerRadius - innerRadius

  function joystick:touch(event)
    local phase = event.phase
    if phase=="began" or (phase=="moved" and self.isFocus) then
      if phase == "began" then
        stage:setFocus(event.target, event.id)
        self.eventID = event.id
        self.isFocus = true
      end
      local parent = self.parent
      local posX, posY = parent:contentToLocal(event.x, event.y)
      local angle = -math.atan2(posY, posX)
      local distance = math.sqrt((posX*posX)+(posY*posY))

      if(distance >= stopRadius) then
        distance = stopRadius
        self.x = distance*math.cos(angle)
        self.y = -distance*math.sin(angle)
      else
        self.x = posX
        self.y = posY
      end
    else
      self.x = 0
      self.y = 0
      stage:setFocus(nil, event.id)
      self.isFocus = false
    end
    instance.axisX = self.x / stopRadius
    instance.axisY = self.y / stopRadius
    local axisEvent
    if not (self.y == (self._y or 0)) then
      axisEvent = {name = "axis", axis = { number = startAxis, type = "X"}, normalizedValue = instance.axisX }
      Runtime:dispatchEvent(axisEvent)
    end
    if not (self.x == (self._x or 0))  then 
      axisEvent = {name = "axis", axis = { number = startAxis+1, type = "Y" }, normalizedValue = instance.axisY }
      Runtime:dispatchEvent(axisEvent)
    end
    self._x, self._y = self.x, self.y
    return true
  end

  function instance:activate()
    self:addEventListener("touch", joystick)
    self.axisX = 0
    self.axisY = 0
  end

  function instance:deactivate()
    stage:setFocus(nil, joystick.eventID)
    joystick.x, joystick.y = outerArea.x, outerArea.y
    self:removeEventListener("touch", self.joystick)
    self.axisX = 0
    self.axisY = 0
  end

  instance:activate()
  return instance
end


-- touchStick (TM)

local touchSheetOptions = { width = 16, height = 16, numFrames = 5 }
local touchSheet = graphics.newImageSheet("gfx/touch.png", touchSheetOptions)
function M.newTouchpad(w, h, startAxis,stopRadius)

  startAxis = startAxis or 1
  stopRadius = stopRadius or 52
  local minDist = 4
  local numDots = 6
  local touchFrames = 10
  w,h = w or display.actualContentWidth, h or display.actualContentHeight
  local instance = display.newGroup()
  instance.touchpad = display.newRect(instance, 0, 0, w, h)
  instance.touchpad.isHitTestable = true
  instance.touchpad.alpha = 0.0
  instance.designedWidth, instance.designedHeight = w,h 

  instance.dots = {}

  local function enterFame()
    instance.touchpad.frames = (instance.touchpad.frames or 0) + 1
  end

  function instance.touchpad:touch(event)
    if instance.enabled == false then return false end
    local phase = event.phase
    if phase=="began" then
      stage:setFocus(self)
      self.isFocus = true
      self.frames = 0
      self.dots = {}
      for i = 1, numDots do 
        self.dots[i] = display.newImage(instance, touchSheet, 3)
        self.dots[i].x, self.dots[i].y = instance:contentToLocal(event.x, event.y)
        self.dots[i]._x, self.dots[i]._y = self.dots[i].x, self.dots[i].y
        self.dots[i].isVisible = false
      end
      self.arrow = display.newImage(instance, touchSheet, 1)
      self.arrow.x, self.arrow.y = instance:contentToLocal(event.x, event.y)
      self.arrow._x, self.arrow._y = self.arrow.x, self.arrow.y
      self.arrow.isVisible = false
      -- add enterFrame
      Runtime:addEventListener("enterFame", enterFame)
    elseif phase=="moved" and self.isFocus then
      local dx, dy = (event.xStart - event.x), (event.yStart - event.y) 
      local angle = -math.atan2(dx, dy)
      local distance = math.sqrt((dy*dy)+(dx*dx))
      local ddx, ddy, adx, ady = 0,0,0,0

      -- show arrow, dots
      for i = 1, numDots do 
        self.dots[i].isVisible = distance > minDist
      end
      self.arrow.isVisible = distance > minDist

      -- move arrow
      if distance < stopRadius then 
        self.arrow.x, self.arrow.y = self:contentToLocal(event.x, event.y)
        self.arrow.rotation = math.deg(angle)
        adx, ady = -dx, -dy
      else
        local maxX, maxY = math.cos(angle-3.14159/2)*stopRadius, math.sin(angle-3.14159/2)*stopRadius
        self.arrow.x, self.arrow.y = self:contentToLocal(event.xStart + maxX, event.yStart + maxY)
        self.arrow.rotation = math.deg(angle)
        adx, ady = maxX, maxY
      end

      -- move dots
      ddx, ddy = adx/(#self.dots+1), ady/(#self.dots+1)
      for i = 1, numDots do
        self.dots[i]:toBack()
        self.dots[i].x, self.dots[i].y = self.dots[i]._x + (ddx*i), self.dots[i]._y + (ddy*i)
      end

      -- kickoff event
      instance.axisX = adx / stopRadius
      instance.axisY = ady / stopRadius
      local axisEvent
      if not (self.arrow.y == (self.arrow._y or 0)) then
        axisEvent = {name = "axis", axis = { number = startAxis, type = "X" }, normalizedValue = instance.axisX }
        Runtime:dispatchEvent(axisEvent)
      end
      if not (self.arrow.x == (self.arrow._x or 0))  then 
        axisEvent = {name = "axis", axis = { number = startAxis+1, type = "Y" }, normalizedValue = instance.axisY }
        Runtime:dispatchEvent(axisEvent)
      end
      self.arrow._x, self.arrow._y = self.arrow.x, self.arrow.y
    elseif (phase=="ended" and self.isFocus) or phase=="cancelled" then
      -- send a button instead
      if (self.frames or 0) < 5 and not (self.arrow and self.arrow.isVisible) and phase ~= "cancelled" then
        -- send a button press
        Runtime:dispatchEvent({name = "button", phase = "down", buttonName = "buttonA"})
        local num, dist, time = 8,12,133
        for i = 1, num do
          local dot = display.newImage(instance, touchSheet, 4)
          dot.x, dot.y = instance:contentToLocal(event.x, event.y)
          local angle = 3.14159 * 2 / num * i
          --dot.rotation = math.deg(angle)
          transition.to(dot, { delta = true,
              x = math.cos(angle) * dist,
              y = math.sin(angle) * dist,
              time = time,
              transition = easing.outQuad,
              onComplete = function ()
                display.remove(dot)
              end })
        end
        Runtime:dispatchEvent({name = "button", phase = "up", buttonName = "buttonA"})
      end

      -- finish axis event
      display.remove(self.arrow)
      if self.dots then
        for i = 1, #self.dots do
          display.remove(self.dots[i])
        end
      end
      instance.axisX = 0
      instance.axisY = 0
      Runtime:dispatchEvent({name = "axis", axis = { number = startAxis, type = "X" }, normalizedValue =  instance.axisX })
      Runtime:dispatchEvent({name = "axis", axis = { number = startAxis+1, type = "Y" }, normalizedValue = instance.axisY })
      stage:setFocus(nil)
      self.isFocus = false

      -- remove enterFrame
      Runtime:removeEventListener("enterFame", enterFame)
    end
    return true
  end

  function instance:activate()
    print("Activating touchpad")
    self.enabled = true
    self:toFront()
    self:addEventListener("touch", self.touchpad)
  end

  function instance:deactivate()
    print("Dectivating touchpad")
    self:removeEventListener("touch", self.touchpad)
    self.touchpad:touch({phase = "cancelled"})
    self.enabled = false
  end

  instance:activate()
  return instance
end

return M
