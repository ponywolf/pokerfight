-- Requirements
local composer = require "composer"
local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local snap = require "com.ponywolf.snap"
local fx = require "com.ponywolf.ponyfx"
local json = require "json"

-- Vars/Objects local to scene
local scene = composer.newScene()
local alert, description, ui, key, callback, level

function scene:create(event)
  local view = self.view

  local params = event.params or {}
  level = event.params.level or {}

  view:insert(fx.dim())

  function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    if phase == "released" and (tag == "levelup" or tag == "android") then
      snd:play("click")
      if name == "ok" or name == "back" then
        composer.hideOverlay("fade", 133)	
      end
    end
  end

  function key(event)
    local phase, name = event.phase, event.keyName
    print(phase, name)
    if phase == "up" then
      if name == "buttonA" then
        ui({phase = "released", tag = "levelup", buttonName = "ok"})
        return true
      elseif name == "buttonB" then
        ui({phase = "released", tag = "levelup", buttonName = "back"})
        return true
      else

      end
    end
  end

  local streaks = fx.newStreak()
  streaks.alpha = 0.0
  transition.to(streaks, { alpha = 0.15 })
  snap(streaks)
  view:insert(streaks)

  -- load dialog
  local mapData = json.decodeFile(system.pathForFile("gfx/levelup.json"))  -- load from json export
  alert = tiled.new(mapData, "gfx")
  alert:centerAnchor()
  alert:extend("button", "label")
  
  -- show joystick helpers
  local device = require "com.ponywolf.device"
  local settings = require "com.ponywolf.settings"
  alert:findLayer("joystick").isVisible = device.isDesktop and settings:get("joystick", true)
  
  view:insert(alert)
  snap(alert)

  -- find text
end

function scene:show(event)
  local phase = event.phase
  local params = event.params or {}
  if phase == "will" then
    callback = params.callback

    print(json.prettify(level))
    alert:findObject("name"):update(level.name or "Unknown")
    alert:findObject("cards"):update(level.totalCards)
    alert:findObject("hp"):update(level.hp)
    alert:findObject("description"):update(level.description or "Unknown")

    transition.from(alert, {yScale = 0.5, yScale = 0.5, time = 666, delay = 0, transition = easing.outQuad,
        rotation = math.random(1)==0 and -360 or 360, 
        onComplete = function ()
          Runtime:addEventListener("key", key)				
          Runtime:addEventListener("ui", ui)				
        end})
    snd:play("swoosh")
  elseif phase == "did" then

  end
end

function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then
    snd:play("swooshClose")
    Runtime:removeEventListener("key", key)				
    Runtime:removeEventListener("ui", ui)				
  elseif phase == "did" then
    if callback then callback() end
  end
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")

return scene