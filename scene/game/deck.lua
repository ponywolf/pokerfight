-- standard deck with options

local M = {}

local json = require "json"

local fx = require "com.ponywolf.ponyfx"
local card = require "scene.game.card"

local nums = { "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A" }
M.jokerNum = "?"
local suits = { "C", "D", "H", "S" }

local function cardValue(card)
  local val = card:sub(1, 1)
  local n = tonumber(val)
  if n then return n end
  if val == "T" then return 10 end    
  if val == "J" then return 11 end
  if val == "Q" then return 12 end
  if val == "K" then return 13 end
  if val == "A" then return 1 end
  error("Invalid card value: ", val)
end

local function cardFace(card)
  local val = card:sub(1, 1)
  if val == "T" then return 10 else return val end    
end

local function cardSuit(card)
  local suit = card:sub(-1, -1)
  if suit == "C" then return "clubs" end    
  if suit == "D" then return "diamonds" end
  if suit == "H" then return "hearts" end
  if suit == "S" then return "spades" end
  if suit == "J" then return "joker" end
end

local mt = {
  __index = function (table, key)
    return table.available[key]
  end
  -- looks like __len doesn't exist in lua 5.1 but does in 5.2
--  ,__len = function (table)
--    return { #table.available }
--  end,
}

function M.new(options)
  local instance = {}
  options = options or {}
  setmetatable(instance, mt)
  instance.available = {}
  instance.unavailable = {}
  instance.noShuffle = options.noShuffle
  instance.jokerCount = options.jokerCount or 0
  instance.size = options.size or 52
  instance.suits = options.suits or suits
  instance.autoShuffle = options.autoShuffle or true
  -- ensure the size is a multiple of #suits
  instance.size = instance.size - instance.size % #instance.suits

  function instance:reset()
    self.available = {} -- needed to clear that table
    self.unavailable = {}

    for n = 1, self.size / #self.suits do
      for s = 1, #self.suits do
        self.available[#self.available + 1] = nums[(n % #nums) == 0 and #nums or (n % #nums)] .. self.suits[s]
      end
    end

    for j = 1, self.jokerCount do
      self.available[#self.available + 1] = M.jokerNum .. j
    end
  end

  function instance:shuffle(iterations)
    self:reset()
    for _ = 1, iterations or 5 do
      for d=1, #self.available do
        local rand = math.random(#self.available)
        self.available[d], self.available[rand] = self.available[rand], self.available[d]
      end
    end

    return instance
  end

  function instance:pop()
    if #self.available > 0 then
      local r = self.available[#self.available]
      instance.unavailable[#instance.unavailable + 1] = r
      self.available[#self.available] = nil
      return r
    else
      if instance.autoShuffle then
        self:reset()
        self:shuffle()
        return self:pop()
      else
        return false
      end
    end
  end

  function instance:push(card)
    for i = 1, #self.unavailable do
      if self.unavailable[i] == card then
        -- remove unavailable card
        self.unavailable[i] = self.unavailable[#self.unavailable]
        self.unavailable[#self.unavailable] = nil
        break
      end
    end

    local found = false
    for i = 1, #self.available do
      if self.available[i] == card then
        found = true
        break
      end
    end

    if not found then
      self.available[#self.available + 1] = card
    end
  end

  function instance.decode(card)
    --print(card)
    card = card:upper()
    return cardSuit(card), cardFace(card)
  end

  function instance:remaining()
    return #self.available
  end

  function instance:used()
    return #self.unavailable
  end

  instance:reset()
  if not instance.noShuffle then
    instance:shuffle()
  end

  return instance
end

return M