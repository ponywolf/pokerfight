-- Check whether t is a valid poker hand

local M = {}
local json = require "json"
local jokerNum = (require "scene.game.deck").jokerNum

M.default = { score = 0, name = "", highCard = 0 }

local handScores = {
  ["RF"] = 9,
  ["SF"] = 8,
  ["4K"] = 7,
  ["FH"] = 6,
  ["F"] = 5,
  ["S"] = 4,
  ["3K"] = 3,
  ["2P"] = 2,
  ["JB"] = 1,
  ["P"] = 0,
  ["AH"] = 0,
  ["HC"] = 0,
}

local function valid (t)
  if #t ~= 5 then return false end

-- TODO I broke this part :(
--  for k, v in pairs(t) do
--      for key, card in pairs(t) do
--          if  v.value == card.value and
--              v.suit == card.suit and
--              k ~= key
--          then
--              return false
--          end
--      end
--  end
  return true
end

-- Return numerical value of a single card
local function cardValue(card)
  local val = card:sub(1, 1)
  local n = tonumber(val)
  if n then return n end
  if val == "T" then return 10 end    
  if val == "J" then return 11 end
  if val == "Q" then return 12 end
  if val == "K" then return 13 end
  if val == "A" then return 14 end
  if val == jokerNum then return 0 end
  print("Invalid card value: ", val)
  return 0
end

local function cardLetter(num)
  if num == 10 then return "T" end    
  if num == 11 then return "J"  end
  if num == 12 then return "Q"  end
  if num == 13 then return "K" end
  if num == 14 then return "A" end
  return num
end

-- Detect whether hand t is a straight
local function straight (numbers, wilds)
  -- sort our numbers so we can easily identify a straight
  table.sort(numbers, function (a, b) return a < b end)

  local count = 0
  local ace = numbers[5] == 14
  local wildAce = false

  if ace then
    count = 1
  elseif wilds > 0 then
    count = 1
    ace = true
    wildAce = true
    wilds = wilds - 1
  end

  -- Count from 2 to 14 (ace) with an ace as a 1 already considered
  for i = 2, 14 do
    local found, subtracted = false, false
    -- TODO, I don't think we need this loop since the numbers are in order
    for _, v in ipairs(numbers) do
      -- if we have this number add 1 to the count
      if not found and v == i then
        --print("add: " .. v .. " = ".. i)
        count = count + 1
        found = true
        -- if this number is exactly 5 spots behind, we need to subtract 1 from the count
      elseif not subtracted and (v == i - 5) and v > 0 then
        --print("subtract: " .. v .. " = ".. (i - 5))
        count = count - 1
        subtracted = true
      end
    end

    if i == 6 and ace then
      if wildAce then
        wilds = wilds + 1
        wildAce = false
      end

      ace = false
      count = count - 1
    end

    --print (i .. " " .. (ace and "ACE" or "") .. ", " .. (found and "found" or "not found") .. " count = " .. count .. " wilds = " .. wilds)

    -- if the count + wild cards ever reaches 5, we have a straight
    if count + wilds == 5 then
      return true
    end
  end

  return false
end

-- Detect whether hand t is a flush
local function isFlush (suits, wilds)
  if wilds == 5 then
    return true
  end

  for _, v in pairs(suits) do
    if v == 5 - wilds then
      return true
    end
  end

  return false
end

local function getScore(hand, highPair, lowPair, highCard)
  return handScores[hand] + highPair * 10000 + lowPair * 100 + highCard
end

-- Return the rank of a poker hand represented as a string
function M.rank (cards, wildCards)

  if not valid(cards) then 
    return { score = -1, name = "Invalid" } 
  end

  wildCards = wildCards or { jokerNum }
  local numbers, numberCount, suits, wilds = {}, {}, {}, 0
  local suit, number
  local oak2, oak3, oak4 = 0, 0, 0

  --print(json.prettify(wildCards))

  for i = 1, #cards do
    number = cards[i]:sub(1, 1):upper()
    suit = cards[i]:sub(2, -1):upper()

    local isWild = false
    for j = 1, #wildCards do 
      if wildCards[j] == number then
        isWild = true
        break
      end
    end

    --print (number .. ' ' .. tostring(isWild))

    numbers[#numbers + 1] = isWild and 0 or cardValue(number)
    if not isWild then
      numberCount[number] = (numberCount[number] or 0) + 1
      suits[suit] = (suits[suit] or 0) + 1
    else
      wilds = wilds + 1
    end
  end

  local st, fl = straight(numbers, wilds), isFlush(suits, wilds)
  local hc, lc = numbers[5], numbers[1]

  for i = 1, #numbers do
    if numbers[i] ~= 0 then
      lc = numbers[i]
      break
    end
  end

  -- Find or X of a kind counts
  local highPair, lowPair = 0, 0
  for key, count in pairs(numberCount) do
    if count == 4 then
      oak4 = 1
      highPair = cardValue(key)
    elseif count == 3 then
      oak3 = 1
      highPair = cardValue(key)
    elseif count == 2 then
      oak2 = oak2 + 1
      if (cardValue(key) > highPair and oak3 == 0) then
        if highPair ~= 0 then
          lowPair = highPair
        end

        highPair = cardValue(key)
      else
        lowPair = cardValue(key)
      end
    end
  end

  print(json.prettify(cards))
  print("Wilds = " .. wilds)
  print("Flush = " .. tostring(fl) .. ", straight = " .. tostring(st))
  print("Of a Kind (4)" .. oak4 .. ", (3)" .. oak3 .. ", (2)" .. oak2)

  -- Go through hand checks from best to worst
  if st and fl then
    -- high card value adjustments if we have wilds
    if wilds == 5 then
      hc = 14
    elseif wilds == 4 then
      hc = hc + wilds
    else
      hc = math.min(lc + 4, 14)
    end
    -- Royal Flush
    if hc == 14 or (lc >= 10 and wilds > 1) or wilds == 5 then
      return { score = handScores["RF"], name = "Royal Flush", keep = { "*" } }
      -- Straight Flush
    else
      return { score = handScores["SF"], name = "Straight Flush", keep = { "*" } }
    end
    -- Four of a Kind
  elseif oak4 >= 1 or wilds >= 3 or (oak3 >= 1 and wilds >=1) or (oak2 >= 1 and wilds >=2) then
    if wilds >= 3 then
      highPair = hc
    end
    return { score = handScores["4K"], name = "4 of a Kind", keep = { cardLetter(highPair) } }
    -- Full House
  elseif (oak3 == 1 and oak2 == 1) or (oak2 == 2 and wilds >= 1) then
    return { score = handScores["FH"], name = "Full House", keep = { "*" }  }
    -- Flush
  elseif fl then
    return { score = handScores["F"], name = "Flush", keep = { "*" }  }
    -- Straight
  elseif st then
    return { score = handScores["S"], name = "Straight", keep = { "*" } }
    -- Three of a Kind
  elseif oak3 == 1 or (oak2 >= 1 and wilds >=1) or wilds >= 2 then
    if wilds >= 2 then
      highPair = hc
    end
    return { score = handScores["3K"], name = "3 of a Kind", keep = { cardLetter(highPair) } }
    -- Two Pair
  elseif oak2 >= 2 then
    -- We don't have to worry about wilds with 2 pairs since there is always a better use of the wild card
    return { score = handScores["2P"], name = "Two Pair", keep = { cardLetter(highPair), cardLetter(lowPair) } }
    -- Pair
  elseif oak2 >= 1 or wilds >= 1 then
    -- Our high card will be our high pair if we hae a wild card
    if wilds >= 1 then
      highPair = hc
    end
    if highPair > 10 then 
      return { score = handScores["JB"], name = "Jacks or Better", keep = { cardLetter(highPair) } }
    else
      return { score = handScores["P"], name = "Pair" }
    end
  end
  if hc == 14 then 
    return { score = handScores["AH"], name = "Ace High", }
  else
    return { score = handScores["HC"], name = "High Card", }
  end
end

-- Main procedure
local testCases = {
  {"2H","2D","2C","kC","QD"}, -- three-of-a-kind
  {"2H","2D","QC","QC","QD"}, -- three-of-a-kind
  {"KH","QC","QS","QH","2S"}, -- three-of-a-kind
  {"2H","5H","7D","8C","9S"}, -- high-card (9)
  {"AH","2D","3C","4C","5D"}, -- straight (14)
  {"4H","5D","6C","7C","8D"}, -- another straight (8)
  {"AH","2D","3C","4C","6D"}, -- almost a straight (high card 14)
  {"AH","KD","QC","JC","TD"}, -- ace high straight
  {"2H","3H","2D","3C","3D"}, -- full-house
  {"2H","7H","2D","3C","3D"}, -- two-pair
  {"2H","7H","7D","7C","7S"}, -- four-of-a-kind 
  {"TH","JH","QH","KH","9H"}, -- straight-flush
  {"4H","4S","KS","5D","TS"}, -- one-pair
  {"QC","TC","7C","6C","4C"}, -- flush
  {"AD","JD","QD","KD","TD"}, -- royal flush
  {"??","??","9D","QD","JD"}, -- joker test
}

--for _, case in pairs(testCases) do 
--  print(case, ": " .. json.prettify(M.rank(case))) 
--  print(case, " (2s wild): " .. json.prettify(M.rank(case, { "?", "2" }))) 
--end

return M