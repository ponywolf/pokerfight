
local json = require "json"
local composer = require "composer"
local snap = require "com.ponywolf.snap"
local snd = require "com.ponywolf.ponysound"
local tiled = require "com.ponywolf.ponytiled"
local fx = require "com.ponywolf.ponyfx"

local M = {}
local statusData = json.decodeFile(system.pathForFile("gfx/status.json"))  -- load from json export
local queue = {}

local function split(inputstr, sep)
  sep = sep or "|"
  inputstr = inputstr or ""
  local tbl={}
  local i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    tbl[i] = str
    i = i + 1
  end
  return tbl
end

function M.new()
  local scene = composer.getScene("scene.game")
  local parent = scene.view.hud or scene.view

  -- make the ui
  local instance = tiled.new(statusData, "gfx")
  instance:extend("label")
  instance:centerAnchor()
  instance.x = display.contentCenterX
  instance.y = display.screenOriginY - instance.height
  parent:insert(instance)

  function instance:add(text)
    if #queue > 0 and text == queue[1] then
      print ("Duplicate status", text)
      return false
    end
    local lines = split(text)
    for i = 1, #lines do
      table.insert(queue,lines[i])
    end
    self:show()
    return true
  end

  local function close()
    if instance.closing then return true end
    snd:play("step")
    if instance then
      transition.cancel(instance)
    end

    if #queue > 1 then -- we have more to say
      -- remove it
      queue[1] = nil
      table.remove(queue, 1)
      transition.from(instance:findObject("text"), { time = 166, xScale = 1.05, yScale = 1.05, transition = easing.outBounce })
      instance:show()
    elseif instance.open then -- close it
      queue = {}
      instance.closing = true
      instance.open = false
      snap.safe(instance, "topcenter", 8)
      transition.to(instance, { time = 250, y = display.screenOriginY - instance.height,
          onComplete = function()
            instance.closing = false
          end})
    end
  end

  function instance:show()
    -- text is the top of queue
    local text = queue[1] or "Error"
    instance:toFront()

    -- show the text
    local label = instance:findObject("text")
    label:update(text)

    -- timeout based on text length
    transition.to(self, { delay = 2250, onComplete = close })

    -- am I open?
    if not self.open then
      self.open = true
      snd:play("step")
      snap.safe(self, "topcenter", 8)
      transition.from(self, { time = 333, y = display.screenOriginY - self.height, transition = easing.outBounce })
    end
    self:toFront()
  end

  -- touch to close
  function instance:touch(event)
    if event.phase == "ended"  or event.phase == "cancelled" then
      close()
    end
    return true
  end
  instance:addEventListener("touch")

  -- listen for status event
  local function status(event)
    instance:add(event.text)
    print ("Status", event.text, #queue)
  end

  -- remove status listener
  function instance:finalize()
    Runtime:removeEventListener("status", status)
    if self then
      transition.cancel(self)
    end
    instance = nil
    queue = {}
  end
  instance:addEventListener("finalize")

  Runtime:addEventListener("status", status)
  return instance

end

return M
