-- Requirements
local composer = require "composer"
local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local snap = require "com.ponywolf.snap"
local fx = require "com.ponywolf.ponyfx"
local settings = require "com.ponywolf.settings"
local stats = require "com.ponywolf.ponystats"
local json = require "json"

-- Vars/Objects local to scene
local scene = composer.newScene()
local alert, ui, key, callback, showStats

local name = {}
name[12] = { k = "floorsWon", v = "Floors Won" }
name[11] = { k = "xp", v = "XP Earned" }
name[10] = { k = "monstersKilled", v = "Enemies Killed" }
name[9] = { k = "attack", v = "Hands Played" }
name[8] = { k = "handsWon", v = "Hands Won" }
name[7] = { k = "handsTied", v = "Hands Tied" }
name[6] = { k = "itemsFound", v = "Items Found" }
name[5] = { k = "chests", v = "Chests Opened" }
name[4] = { k = "monsters", v = "Monsters Found" }
name[3] = { k = "potions", v = "Potions Drank" }
name[2] = { k = "weaponSwap", v = "Weapons Used" }
name[1] = { k = "shieldSwap", v = "Shields Used" }

local function short(num)
  if num > 9999 then
    if num > 999999 then
      return math.floor(num / 1000000) .. "M"
    else
      return math.floor(num / 1000) .. "k"
    end
  else
    return num
  end
end

function scene:create(event)
  local view = self.view

  local params = event.params or {}
  local statKind

  view:insert(fx.dim())

  function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    snd:play("click")
    if phase == "released" and (tag == "floor" or tag == "android") then
      if name == "ok" or name == "back" then
        local bg = alert:findLayer("tiles")
        if bg then
          transition.cancel(bg)
        end
        composer.hideOverlay("fade", 133)
      elseif name == "global" then
        showStats(statKind.text == "All Time")
      end
    end
  end

  function key(event)
    local phase, name = event.phase, event.keyName
    print(phase, name)
    if phase == "up" then
      if name == "buttonA" then
        ui({phase = "released", tag = "floor", buttonName = "ok"})
        return true
      elseif name == "buttonY" then
        ui({phase = "released", tag = "floor", buttonName = "global"})
        return true
      else

      end
    end
  end

  local streaks = fx.newStreak()
  streaks.alpha = 0.0
  transition.to(streaks, { alpha = 0.15 })
  snap(streaks)
  view:insert(streaks)

-- load dialog
  local mapData = json.decodeFile(system.pathForFile("gfx/floor.json"))  -- load from json export
  alert = tiled.new(mapData, "gfx")
  alert:centerAnchor()
  alert:extend("button", "label", "decor")

  view:insert(alert)
  snap(alert)

  function showStats(session)
    local currentFloor = settings:get("floor",1)
    if session then
      statKind:update("Floor " .. currentFloor or 1)
    else
      statKind:update("All Time")
    end
    local key, val = alert:findObject("key"), alert:findObject("val")
    key:update(nil)
    val:update(nil)
    local i = 0
    local bg = alert:findLayer("tiles")
    for j = #name - (session and 1 or 0), 1, -1 do
      local num = stats.get(name[j].k, session)
      if num > 0 then
        transition.from(bg, { xScale = 1.025, yScale = 1.025, delay = 333 + (166 * i), time = 133, transition = easing.inQuad,
            onComplete = function ()
              snd:play("boop")
              if key and val then
                key:update(key.text .. "\n" .. name[j].v)
                val:update(val.text .. "\n" .. short(num or 0))
              end
              bg.xScale = 1
              bg.yScale = 1
            end,
            onCancel = function ()
              if key and val then
                key:update(key.text .. "\n" .. name[j].v)
                val:update(val.text .. "\n" .. short(num or 0))
              end
              bg.xScale = 1
              bg.yScale = 1
            end})
        i = i + 1
      end
    end
    if i == 0 then
      snd:play("boop")
      key:update("Stats")
      val:update("none")
    end
  end

  statKind = alert:findObject("statKind")
  showStats(true)

-- find text
end

function scene:show(event)
  local phase = event.phase
  local params = event.params or {}
  if phase == "will" then
    callback = params.callback

    transition.from(alert, { time = 666, delay = 0, transition = easing.outQuad,
        y = display.screenOriginY - alert.designedHeight,
        onComplete = function ()
          Runtime:addEventListener("key", key)
          Runtime:addEventListener("ui", ui)
        end})
    snd:play("swoosh")
  elseif phase == "did" then

  end
end

function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then
    snd:play("swooshClose")
    Runtime:removeEventListener("key", key)
    Runtime:removeEventListener("ui", ui)
  elseif phase == "did" then
    if callback then callback() end
  end
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")

return scene