-- screenTap

local M = {}
local composer = require "composer"

function M.new()

  -- touch anywhere else to stay
  local instance = display.newRect(display.screenOriginX, display.screenOriginY, display.actualContentWidth-8, display.actualContentHeight-8)

  instance.alpha = 0.0
  instance.isHitTestable = true
  instance.enabled = false

  function instance:touch(event)
    if event.phase == "began" and self.enabled then
      display.getCurrentStage():setFocus(self)
      self.isFocus = true
    elseif event.phase == "ended" and self.enabled and self.isFocus then
      display.getCurrentStage():setFocus(nil)
      self.isFocus = false
      Runtime:dispatchEvent({name = "ui", buttonName = "tap", tag = "inventory", phase = "released" }) 
    elseif event.phase == "cancelled" then
      display.getCurrentStage():setFocus(nil)
      self.isFocus = false
    end
    return self.enabled
  end
  instance:addEventListener("touch")

  function instance:activate()
    print("Activating tap")
    instance:toBack()
    self.enabled = true
    self:addEventListener("touch", self.touchpad)
  end

  function instance:deactivate()
    print("Dectivating tap")
    self:removeEventListener("touch", self.touchpad)
    self:touch({phase = "cancelled"})
    self.enabled = false
  end

  instance:activate()
  return instance
end

return M