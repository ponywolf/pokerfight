-- tiled Plugin template

-- Use this as a template to extend a tiled object with functionality
local M = {}
local fx = require "com.ponywolf.ponyfx"
local tiled = require "com.ponywolf.ponytiled"
local json = require "json"

function M.new()

  local statsData = json.decodeFile(system.pathForFile("gfx/statsHero.json"))  -- load from json export
  local instance = tiled.new(statsData, "gfx")
  instance:centerAnchor()
  instance:extend("label")
  instance.isVisible = false

  function instance:open()
    self.x = self.opened
    transition.from(self, { x = display.screenOriginX - self.designedWidth, time = 333, transition = easing.outBounce })
    self.isVisible = true
  end

  function instance:close()
    transition.to(self, { x = display.screenOriginX - self.designedWidth, time = 333, transition = easing.outBounce })
    self.isVisible = false
  end
  
  function instance:setHealth(hp, max)
    self:findObject("hp"):update((hp or "0") .. "/" .. (max or "25"))
    self:findObject("healthBar").xScale = math.max(0.01, (hp or 0) / (max or 25))
  end

  function instance:setMana(mana, max)
    self:findObject("mana"):update((mana or "0") .. "/" .. (max or "25"))
    self:findObject("manaBar").xScale = math.max(0.01, (mana or 0) / (max or 25))
  end

  return instance
end

return M