-- Data store / Global Replacement 
-- by Michael Wilson / Ponywolf
-- Code init 9/9/2014

-- Base 64 Encoding (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

-- Usage: settings = require "settings"
local M = {}
--local translate = require "com.ponywolf.translator"
local snap = require "com.ponywolf.snap"
local json = require "json"
local isSimulator = system.getInfo("environment") == "simulator"

-- Anything added to "store" gets saved/loaded
-- when M:save() or M:load() is called

local store = {}
M.session = {} -- a place to keep settings for this session

-- icon for saving
local saveIcon = display.newImageRect("gfx/disk.png",32,32)
if not saveIcon then
  saveIcon = display.newCircle(0,0,96/2)
end
local saveTimer
local saveDelay = 333
saveIcon.alpha = 0

local iCloud = {} --system.getInfo("platform") == "ios" and require("plugin.iCloud") or {}
local function iCloudListener(event)
  if not M:get("iCloud") then return false end
  print ("iCloud", event.type, event.isError, event.keys)
  if (event.type == "serverChange") and M:get("iCloud") then
    M:load() -- reload the data
  end
end

if iCloud.identityToken then
  iCloud.setKVSListener(iCloudListener)
end

-- set a store key, value
function M:set(k,v)
  store[k] = v
  print(k,v)
end

-- erase a store key, value
function M:wipe(k,v)
  store[k] = nil
end

-- get a store key, default
function M:get(k,default)
  if store[k] == nil then
    store[k] = default -- or false
  end
  return store[k] 
end

-- Set to false to store as plain JSON or leave true
-- and D.store will be stored as a BASE64 string
-- making it difficult for end users to edit the
-- data needed by your program.
M.base64 = not isSimulator 

-- Create your own 64 character string (with no
-- repeats) for even more "security" or randomize
-- "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
-- with http://textmechanic.com/String-Randomizer.html
local b = "qJPFWtcNYZH6Va4DKGCzBXgLxyfeOM/SbIQEApRdThr+9k12m8vw7is35onju0lU"

-- Base64 encoding
local function enc(data)
  if not type(data) == "table" then return false end
  return ((data:gsub('.', function(x) 
          local r,b='',x:byte()
          for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
          return r;
        end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
      end)..({ '', '==', '=' })[#data%3+1])
end

-- Base64 decoding
local function dec(data)
  if not data then return false end
  if not (type(data) == "string") then return false end
  data = string.gsub(data, '[^'..b..'=]', '')
  return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
      end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
      end))
end

function M:save(now, filename)
  print ("Caching save data")
  local function doSave()
    filename = filename or (M.base64 and "data.dat" or "data.json")
    local path = system.pathForFile(filename, system.DocumentsDirectory)
    local file = io.open(path, "w")
    if file then
      store._saveTime = os.time()
      local contents = isSimulator and json.prettify(store) or json.encode(store)
      if self.base64 then contents = enc(contents) end
      if contents then
        file:write(contents)
      end
      io.close(file)
      -- iCloud sync
      if iCloud.identityToken and iCloud.identityToken() and M:get("iCloud") then --we're logged in/enabled
        iCloud.set(filename, contents)
        iCloud.synchronize()
      end
      print ("Writing to disk")  
      saveTimer = nil
      return true
    else
      print ("Save Failed")
      return false
    end
  end
  if now then
    doSave()
  else
    -- show icon
    transition.cancel(saveIcon)
    snap.safe(saveIcon, "lowerright", 8)
    saveIcon:toFront()
    saveIcon.alpha = 1.0
    transition.to(saveIcon, {alpha = 0, delay = saveDelay, time = 333 })
    -- cache saving    
    if saveTimer then timer.cancel(saveTimer) end
    saveTimer = timer.performWithDelay(saveDelay, doSave)
  end
end

function M:clear()
  store = nil
  M.session = nil
  collectgarbage()
  store = {}
  M.session = {}
  M:set("firstRun", os.time())
  M:save(true)
end

local function luafy(tbl)
  if type(tbl) ~= "table" then return tbl end
  local function isNumeric(str)
    return tonumber(str) and true or false
  end
  for k, v in pairs(tbl) do
    if v=="true" then
      tbl[k]=true
    elseif v=="false" then
      tbl[k]=false
    elseif isNumeric(v)==true then
      tbl[k]=tonumber(v)
    elseif v=="" then
      tbl[k]=nil            
    elseif type(v) == "table" then
      luafy(v)
    end
  end
  return tbl
end

function M:load(filename)
  filename = filename or (M.base64 and "data.dat" or "data.json")
  local path = system.pathForFile(filename, system.DocumentsDirectory)
  local file = io.open(path, "r")
  local cloudSave, localSave = {}, {}

  if file then
    -- read all contents of file into a string
    local contents = file:read("*a")
    if self.base64 then contents = dec(contents) end
    io.close(file)
    localSave = contents and luafy(json.decode(contents)) or {}
    -- iCloud sync
    if iCloud.identityToken and iCloud.identityToken() and M:get("iCloud") then --we're logged in/enabled
      -- Sync dialogs
      local function iCloudSyncIssue(event)
        if (event.action == "clicked") then
          local i = event.index
          if i == 1  then
            print("WARNING: Using local save, iCloud stays enabled...")
            store = localSave
            M:set("iCloud", true)
            M:save(true)
          elseif i == 2 then
            print("WARNING: Using iCloud save, iCloud enabled...")
            store = cloudSave
            M:set("iCloud", true)
            M:save(true)
            Runtime:dispatchEvent({name = "data", phase = "loaded", location = "iCloud"})
          elseif i == 3 then
            print("WARNING: Using local save, iCloud disbaled...")            
            store = localSave
            M:set("iCloud", false)
            M:save(true)
          end
        end
      end
      
      -- attempt cloud loading
      local data = iCloud.get(filename)
      data = dec(data) or "{}"
      cloudSave = data and luafy(json.decode(data)) or {}
      if (cloudSave._saveTime or 0) == (localSave._saveTime or 0) then
        print("Local and cloud saves are in sync...", (localSave._saveTime or 0), (cloudSave._saveTime or 0))
        store = json.decode(dec(iCloud.get(filename)))
      elseif (cloudSave._saveTime or 0) > (localSave._saveTime or 0) then
        print("WARNING: Cloud save newer than local save...")
        native.showAlert(translate("iCloud Sync Issue", "SYNC_ISSUE"),
          translate("Your level {1} iCloud save is newer than your level {2} local save. Which save do you want to keep?",
            "icloud_newer", { ["{1}"] = cloudSave.progressLevel, ["{2}"] = localSave.progressLevel}),
--          { translate("Local"), translate("iCloud"), translate("Disable Sync") },
          { "Local", "iCloud", "Disable Sync" },
          iCloudSyncIssue)
        store = json.decode(dec(iCloud.get(filename)))
      elseif (cloudSave._saveTime or 0) < (localSave._saveTime or 0) then
        print("WARNING: Local save newer than cloud save...")
        native.showAlert(translate("iCloud Sync Issue", "SYNC_ISSUE"),
          translate("Your level {2} local save is newer than your level {1} iCloud save. Which save do you want to keep?",
            "local_newer", { ["{1}"] = cloudSave.progressLevel, ["{2}"] = localSave.progressLevel }),
--          { translate("Local"), translate("iCloud"), translate("Disable Sync") },
          { "Local", "iCloud", "Disable Sync" },
          iCloudSyncIssue)
        store = localSave
      end
      --iCloud.synchronize()
    else
      -- not using iCloud
      store = localSave
    end
    print(M:get("firstRun",0), "Successful loading, Level", store.progressLevel, M:get("firstRun",0) > 0)
    if not (M:get("firstRun",0) > 0) then
      print("WARNING: Savegame found, but firstRun not found... possible corrupted save file")
      -- try loading backup
      if filename ~= (M.base64 and "_backup.dat" or "_backup.json") then
        M:load(M.base64 and "_backup.dat" or "_backup.json")
      end
    else 
      -- successful load, backing up
      M:backup()
    end
  else
    -- try loading backup
    if filename ~= (M.base64 and "backup.dat" or "backup.json") then
      M:load(M.base64 and "backup.dat" or "backup.json")
    end
  end
  if not (M:get("firstRun",0) > 0) then
    print ("WARNING: Starting fresh save file")
    store = {}
    M:set("firstRun", os.time())
    M:save(true)
    return
  end
  return -- truly nothing to load
end

function M:backup(filename)
  filename = M.base64 and "backup.dat" or "backup.json"
  local path = system.pathForFile(filename, system.DocumentsDirectory)
  local file = io.open(path, "w")
  if file then
    local contents = json.encode(store)
    if self.base64 then contents = enc(contents) end
    if contents then
      file:write(contents)
    end
    io.close(file)
    print ("Writing backup to disk")  
    return true
  else
    print ("Backup save Failed")
    return false
  end
end

-- Print the store table 

function M:dump()
  print(json.prettify(store))
end

return M
