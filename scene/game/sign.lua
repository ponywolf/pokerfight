-- tiled Plugin template

-- Use this as a template to extend a tiled object with functionality
local M = {}
local composer = require "composer"
local fx = require "com.ponywolf.ponyfx"

function M.new(instance)

  if not instance then error("ERROR: Expected display object") end
  local scene = composer.getScene("scene.game")

  function instance:collision(event)
    local phase = event.phase
    local other = event.other
    --print(phase, other.name)
    if phase == "began" then
      if other.name == "hero" then
        -- hero touched us
        if self.confirm then -- show an emote, delayed action
          other.emote:show("read")
          scene.tapHelp.isVisible = true
          other.action = "dialog"
          other.params = { text = self.text or "Unknown", background = self.background }
        else -- just do it
          other:execute("dialog", { text = self.text or "Unknown", background = self.background })
        end
      end
    elseif phase == "ended" then
      if other.name == "hero" then
        -- hero left us
        if self.confirm then -- show an emote
          --print("Left", self.type, self.name)
          other.emote:hide()
          scene.tapHelp.isVisible = false
          other.action = nil
          other.params = nil
        end
      end
    end
  end

  function instance:preCollision(event)
    local other = event.other

  end

-- Add our collision listeners
  instance:addEventListener("preCollision")
  instance:addEventListener("collision")

  return instance
end

return M