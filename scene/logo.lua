-- Requirements
local composer = require "composer"
local spine = require "com.spine-corona.spineWrapper"
local snap = require "com.ponywolf.snap"


-- Vars/Objects local to scene
local scene = composer.newScene()
local isSimulator = "simulator" == system.getInfo("environment")
local movie, logo

-- Called when the app's data has been updated
local function onData(event)
  composer.gotoScene("scene.menu", { params = { sync = true }})
end

-- Create your scene here
function scene:create(event)
  local view = self.view

  local filename = "skeleton"
  local path = "scene/logo/spine/logo/"

  -- load animation

  local function onEvent(event)
    if event.phase == "completed" then
      composer.gotoScene("scene.game")
    end
    if event.phase == "started" then
      timer.performWithDelay(133, function () movie.group.isVisible = true end)
    end    
  end

  movie = spine.new(filename, path, {
      scale = 0.333,
      onEvent = onEvent
    })
  view:insert(movie.group)
  snap(movie.group)
  movie.group:translate(0,16)   
  movie.group.isVisible = false
end

-- Add enterFrame on show
function scene:show(event)
  local view = self.view
  local phase = event.phase
  if (phase == "will") then

  elseif (phase == "did") then
    movie:play("animation",false)
  end
end

-- Remove enterFrame on hide
function scene:hide(event)
  local phase = event.phase
  if (phase == "will") then

  end
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)

return scene