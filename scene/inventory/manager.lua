-- inventory manager

local settings = require "com.ponywolf.settings"
local stats = require "com.ponywolf.ponystats"
local json = require "json"

local M = {}

local defaultInventory = '{"rusty":1,"mana":1,"health":1,"woody":1}'
local defaultSlots = '{"attack":false,"defend":false,"left":false,"right":false}'

print(json.encode(defaultInventory))

function M.load()
  M.inventory = json.decode(settings:get("inventory", defaultInventory))
  M.slots = json.decode(settings:get("slots", defaultSlots))
end

function M.save()
  settings:set("inventory", json.encode(M.inventory))
  settings:set("slots", json.encode(M.slots))
end

function M.reset()
  M.inventory = {}
  M.slots = {}
  M.save()
end

function M.add(k,v)
  if tonumber(v) or v == nil then
    v = v or 1
    print ("Adding", v ,"to inventory", k)
    -- add item to persistent
    M.inventory[k] = (M.inventory[k] or 0) + v
    return M.inventory[k]
  else
    print ("WARNING: Unable to add", v ,"to inventory", k)
    return false
  end
end

function M.count(k)
  if M.inventory[k] then
    return M.inventory[k]
  else
    print ("WARNING: No such item as", k)
    return 0
  end
end

function M.use(k)
   M.inventory[k] = tonumber(M.inventory[k]) or 0
  if M.inventory[k] then
    if M.inventory[k] >= 1 then
      M.inventory[k] = M.inventory[k] - 1
      return true
    end 
  else
    print ("WARNING: No such stat as", k)
    return 0
  end
end

function M.dump()
  print("--INVENTORY---------------------")
  for k,v in pairs(M.inventory) do
    print (k,v)
  end
  print ("--STORE--------------------")
  print (settings:get("inventory"))
end


--local function stat(event)
--  if event.phase == "inc" or event.phase == "add" then
--    return M.inc(event.stat, event.val or event.num)
--  elseif event.phase == "save" then
--    M.save()
--    return true
--  end
--end

--Runtime:addEventListener("stat",stat)

M.load()
M.dump()

return M