-- template

local M = {}

function M.new()

  -- create instance
  local instance = display.newGroup()

  function instance:something(...)

  end

  -- functions
  local function enterFrame(event)

  end  

  function instance:finalize()
    -- On remove, cleanup instance, or call directly for non-visual
    Runtime:removeEventListener("enterFrame", enterFrame)

  end

-- Add a finalize listener (for display objects only, comment out for non-visual)
  instance:addEventListener("finalize")

-- Add our enterFrame listener
  Runtime:addEventListener("enterFrame", enterFrame)

  return instance
end

return M