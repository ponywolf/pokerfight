-- ponyrogue 0.2

-- From scratch lua algrothims for procedual tile maps
local color = require "com.ponywolf.ponycolor"

local M = {}

local function array(w,h)
  local data = {}
  for x = 0, (w or 0) + 2 do
    data[x] = {}
    for y = 0, (h or 0) + 3 do
      data[x][y] = 0
    end
  end
  return data
end

local function dump(data, w, h)
  for y = 1, h do
    local str = ""
    for x = 1, w do
      if data[x][y] == 1 then
        str = str .. "[ ]"
      else
        str = str .. ": :"
      end
    end
    print(str)
  end
end

local function cleared(data, w, h)
  local total = 0
  for y = 1, h do
    for x = 1, w do
      total = total + ((data[x][y] == 0) and 1 or 0)
    end
  end
  return total / (w*h)
end

local function isClear(data, x, y, w, h, margin)
  for i = x, w+x do
    for j = y, h+y do
      if (data[i] and data[i][j]) then
        if data[i][j] == 0 then return false end
        if i <= margin then return false end
        if j <= margin then return false end
        if i >= data.w-margin then return false end
        if j >= data.h-margin then return false end
      end
    end
  end
  return true
end

local function rect(data, x, y, w, h, val)
  val = val or 1
  for i = x, w+x do
    for j = y, h+y do
      if (data[i] and data[i][j]) then
        data[i][j] = val or 1
      end
    end
  end
end

local function box(data, x, y, w, h, val)
  val = val or 1
  for i = x, w+x do
    for j = y, h+y do
      local edge = (i==x or j==y or i==w+x or j==y+h)
      if (data[i] and data[i][j] and edge) then
        data[i][j] = val or 1
      end
    end
  end
end

local function column(data, x, y, w, h)
  local hw, hh = math.floor(w/4), math.floor(h/4)
  rect(data, x+hw-1, y+hh-1, 1, 1, 1)
  rect(data, x+w-hw, y+h-hh, 1, 1, 1)
  rect(data, x+hw-1, y+h-hh, 1, 1, 1)
  rect(data, x+w-hw, y+hh-1, 1, 1, 1)
end

local function tower(data, x, y, w, h)
  rect(data, x-1, y-1, 1, 1, 0)
  rect(data, x+w, y-1, 1, 1, 0)
  rect(data, x-1, y+h, 1, 1, 0)
  rect(data, x+w, y+h, 1, 1, 0)
end

local function connect(data, x1, y1, x2, y2, val)
  local dx = x1 - x2
  local dy = y1 - y2
  local xd = dx < 0 and 1 or -1
  local yd = dy < 0 and 1 or -1

  if math.random() < 0.5 then
    for i = x1, x2, xd do
      data[i][y1] = val or 0
    end
    for i = y1, y2, yd do
      data[x2][i] = val or 0
    end
  else
    for i = y1, y2, yd do
      data[x2][i] = val or 0
    end
    for i = x1, x2, xd do
      data[i][y1] = val or 0
    end
  end
end

function M.new(options)
  options = options or {}
  local w,h = options.width or 32, options.height or 32
  local method = options.method or "roguejack"
  local roomW, roomH = options.roomWidth or 6, options.roomHeight or 6
  local seed = options.seed or os.time()
  print("SEED:", seed)
  local margin = options.margin or 2
  local digPercent = options.digPercent or 0.333
  math.randomseed(seed)

  local map
  local tries = 10
  repeat
    map = {}
    map.width, map.height = w, h
    map.data = array(w,h)
    map.data.w = w
    map.data.h = h
    map.room = {}
    tries = tries - 1
    local timeout = 10000
    if method == "square" then
      rect(map.data, 0, 0, w, h) -- fill
      local i, spacing = 1, 1
      repeat
        timeout = timeout - 1
        local rw, rh, dx, dy
        repeat
          timeout = timeout - 1
          if (timeout < 0) then print ("Map timeout:", 10 - tries) end
          rw = math.random(roomW - 2, roomW + 2)
          rh = math.random(roomH - 2, roomH + 2)
          if math.random(1) == 0 then
            dx, dy = math.min(w - rw - 4, (dx or 1) + 3), math.random(h - rh - 4) + 3
          else
            dx, dy = math.random(w - rw - 4) + 3, math.min(h - rh - 4, (dy or 1) + 3)
          end
        until isClear(map.data, dx - spacing, dy - spacing, rw + (spacing * 2), rh + (spacing * 2), margin) or (timeout < 0)
        if timeout > 0 then
          rect(map.data, dx, dy, rw, rh, 0)
          map.room[i] = { x = math.floor(dx + rw / 2), y = math.floor(dy + rh / 2), top = dy, left = dx, width = rw, height = rh }
          if i > 1 then
            connect(map.data, map.room[i-1].x, map.room[i-1].y, map.room[i].x, map.room[i].y)
          end
          i = i + 1
        end
      until cleared(map.data, w, h) > digPercent or (timeout < 0)
      for j = 1, #map.room do
        local dy, dx = map.room[j].top, map.room[j].left
        local rw, rh = map.room[j].width, map.room[j].height
        if math.random() > 0.5 or j == 1 then
          if rw >= 6 and rh >= 6 then
            --column(map.data, dx, dy, rw, rh)
          else
            --tower(map.data, dx, dy, rw, rh)
          end
        end
      end
    elseif method == "roguejack" then
      rect(map.data, 0, 0, w, h) -- fill
      local i, spacing = 1, 1
      repeat
        timeout = timeout - 1
        local rw, rh, dx, dy
        repeat
          timeout = timeout - 1
          if (timeout < 0) then print ("Map timeout:", 10 - tries) end
          rw = math.random(roomW - 2, roomW + 2)
          rh = math.random(roomH - 2, roomH + 2)
          if math.random(1) == 0 then
            dx, dy = math.min(w - rw - 4, (dx or 1) + 3), math.random(h - rh - 4) + 3
          else
            dx, dy = math.random(w - rw - 4) + 3, math.min(h - rh - 4, (dy or 1) + 3)
          end
        until isClear(map.data, dx - spacing, dy - spacing, rw + (spacing * 2), rh + (spacing * 2), margin) or (timeout < 0)
        if timeout > 0 then
          rect(map.data, dx, dy, rw, rh, 0)
          map.room[i] = { x = math.floor(dx + rw / 2), y = math.floor(dy + rh / 2), top = dy, left = dx, width = rw, height = rh }
          if i > 1 then
            connect(map.data, map.room[i-1].x, map.room[i-1].y, map.room[i].x, map.room[i].y)
          end
          i = i + 1
        end
      until cleared(map.data, w, h) > digPercent or (timeout < 0)
      for j = 1, #map.room do
        local dy, dx = map.room[j].top, map.room[j].left
        local rw, rh = map.room[j].width, map.room[j].height
        if math.random() > 0.5 or j == 1 then
          if rw >= 6 and rh >= 6 then
            --column(map.data, dx, dy, rw, rh)
          else
            tower(map.data, dx, dy, rw, rh)
          end
        end
      end
    end
  until (cleared(map.data, w, h) > (digPercent/1.75)) or (tries < 0)


  --dump(map.data, w, h)

  -- render a map via spritesheet
  function map.simpleRender(options)

    options = options or {}

    local w, h = map.width, map.height
    local instance = options.parent or display.newGroup()
    local size, spacing = options.size or 16, options.spacing or 16

    local sheet = options.sheet
    local index = options.index or 1

    for y = 2, h-1 do
      for x = 2, w-1 do
        if map.data[x][y] == 1 and ((map.data[x-1][y]==0) or (map.data[x+1][y]==0) or (map.data[x][y-1]==0) or (map.data[x][y+1]==0)) then
          local node
          if sheet then
            node = display.newImageRect(instance, sheet, index, size, size)
          else
            node = display.newRect(instance, (x-w/2) * spacing, (y-h/2) * spacing, 16, 16)
          end
          node.x, node.y = (x-w/2) * spacing, (y-h/2) * spacing
          node._x, node._y = node.x, node.y
        end
      end
    end
    return instance
  end

  function map.render(options)

    options = options or {}

    local w, h = map.width, map.height
    local instance = options.parent or display.newGroup()
    local size, spacing = options.size or 16, options.spacing or 16
    local wallColor, floorColor = options.wallColor or "#ccc", options.floorColor or "#333"
    local data = map.data
    local rooms = map.room

    local sheet = options.sheet

    function map.findRoom(x, y)
      for j = 1, #rooms do
        local ry, rx = rooms[j].top, rooms[j].left
        local rw, rh = rooms[j].width, rooms[j].height
        if x >= rx-1 and x <= (rx + rw + 1) and y >= ry-1 and y <= (ry + rh + 1) then
          return j
        end
      end
    end

    -- Pass 1
    for x = 1, w do
      for y = 1, h do
        -- cases of walls
        local c = (data[x][y] > 0) -- center
        local n = (data[x][y-1] > 0) -- north
        local s = (data[x][y+1] > 0) -- south
        local e = (data[x-1][y] > 0) -- east
        local w = (data[x+1][y] > 0) -- west
        local ne = (data[x-1][y-1] > 0) -- north
        local se = (data[x-1][y+1] > 0) -- south
        local nw = (data[x+1][y-1] > 0) -- east
        local sw = (data[x+1][y+1] > 0) -- west
        if n and s and e and w and ne and nw and se and sw and c then data[x][y] = 99 end -- remove
      end
    end

    -- Pass 2
    for x = 1, w do
      for y = 1, h do
        if data[x][y] == 99 then data[x][y] = -1 end -- remove
        if x==1 or y ==1 or x==w or y==h then data[x][y] = -1 end
      end
    end

    -- Pass 3
    for x = 1, w do
      for y = 1, h do
        local wallType = 0
        -- cases of walls
        local c = not (data[x][y] == 0) -- center
        local n = not (data[x][y-1] == 0) -- north
        local s = not (data[x][y+1] == 0) -- south
        local e = not (data[x+1][y] == 0) -- east
        local w = not (data[x-1][y] == 0) -- west

        -- remove T stubs
--        local connections = ((data[x][y-1] > 0) and 1 or 0) + ((data[x][y+1] > 0) and 1 or 0) + ((data[x+1][y] > 0) and 1 or 0) + ((data[x-1][y] > 0) and 1 or 0)
--        if connections == 3 then
--          data[x][y] = 0
--        end

        -- free corners
        local ne = (data[x+1][y-1] == 0) -- north
        local se = (data[x+1][y+1] == 0) -- south
        local nw = (data[x-1][y-1] == 0) -- east
        local sw = (data[x-1][y+1] == 0) -- west

        -- cases of walls
        if c then
          if w then wallType = 38 end
          if e then wallType = 43 end
          if s then wallType = 21 end
          if n then wallType = 18 end
          if e and w then wallType = 37 end
          if n and s then wallType = 11 end
          if s and w then wallType = 41 end
          if n and w then wallType = 48 end
          if s and e then wallType = 40 end
          if n and e then wallType = 47 end
          if s and e and w then wallType = 30 end
          if e and n and s then wallType = 20 end
          if n and w and s then wallType = 15 end
          if n and e and w then wallType = 2 end
          if n and s and e and w then
            if se then wallType = 1 end
            if sw then wallType = 6 end
            if ne then wallType = 29 end
            if nw then wallType = 34 end
          end
        end

        local cell
        local empty = (data[x][y] < 0)
        if wallType > 0 and wallType < 50 and not empty then
          cell = display.newImageRect(instance, sheet, wallType, size, size)
          cell:setFillColor(unpack(color.hex2rgb(wallColor)))
          cell.name = "wall"
          cell.type = "solid"
        elseif wallType == 0 and map.findRoom(x, y) then -- floor
          cell = display.newImageRect(instance, sheet, 17, size, size)
          cell.name = "floor"
          cell.type = "decor"
        end

        -- put in group
        if cell then
          instance:insert(cell)
          cell.x, cell.y = x*spacing, y*spacing
        end
      end
    end

    return instance

  end

  return map
end

return M