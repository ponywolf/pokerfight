-- camera control

local M = {}

local centerX, centerY = display.contentCenterX, display.contentCenterY

function M.new()
  local instance = display.newGroup()

  function instance:center()
    -- centers the world on screen
    instance.x, instance.y = centerX, centerY
  end

  function instance:reset()
    -- places the world on screen at 0,0
    instance.x, instance.y = 0,0
  end

  function instance:centerObj(obj, tween)
    -- moves the world, so the specified object is on screen
    if not obj then return false end

    -- easiest way to scroll a map based on a character
    -- find the difference between the hero and the display center
    -- and move the world to compensate
    local objx, objy = obj:localToContent(0,0)
    objx, objy = centerX - objx , centerY - objy
    if tween then
      self.x, self.y = self.x + objx/4, self.y + objy/4
    else
      self.x, self.y = self.x + objx, self.y + objy
    end
  end

  function instance:alignToGrid(size)
    -- snaps everything in the group to the grid spacing
    size = size or 16

    local skipLayers = { hero = true, monster = true, decor = true, overlay = true, number = true }

    for i = 1, self.numChildren do
      if self[i] and not skipLayers[self[i].name] and self[i].numChildren then
        self.alignToGrid(self[i], size)
      elseif self[i] then
        self[i].x = math.floor((self[i].x / size) * size)
        self[i].y = math.floor((self[i].y / size) * size)
        if self[i].type == "decor" then -- move to botton layer
          instance.decor:insert(self[i])
        end
      end
    end
  end

  function instance:objectsAt(x, y, objs)
    -- returns the objects in the group at the x,y asked within size
    local size = 16
    local sx, sy = math.floor(x / size), math.floor(y / size)
    objs = objs or {}

    local skipLayers = { hero = true, monster = true, decor = true, overlay = true, exit = true }

    for i = 1, self.numChildren do
      if self[i] and not skipLayers[self[i].name] and self[i].numChildren then
        self.objectsAt(self[i], x, y, objs)
      elseif math.floor(self[i].x / size) == sx and math.floor(self[i].y / size) == sy  then
        if self[i].name or self[i].type then
          objs[#objs+1] = self[i]
        end
      end
    end
    return #objs and objs or false
  end

  function instance:count(name)
    -- returns the count of name objects in the group
    local total = 0
    for i = 1, self.numChildren do
      if self[i].name == name then
        total = total + 1
      end
    end
    print ("FOUND:", total, name)
    return total
  end

  function instance:dump()
    for i = 1, self.numChildren do
      if self[i].numChildren then
        self.dump(self[i])
      else
        print ("dump ->", self[i].name, self[i].type, self[i].x, self[i].y)
      end
    end
  end

  function instance:light(x, y, near, far, reset)
    -- returns the objects in the group at the x,y asked within size
    local size = 16

    local function distance(x1,y1,x2,y2)
      return math.sqrt((math.abs(x1-x2)*math.abs(x1-x2))+(math.abs(y1-y2)*math.abs(y1-y2)))
    end

    local skipLayers = { hero = true, monster = true, exit = true, number = true }

    for i = 1, self.numChildren do
      if self[i] and not skipLayers[self[i].name] and self[i].numChildren then
        self.light(self[i], x, y, near, far)
      else
        local sx,sy = math.floor(self[i].x / size), math.floor(self[i].y / size)
        local dist = distance(x,y,sx,sy)
        if reset then self[i].discovered = false end
        if dist < near then
          self[i].alpha = 1
          self[i].discovered = self[i].name == "monster" and false or true
        elseif dist > far and not self[i].discovered then
          self[i].alpha = 0
        elseif not self[i].discovered then
          self[i].alpha = 1 - (dist / far)
        end
      end
    end
    self.lastX, self.lastY = x, y
    return
  end

  function instance:setVisibility(near, far, reset)
    near = math.max(near,2)
    far = math.max(far,4)
    self:light(self.lastX, self.lastY, near, far, reset)
  end

  instance.decor = display.newGroup()
  instance.decor.name = "decor"
  instance:insert(instance.decor)

  instance.overlay = display.newGroup()
  instance.overlay.name = "overlay"
  instance:insert(instance.overlay)

  function instance:update()
    instance.decor:toBack()
    instance.overlay:toFront()
  end

  return instance
end

return M