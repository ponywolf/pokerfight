
-- The default magnification sampling filter applied whenever an image is loaded by Corona.
-- Use "nearest" with a small content size to get a retro-pixel look
display.setDefault("magTextureFilter", "nearest")
display.setDefault("minTextureFilter", "linear")

local app = require "app"
local composer = require "composer"
composer.recycleOnSceneChange = true

local settings = require "com.ponywolf.settings"
settings:load()

local device = require "com.ponywolf.device"

-- Removes bottom bar on Android
if device.isAndroid then
  if system.getInfo("androidApiLevel") and system.getInfo("androidApiLevel") < 19 then
    native.setProperty("androidSystemUiVisibility", "lowProfile")
  else
    native.setProperty("androidSystemUiVisibility", "immersiveSticky")
  end
end

if device.isiOS then -- don't turn off background music, remove status bar on iOS
  display.setStatusBar(display.HiddenStatusBar)
  native.setProperty("prefersHomeIndicatorAutoHidden", true)
  native.setProperty("preferredScreenEdgesDeferringSystemGestures", true)
  audio.setSessionProperty(audio.MixMode, audio.AmbientMixMode)
end

-- Audio/Music settings
local snd = require "com.ponywolf.ponysound"
local musicVolume = settings:get("musicVolume", 0.5)
local volume = settings:get("volume", 0.75)
snd:batch("click", "attack", "attacked", "heal", "swoosh", "swooshClose", "boop", "coin", "stairs", "step", "start", "equip", "levelup", "potion", "junk", "win", "bolt", "sword")
snd:setMusicVolume(musicVolume)
snd:setVolume(volume)
snd:playMusic()
if musicVolume > 0 then
  snd:resumeMusic()
else
  snd:pauseMusic()
end


if device.isSimulator then
  -- show FPS
  local visualMonitor = require("com.ponywolf.visualMonitor")
  local visMon = visualMonitor:new()
  visMon.isVisible = false

  local function key(event)
    local phase = event.phase
    local name = event.keyName
    if phase == "up" then
    elseif name == "f11" then
      visMon.isVisible = not visMon.isVisible
    elseif name == "f12" then
      device.screenshot()
    end
  end
  Runtime:addEventListener("key", key)
end

-- Android back button
if device.isAndroid then
  local function key(event)
    local keyName = event.keyName
    if "back" == keyName and event.phase == "down" then
      Runtime:dispatchEvent({name = "ui", buttonName = "back", phase = "released", tag = "android"})
      return true
    else
      return false
    end
  end
  Runtime:addEventListener("key", key)
end

require "com.ponywolf.transition"

local stats = require "com.ponywolf.ponystats"
stats.inc("sessions")

local function onSystemEvent(event)
  local name, kind = event.name, event.type
  if kind == "applicationResume" then
    settings:load()
    local musicVolume = settings:get("musicVolume", 0.5)
    local volume = settings:get("volume", 0.75)
    snd:setMusicVolume(musicVolume)
    snd:setVolume(volume)
    --settings:save()
    if musicVolume > 0 then
      snd:resumeMusic()
    else
      snd:pauseMusic()
    end
  elseif kind == "applicationSuspend" then
    snd:pauseMusic()
    stats.save()
    settings:save(true)
    settings:backup()
  elseif kind == "applicationExit" then
    settings:save(true)
    settings:backup()
  end
end
Runtime:addEventListener("system", onSystemEvent)

-- Broadcast Ad Events
--local unityads = {}
--if not isPremium then
--  unityads = require "plugin.unityads"
--  function unityads.adListener(event)
--    if event.phase == "init" and not event.isError then  -- Successful initialization
--      unityads.active = true
--      print ("Unity Ads: Successful initialization")
--      unityads.setHasUserConsent(true)
--    end
--    --local json = require "json"
--    --print(json.prettify(event))
--    Runtime:dispatchEvent(event)
--  end

--  -- Initialize the unityads plugin
--  local gameId = isiOS and "xxxxxxx" or "xxxxxxx"
--  unityads.init(unityads.adListener, { gameId=gameId }) --, testMode = true })
--else
--  unityads.active = false
--end

math.randomseed(os.time())

-- Steam
local function checkLogin()
  local steamworks = require "plugin.steamworks"
  if not steamworks.isLoggedOn then
    local function onAlertHandler( event )
      if event.action == "clicked" then
        if event.index == 1 then
          system.openURL( "steam:" )
          native.requestExit()
        else
          composer.gotoScene( "scene.logo", { })
        end
      end
    end
    local message = "This game was designed to be played from the Steam Launcher."
    native.showAlert( "Warning", message, { "Launch Steam", "Ignore" }, onAlertHandler )
    return
  else
    composer.gotoScene( "scene.logo", { })
  end
end

local function start()
  composer.gotoScene("scene.logo", { params={ } })
end

if device.isSimulator then
  require "com.ponywolf.joykey"
  composer.gotoScene("scene.game", { params = { map = "town" } })
elseif device.isDesktop then
  display.fullscreen = settings:get("isFullscreen", true)
  if display.fullscreen then
    native.setProperty("windowMode", "fullscreen")
  else
    native.setProperty("windowMode", "normal")
  end
  require "com.ponywolf.joykey"
  if device.isSteam then
    checkLogin()
  else
    start()
  end
else
  start()
end