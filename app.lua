local M = {}

-- Global variables to "app"
M.lastPrice = "$1.99"

-- varaible sharing via get/set/clear
local store = {}

function M.setVariable(k,v)
  store[k] = v
end

function M.getVariable(k,v)
  if not store[k] then print("Variable doesn't exist", k) return false end
  return store[k]
end

function M.clearVariables(k,v)
  store = {}
end

M.buttonMap = {
  -- SteelSeries to xBox
  menu = "buttonStart",
  -- DualShock to xBox
  button1 = "buttonX",
  button2 = "buttonA",
  button3 = "buttonB",
  button4 = "buttonY",
  button5 = "leftShoulderButton1",
  button6 = "rightShoulderButton1",
  button7 = "leftTrigger",
  button8 = "rightTrigger",
  button9 = "buttonShare",
  button10 = "buttonOptions",
  button11 = "leftJoystickButton",
  button12 = "rightJoystickButton",
  button13 = "buttonStart",
  button14 = "buttonTap",
  -- WSAD map
  w = "up",
  s = "down",
  a = "left",
  d = "right",
}

local buttonMap = M.buttonMap

-- App wide key handler
M.key = {}
local function key(event)
  local phase = event.phase
  local name = event.keyName
  name = buttonMap[name] or name
  M.key[name or "none"] = (phase=="down")
end
Runtime:addEventListener("key", key)

return M