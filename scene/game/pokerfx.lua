-- game specific fx routines

local composer = require "composer"

local M = {}

function M.popText(object,text,delay)
  local instance = display.newGroup()
  local style = { parent = instance, text = text or 0, align = "center", font = "font/silver.ttf", fontSize = 14 }
  local scene = composer.getScene("scene.game")

  local black = display.newText(style)
  black:setFillColor(0)
  local white = display.newText(style)
  white:setFillColor(1)
  white:translate(-0.75,-0.75)

  scene.world:insert(instance)
  instance.x, instance.y = object.x, object.y - 10

  transition.to(instance, { delay = delay or 0, time = 666, y = instance.y - 12,
      transition = easing.outQuad, tag = "fx",
      onComplete = function ()
        transition.to(instance, { delay = 666, time = 333, alpha = 0,
            transition = easing.outQuad, onComplete = function ()
              display.remove(instance)
            end})
      end})
end

local victorySheetOptions = { width = 40, height = 40, numFrames = 120 }
local victory = graphics.newImageSheet("gfx/font/victory.png", victorySheetOptions)

function M.victoryText(text, onComplete)
  local instance = display.newGroup()
  instance.glyphs = {}
  local scene = composer.getScene("scene.game")
  text = text or "VICTORY"
  local half = text:len()/2
  local count = text:len()
  local spacing = 24

  for i = 1, count do
    local char = text:sub(i,i)
    local glyph = math.max(0, char:byte()-31)
    --print (char, glyph)
    instance.glyphs[i] = display.newImage(instance, victory, glyph)
    instance.glyphs[i].x, instance.glyphs[i] .y = 0
    instance.glyphs[i]:translate((i-0.5) * spacing - (half * spacing), 0)
    instance.glyphs[i]:toBack()
  end

  -- drop in
  for i = 1, count do
    transition.from(instance.glyphs[i], { delay = i * 66, time = 666, y = instance.y - display.contentCenterY,
        transition = easing.outBounce, tag = "fx",
        onComplete = function ()
          transition.to(instance.glyphs[i], { delay = 500, time = 333, alpha = 0,
              transition = easing.outQuad, 
              onComplete = function ()
                display.remove(instance)
                if i == count and onComplete then onComplete() end
              end})
        end})
  end
  
  return instance
end


local rankSheetOptions = { width = 20, height = 20, numFrames = 120 }
local rank = graphics.newImageSheet("gfx/font/rank.png", rankSheetOptions)

function M.rankText(text, onComplete)
  local instance = display.newGroup()
  instance.glyphs = {}
  local scene = composer.getScene("scene.game")
  text = text or "MISSED!"
  local half = text:len()/2
  local count = text:len()
  local spacing = 11

  for i = 1, count do
    local char = text:sub(i,i)
    local glyph = math.max(0, char:byte()-31)
    --print (char, glyph)
    instance.glyphs[i] = display.newImage(instance, rank, glyph)
    instance.glyphs[i].x, instance.glyphs[i] .y = 0
    instance.glyphs[i]:translate((i-0.5) * spacing - (half * spacing), 0)
    instance.glyphs[i]:toBack()
  end

  -- climb in
  for i = 1, count do
    transition.from(instance.glyphs[i], { delay = i * 33, time = 333, y = instance.y + display.contentCenterY/2,
        transition = easing.outElastic, tag = "fx",
        onComplete = function ()
          transition.to(instance.glyphs[i], { delay = 1000, time = 333, alpha = 0,
              transition = easing.outQuad, 
              onComplete = function ()
                display.remove(instance)
                if i == count and onComplete then onComplete() end
              end})
        end})
  end
  
  return instance
end

return M