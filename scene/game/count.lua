-- Score lib

-- Define module
local M = {}
local fx = require "com.ponywolf.ponyfx"
local colors = require "com.ponywolf.ponycolor"
local stroked = require "com.ponywolf.ponystroke"
local renderSteps = 0.5

local function strokedText(options)
  -- default options for instance
  options = options or {}
  local x = options.x or 0
  local y = options.y or 0
  local h = options.height
  local parent = options.parent
  options.height = nil
  options.x = 0
  options.y = 0

  -- new options 
  local color = options.color or { 1 }
  local strokeColor = options.strokeColor or { 0 }
  local strokeWidth = options.strokeWidth or 1

  -- create the main text
  local text = display.newText(options)
  text:setFillColor(unpack(color))

  --  create group to hold text/strokes
  local stroked = display.newGroup()
  if parent then parent:insert(stroked) end
  stroked:insert(text)
  stroked.strokes = {}
  stroked.unstroked = text

  -- draw the strokes
  for i = -strokeWidth, strokeWidth, renderSteps do
    for j = -strokeWidth, strokeWidth, renderSteps do
      if not (i == 0 and j == 0) then --skip middle
        options.x,options.y = i,j
        local stroke = display.newText(options)
        stroke:setTextColor(unpack(strokeColor))
        stroked:insert(stroke)
      end
    end
  end

  -- call this function to update the label
  function stroked:update(text)
    self.text = text
    for i=1, stroked.numChildren do
      stroked[i].text = text
    end
  end

  function stroked:setTextColor(...)
    stroked.unstroked:setFillColor(...)
  end

  function stroked:setAnchor(x, y)
    text.anchorX, text.anchorY = x or text.anchorX, y or text.anchorY
    for i=1, stroked.numChildren do
      stroked[i].anchorX, stroked[i].anchorY = x or text.anchorX, y or text.anchorY
    end
  end

  stroked:translate(x, y)
  text:toFront()
  return stroked
end

function M.new(options)

  -- Default options for instance
  options = options or {}
  local label = options.label or ""
  local after = options.after
  local align = options.align or "center"  
  local x, y = options.x or 0, options.y or 0
  local font = options.font or "font/MatchupPro.ttf"
  local size = options.size or 16
  local strokeWidth = options.strokeWidth or 1
  local color = options.color and colors.hex2rgb(options.color) or { 1 } 
  local strokeColor = options.strokeColor and colors.hex2rgb(options.color) or { 0 }

  local instance
  local num = options.score or 0
  local textOptions = { x = x, y = y, text = after and (num .. label) or (label .. " " .. num), font = font, fontSize = size,
    color = color, strokeColor = strokeColor}

  instance = strokedText(textOptions)

  if align == "right" then 
    instance:setAnchor(1)
  elseif align == "left" then
    instance:setAnchor(0)
  end
  instance.num = num
  instance.target = num

  function instance:add(points, max)
    if max then -- cap things like health
      if (instance:get() + points) > max then return false end
    end
    self.target = self.target + (points or 10)
    self.target = self.target < 0 and 0 or self.target
    local function countUp()
      local diff = math.ceil((self.target - self.num) / 12)
      self.num = self.num + diff
      if self.num > self.target then
        self.num = self.target
        timer.cancel(self.timer)
        self.timer = nil
      end
      self:update(after and (num .. label) or (label .. " " .. num))
    end
    if not self.timer then
      self.timer = timer.performWithDelay(30, countUp, -1)
    end
    return self.target
  end

  function instance:set(num)
    self.target = num or 0
    self.num = num or 0
    self:update(after and (num .. label) or (label .. " " .. num))
  end

  function instance:get() return self.target or 0 end

  function instance:damage(points)
    self.target = self.target - (points or 1)
    self.target = self.target < 0 and 0 or self.target
    self.num = self.target
    self:update(after and (num .. label) or (label .. " " .. num))
    return self.num
  end

  function instance:heal(points, max)
    if max then -- cap things like health
      if (instance:get() + points) > max then return false end
    end
    self.target = self.target + (points or 1)
    self.target = self.target < 0 and 0 or self.target
    self.num = self.target
    self:update(after and (num .. label) or (label .. " " .. num))
    return self.num
  end

  function instance:finalize()
    -- On remove, cleanup instance
    if instance and instance.timer then timer.cancel(instance.timer) end
  end

  instance:addEventListener("finalize")

  -- Return instance
  return instance
end

return M
