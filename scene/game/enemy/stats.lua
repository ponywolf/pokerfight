-- tiled Plugin template

-- Use this as a template to extend a tiled object with functionality
local M = {}
local fx = require "com.ponywolf.ponyfx"
local tiled = require "com.ponywolf.ponytiled"
local json = require "json"

function M.new()

  local statsData = json.decodeFile(system.pathForFile("gfx/statsEnemy.json"))  -- load from json export
  local instance = tiled.new(statsData, "gfx")
  instance:centerAnchor()
  instance:extend("label")
  instance.isVisible = false

  function instance:open(id)
    self.x = self.opened
    print("Getting",id,"stats")
    local faces = instance:listTypes("actor")
    for i = 1, #faces do
      faces[i].isVisible = faces[i].type == "actor"
      faces[i].isVisible = faces[i].name == (id or "trainer")
    end
    --self:findObject("enemyText"):update(name)
    transition.from(self, { x = display.actualContentWidth + self.designedWidth, time = 333, transition = easing.outBounce })
    self.isVisible = true
  end

  function instance:close()
    transition.to(self, { x = display.actualContentWidth + self.designedWidth, time = 333, transition = easing.outBounce })
    self.isVisible = false
  end

  function instance:setHealth(hp, max)
    self:findObject("hp"):update((hp or "0") .. "/" .. (max or "25"))
    self:findObject("health").xScale = math.max(0.01, (hp or 0) / (max or 25))
  end

  function instance:setDamage(min, max)
    self:findObject("damage"):update((min or "1") .. "-" .. (max or min or "1"))
  end

  function instance:setDefense(num)
    self:findObject("defense"):update("-" .. (num or "0") .. "%")
  end

  function instance:setBonus(bonus, num)
    -- bonus
    print("setting bonus", bonus, "to", num)
    local bonuses = self:listTypes("bonus")
    for i = 1, #bonuses do bonuses[i].isVisible = false end
    local value = self:findObject("bonusValue") or {}
    if bonus and self:findObject(bonus) then
      self:findObject(bonus).isVisible = true
      value.text = "+" .. (num or "0")
    end
  end

  return instance
end

return M