-- on screen key

local M = {}
local stage = display.getCurrentStage()

local function distance(x1,y1,x2,y2)
  --print(x1,y1,x2,y2)
  return math.sqrt((math.abs(x1-x2)*math.abs(x1-x2))+(math.abs(y1-y2)*math.abs(y1-y2)))
end

local length = 32

function M.new(instance)

  if not instance then error("ERROR: Expected display object") end

  instance.isHitTestable = true
  instance.alpha = 0.10
  instance.isVisible = false
  instance.frames = 0

  local function enterFrame()
    instance.frames = instance.frames + 1
  end

  -- touch code
  local x1, y1, x2, y2, sx, sy
  function instance:touch(event)
    local phase, name = event.phase, event.name
    if self.enabled == false then return false end
    -- press in animation
    if phase == "began" then
      if event.id then stage:setFocus(event.target, event.id) end
      self.isFocus = true
      instance.frames = 0
      instance.feedback = display.newGroup()
      x1, y1 = event.x, event.y
      sx, sy = x1, y1
      Runtime:addEventListener("enterFrame", enterFrame)
    elseif phase == "moved" and self.isFocus then
      if instance.frames < 45 then
        local line = display.newLine(sx, sy, event.x, event.y)
        line:setStrokeColor(1,1,1, 0.5)
        line.strokeWidth = 4
        instance.feedback:insert(line)
        transition.to(line, {alpha = 0, time = 166})
      end
      sx, sy = event.x, event.y
    elseif phase == "ended" or phase == "cancelled" then
      if event.id then stage:setFocus(nil, event.id) end
      Runtime:removeEventListener("enterFrame", enterFrame)
      x2, y2 = event.x, event.y
      if distance(x1 or x2,y1 or y2,x2,y2) > length and instance.frames < 45 then
        local keyEventUp, keyEventDown
        if distance(x1,0,x2,0) > distance(0,y1,0,y2) then -- left and right
          if x1 < x2 then
            keyEventDown = {name = "ui", phase = "pressed", buttonName = "right"}
            keyEventUp = {name = "ui", phase = "released", buttonName = "right"}
          else
            keyEventDown = {name = "ui", phase = "pressed", buttonName = "left"}
            keyEventUp = {name = "ui", phase = "released", buttonName = "left"}
          end
        else -- up and down
          if y1 < y2 then
            keyEventDown = {name = "ui", phase = "pressed", buttonName = "down"}
            keyEventUp = {name = "ui", phase = "released", buttonName = "down"}
          else
            keyEventDown = {name = "ui", phase = "pressed", buttonName = "up"}
            keyEventUp = {name = "ui", phase = "released", buttonName = "up"}
          end
        end
        Runtime:dispatchEvent(keyEventDown)
        timer.performWithDelay(1, function ()
            Runtime:dispatchEvent(keyEventUp)
          end)
      end
      display.remove(instance.feedback)
      self.isFocus = false
    end
    return true
  end

  instance:addEventListener("touch")

  return instance
end

return M