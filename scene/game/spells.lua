-- Requirements
local composer = require "composer"
local json = require "json"
local tiled = require "com.ponywolf.ponytiled"
local snd = require "com.ponywolf.ponysound"
local snap = require "com.ponywolf.snap"
local stats = require "com.ponywolf.ponystats"
local fx = require "com.ponywolf.ponyfx"
local alert = require "scene.alert"


-- Vars/Objects local to scene
local M = {}

local libmanager = require "scene.inventory.manager"
local slots = libmanager.slots

-- TODO create all spell functions here

local function cast(spell)
  -- Get the current scene
  local scene = composer.getScene("scene.game")
  if spell == "reshuffle" then
    if scene.hero:useMana(5) then
      scene:toggle("reshuffle")
      -- TODO cool down
    else
      alert.new({ filename = "gfx/alert.json", dim = true, text = "Out of Mana!", ok = "cancel" })
    end
    -- TODO cool down
  elseif spell == "2x" then
    if scene.hero:useMana(3) then
      scene.hero:buff("2x")
    else
      alert.new({ filename = "gfx/alert.json", dim = true, text = "Out of Mana!", ok = "cancel" })
    end
    -- TODO cool down
  end
end

function M.new(event)

  local mapData = json.decodeFile(system.pathForFile("gfx/spellChoose.json"))  -- load from json export

  local instance = tiled.new(mapData, "gfx")
  instance:centerAnchor()
  instance:extend("button", "label", "decor")

  local function ui(event)
    local phase, name, tag = event.phase, event.buttonName or "none", event.tag
    if phase == "released" and (tag == "spells" or tag == "android") and (not transition.isTransitioning("cards")) then
      snd:play("click")
      if name == "spellLeft" then
        print ("Casting", slots["left"])
        cast(slots["left"])
      elseif name == "spellRight" then
        print ("Casting", slots["right"])
        cast(slots["right"])
      end
    end
    return true
  end

  function instance:open()
    -- Turn on icons
    print("Enabling", slots["left"], slots["right"])
    local spells = instance:listTypes("spell")
    for i = 1, #spells do
      transition.cancel(spells[i])
      spells[i].isVisible = false
      if (spells[i].name) == (slots["left"] .. "Left") or (spells[i].name) == (slots["right"] .. "Right") then
        spells[i].isVisible = true
        fx.float(spells[i])
      end
    end
    -- Open animation
    if self.isOpen then return end
    transition.cancel(self)
    snap(instance, "bottomcenter", 12)
    instance:findLayer("drawHelp").isVisible = false
    local function finished()
      if stats.get("hands") <= 5 then
        instance:findLayer("drawHelp").isVisible = true
      end
      snap(instance, "bottomcenter", 12)
      Runtime:addEventListener("ui", ui)
    end
    transition.from(instance, {y = instance.designedHeight + display.actualContentHeight, time = 333, delay = 333,
        onCancel = finished, onComplete = finished, transition = easing.outBounce
      })
    snd:play("swoosh")
    self.isVisible = true
    self.isOpen = true
  end

  function instance:close()
    if not self.isOpen then return end
    transition.cancel(self)
    snap(instance, "bottomcenter", 12)
    local function finished()
      self.isVisible = false
    end
    transition.to(instance, {y = instance.designedHeight + display.actualContentHeight, time = 333, delay = 0,
        onCancel = finished, onComplete = finished, transition = easing.inQuad
      })
    snd:play("swoosh")
    Runtime:removeEventListener("ui", ui)
    self.isOpen = false
  end

  return instance
end

return M