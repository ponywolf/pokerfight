-- game.scene.coin 

local M = {}
local composer = require "composer"
local fx = require "com.ponywolf.ponyfx"
local properties = require "com.ponywolf.properties"

local distanceToHero = 32
local sqrt = math.sqrt 
local function len(dx, dy)
  return sqrt(dx * dx + dy * dy)
end

local itemSheetOptions = { width = 20, height = 20, numFrames = 144 }
local itemSheet = graphics.newImageSheet("gfx/items.png", itemSheetOptions)

local chestTypes = { small = 3, medium = 1, large = 2 }

function M.new(instance)
  -- Get the current scene
  local scene = composer.getScene("scene.game")

  -- Store map placement/options and hide placeholder
  local parent = instance.parent
  local size = chestTypes[(instance.size or "small")]
  local values = properties.get(instance, {"x", "y", "name", "type", "id",})
  display.remove(instance)  

  -- Build chest's view
  instance = display.newGroup()
  instance.closed = display.newImage(instance, itemSheet, size * 2 + 1)
  instance.opened = display.newImage(instance, itemSheet, size * 2 + 2 )
  instance.opened.isVisible = false

  fx.bounce(instance.closed)
  parent:insert(instance)

  -- restore properties
  instance = properties.set(instance, values)

  --shadow
  instance.shadow = display.newImage(scene.world:findLayer("shadow") or instance, "gfx/shadow.png")
  instance.shadow.alpha = 0.25

  -- physics
  physics.addBody(instance, "static", { radius = 14, isSensor = true }) -- or kinematic
  physics.addBody(instance.shadow, "static", { box = { halfWidth=4, halfHeight=4, x=0, y=-6 }}) -- or kinematic

  instance.frameCount = 0
  local function enterFrame()
    -- shadow
    instance.shadow.x,instance.shadow.y = instance.x, instance.y + 8
    instance.shadow.isVisible = instance.isVisible

    -- Do this every frame
    if scene.isPaused then return false end
    instance.frameCount = instance.frameCount + 1
  end

  function instance:open(fast)
    transition.cancel(self)    
    self.opened.isVisible, self.closed.isVisible = true, false
    self.isOpen = true
    if not fast then
      fx.impact(self)
    end
  end

  function instance:collision(event)
    local phase, other = event.phase, event.other
    if other.name == "hero" and phase == "began" and not self.isOpen then -- show an emote, delayed action
      other.emote:show("open")
      scene.tapHelp.isVisible = true
      other.action = "open"
      other.params = { target = self }
    elseif phase == "ended" then
      if other.name == "hero" then
        -- hero left us
        other.emote:hide()
        scene.tapHelp.isVisible = false
        other.action = nil
        other.params = nil
      end
    end
  end

  instance:addEventListener("collision")

  function instance:finalize()
    -- On remove, cleanup instance, or call directly for non-visual
    display.remove(instance.shadow)
    Runtime:removeEventListener("enterFrame", enterFrame)
  end

-- Add a finalize listener (for display objects only, comment out for non-visual)
  instance:addEventListener("finalize")

-- Add our enterFrame listener
  Runtime:addEventListener("enterFrame", enterFrame)

  return instance
end

return M