-- TODO Make these a rating module

local function androidReview()
  local function onComplete(event)
    if (event.action == "clicked") then
      local i = event.index
      if (i == 1) then
        settings:set("rateApp", true) 
        local options={ supportedAndroidStores={"google"}, androidAppPackageName="com.ponywolf.packagename" }
        native.showPopup("rateApp", options)
      elseif (i == 2) then
        settings:set("rateApp", true) 
      elseif (i == 3) then
        -- dismiss
      end
    end
  end  
  native.showAlert("GameName", "Like us? Rate GameName on Google Play!", { "Ok", "Don't ask me again", "Not now", }, onComplete)
end

local function iosReview()
  local function onComplete(event)
    if (event.action == "clicked") then
      local i = event.index
      if (i == 1) then
        settings:set("rateApp", true) 
        system.openURL("itms-apps://apps.apple.com/us/app/")
      elseif (i == 2) then
        settings:set("rateApp", true) 
      elseif (i == 3) then
        -- dismiss
      end
    end
  end  
  native.showAlert("GameName", "Like us? Rate GameName on the Appstore!", { "Ok", "Don't ask me again", "Not now", }, onComplete)
end

local function askReview()
  if not settings:get("rateApp") then
    if isiOS then
      local platformVersion = system.getInfo("platformVersion") or 0
      local iOSVersion = tonumber(string.sub(platformVersion, 1, 4)) or 0
      if iOSVersion >= 10.3 then 
        local reviewPopUp = require "plugin.reviewPopUp"
        reviewPopUp.show()
      end
    end
    -- android review
    if isAndroid then
      androidReview()
    end
  end
end