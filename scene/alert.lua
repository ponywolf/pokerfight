-- Generic alert and dialog box class for Solar2D/Tiled

local json = require "json"
local composer = require "composer"
local snap = require "com.ponywolf.snap"
local snd = require "com.ponywolf.ponysound"
local tiled = require "com.ponywolf.ponytiled"

local M = {}
local queue = {}

local function split(inputstr, sep)
  sep = sep or "|"
  inputstr = inputstr or ""
  local tbl={}
  local i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    tbl[i] = str
    i = i + 1
  end
  return tbl
end

function M.new(options)

  options = options or {}
  local filename = options.filename
  local dir = options.dir or (split(filename,"/")[1])
  local onComplete

  if not (filename and dir) then
    print("ERROR: Tiled file expected...")
    return false
  end

  local function dim(percent)
    local instance = display.newRect(display.contentCenterX,display.contentCenterY,display.actualContentWidth*2,display.actualContentHeight*2)
    instance:setFillColor(0,0,0,1)
    instance.alpha = percent or 0.5
    instance.isHitTestable = true
    instance.name = "dim"
    transition.from(instance, { time = 166, alpha = 0 })
    local function touch(event)
      if event.phase == "ended" then
        Runtime:dispatchEvent ({ name = "ui", phase = "released", buttonName = "cancel" })
      end
      return true
    end
    instance:addEventListener("touch", touch)
    instance:addEventListener("tap", touch)
    return instance
  end

  -- make the ui
  local instance = display.newGroup()

  -- are we modal?
  local modal
  if options.dim or options.modal then
    options.dim = true and 0.5 or options.dim
    options.dim = false and 0 or options.dim
    modal = dim(options.dim)
    instance:insert(modal)
  end

  -- load tiled file
  local alertData = json.decodeFile(system.pathForFile(filename))  -- load from json export
  local alert = tiled.new(alertData, dir)
  alert:extend("label", "textBox", "button")
  alert:centerAnchor()
  alert.x = display.contentCenterX
  alert.y = display.contentCenterY
  instance:insert(alert)

  -- populate text and title
  local text = alert:findObject("text")
  local title = alert:findObject("title")

  if options.text and text then
    text:update(options.text:gsub("|","\n"))
  end

  if options.title and title then
    title:update(options.title)
  end

-- show alert
-- change transtion to fit your needs
  transition.from(alert, { y = display.screenOriginY - alert.designedHeight / 2, time = 333, transition = easing.outBounce })

-- touch to cancel
  function instance:cancel()
    transition.to(modal, { alpha = 0, time = 166 })
    transition.to(alert, { y = display.actualContentHeight + alert.designedHeight, time = 166, transition = easing.outQuad,
        onComplete = function()
          if onComplete then onComplete() end
          display.remove(self)
        end
      })
  end

  local function ui(event)
    local phase, name = event.phase, event.buttonName or "none"
    if phase == "released" then
      snd:play(options.sound or "click")
      if options[name] then
        if options[name] == "cancel" then -- just cancel
          onComplete = nil
          instance:cancel()
        else
          onComplete = options[name] -- assign function
          instance:cancel() -- then cancel
        end
      else
        if name == "cancel" then -- name is cancel but there' no cancel function
          onComplete = nil
          instance:cancel()-- just cancel
        end
      end
    end
    return true
  end

  Runtime:addEventListener("ui", ui)

-- remove ui listener
  function instance:finalize()
    Runtime:removeEventListener("ui", ui)
  end

  instance:addEventListener("finalize")

  return instance

end

return M
