-- ponystore
-- Simple wrapper for "noads" purchases/restores only

local M = {}

local isSimulator = system.getInfo("environment") == "simulator"
local isAndroid = system.getInfo("platform") == "android"
local isiOS = system.getInfo("platform") == "ios"
local isMobile = isiOS or isAndroid
--local isSteam = ("win32" == system.getInfo("platform")) or (system.getInfo("platform") == "macos")
--local isKindle = system.getInfo("manufacturer") == "Amazon"

local store
local json = require "json"
local success, fail, timeoutTimer
local isSandbox
local timeoutDelay = 5000

if isSimulator or isSandbox then
  store = { target = "Sandbox", isActive = true, canMakePurchases = true}
elseif isiOS then 
  store = require "store"
elseif isAndroid then
  store = require "plugin.google.iap.v3"
end

local productIdentifiers = { "noads" }
local function productListener(event)
  print(json.prettify(event or {}))
end

local function storeListener(event)
  -- IAP initialization event
  local name, transaction = event.name, event.transaction or { }
  if timeoutTimer then
    timer.cancel(timeoutTimer)
  end
  if name == "init" then
    if not transaction.isError then
      print ("STORE:", event.name, store.target, store.isActive)
      if store.loadProducts then
        print ("STORE: Attempting to load products...") --, json.prettify(productIdentifiers))
        store.loadProducts(productIdentifiers, productListener)
      end
    else  
      print ("STORE ERROR:",transaction.errorType, transaction.errorString)
    end
    if isiOS and not store.canMakePurchases then
      print ("STORE WARNING: Purchases may be disabled on this device")
      local alert = native.showAlert("PokerKnight", "Purchases may be disabled on this device.", { "OK" })
    end
  elseif name == "storeTransaction" then
    if transaction.state == "purchased" or transaction.state == "restored" then 
      print("STORE TRANSACTION:")--, json.prettify(transaction))
      if isiOS and not (isSimulator or isSandbox or (transaction.state == "restored")) then -- finish
        store.finishTransaction(transaction)
      end
      if success then success() end
    else  -- Unsuccessful transaction; output error details
      print("STORE ERROR:",transaction.errorType, transaction.errorString)
      if fail then fail() end
    end
  end
end

local function sandboxedTransaction(state, id)
  return {
    name = "storeTransaction",
    transaction = {
      state = state,
      productIdentifier = id,
      identifier = "sandboxed",
      isError = false,
    }
  }
end

function M.purchase(productIdentifier, callback, fallback, timeout)
  success, fail = callback, fallback
  print("STORE: Attempting purchase", productIdentifier) 
  if isSimulator or isSandbox then 
    local function onComplete(event)
      if event.action == "clicked" then
        if event.index == 1  then
          print("STORE: Sandbox purchase approved", productIdentifier) 
          timer.performWithDelay (333, function ()
              storeListener(sandboxedTransaction("purchased", productIdentifier))
            end)
        elseif event.index == 2 then
          print("STORE: Sandbox purchase cancelled", productIdentifier) 
          timer.performWithDelay (333, function ()
              storeListener(sandboxedTransaction("cancelled", productIdentifier))
            end)
        elseif event.index == 3 then
          print("STORE: Sandbox purchase failed", productIdentifier) 
          timer.performWithDelay (333, function ()
              storeListener(sandboxedTransaction("failed", productIdentifier))
            end)
        end
      end
    end  
    local alert = native.showAlert("Ponystore", "Approve purchase?", { "Success", "Cancel", "Fail" }, onComplete)
    return
  end
  if store.isActive then
    store.purchase(productIdentifier)
  else
    print ("WARNING: Store not active")
  end
end

function M.restore(callback, fallback, timeout)
  print("STORE: Attempting restore", productIdentifier) 
  if isSimulator or isSandbox then 
    M.purchase("noads", callback, fallback)
    return
  end
  success, fail = callback, fallback
  if store.isActive then
    store.restore()
    if timeout then
      timeoutTimer = timer.performWithDelay(timeoutDelay, timeout)
    end
  else
    print ("WARNING: Store not active")
    fallback()
  end
end

-- Initialize IAP
if isSimulator or isSandbox then
  storeListener({ name = "init" })
else
  store.init(storeListener)
end

return M