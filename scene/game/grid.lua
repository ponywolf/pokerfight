-- Grid module
-- builds a grid on top of a rpoguelike map

local M = {}

function M.new(data, map)
  local points = {}
  local function exists(x,y)
    for i = 1, #points do
      if points[i].x == x and points[i].y == y then
        return true
      end
    end
    return false
  end

  for num = 1, #data.room do
    local room = data.room[num]
    local x,y = room.x, room.y
    local x1,y1 = room.left, room.top
    local x2,y2 = room.left+room.width-1, room.top+room.height-1
    local x3,y3,x4,y4 = 0,0
    if true then
      if room.width >= 9 and not room.hasTower then
        x3,x4 = math.ceil(x1 + room.width/5), math.floor(x2 - room.width/5)
      end
      if room.height >= 9 and not room.hasTower then
        y3,y4 = math.ceil(y1 + room.height/5), math.floor(y2 - room.height/5)
      end
    end
    if room.hasTower then -- get the corners
      points[#points+1] = {x = x1, y = y1, hub = true, room = num }
      points[#points+1] = {x = x1, y = y2, hub = true, room = num }
      points[#points+1] = {x = x2, y = y1, hub = true, room = num }
      points[#points+1] = {x = x2, y = y2, hub = true, room = num }
      points[#points+1] = {x = x1+1, y = y1, hub = true, room = num }
      points[#points+1] = {x = x1+1, y = y2, hub = true, room = num }
      points[#points+1] = {x = x2-1, y = y1, hub = true, room = num }
      points[#points+1] = {x = x2-1, y = y2, hub = true, room = num }
      points[#points+1] = {x = x1, y = y1+1, hub = true, room = num }
      points[#points+1] = {x = x1, y = y2-1, hub = true, room = num }
      points[#points+1] = {x = x2, y = y1+1, hub = true, room = num }
      points[#points+1] = {x = x2, y = y2-1, hub = true, room = num }
      x1, y1, x2, y2 = x1 + 1, y1 +1, x2 - 1, y2 - 1
    end
    for i = x1-1, x2+1 do
      for j = y1-1, y2+1 do
        if data.inside[i][j] then
          if (i == x or i == x1 or i == x2 or i == x3 or i == x4) and (j == y or j == y1 or j == y2 or j == y3 or j == y4) then
            points[#points+1] = {x = i, y = j, hub = true, room = num }
          elseif i == x or i == x1 or i == x2 or i == x3 or i == x4 or j == y or j == y1 or j == y2 or j == y3 or j == y4 then
            if not exists(i,j) then
              points[#points+1] = {x = i, y = j }
            end
          end
        end
      end
    end
  end

  -- points for hallways
  local w, h = data.width, data.height
  for i = 1, w do
    for j = 1, h do
      if not map.findRoom(i,j) and data.inside[i][j]  then
        points[#points+1] = {x = i, y = j }
      end
    end
  end

  return points
end

function M.getPoint(points, mx , my)
  for i = 1, #points do
    if points[i].x == mx and points[i].y == my then
      return points[i]
    end
  end
  return false -- no point at map x,y
end

function M.getRoomPoints(points, room, hubOnly)
  local roomPoints = {}
  for i = 1, #points do
    if points[i].room == room then
      if (hubOnly and points[i].hub) or not hubOnly then
        roomPoints[#roomPoints+1] = points[i]
      end
    end
  end
  return roomPoints
end

return M

