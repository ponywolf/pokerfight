-- Module/class for hero

-- Use this as a template to build an in-game hero
local app = require "app"
local composer = require "composer"
local json = require "json"

local fx = require "com.ponywolf.ponyfx"
local snap = require "com.ponywolf.snap"
local settings = require "com.ponywolf.settings"
local snd = require "com.ponywolf.ponysound"
local properties = require "com.ponywolf.properties"

local items = require "scene.game.items"
local attacks = require "scene.game.hero.attack"
local emotes = require "scene.game.hero.emotes"
local pfx = require "scene.game.pokerfx"
local manager = require "scene.inventory.manager"
local grid = require "scene.game.grid"

local slots = manager.slots

local csv = require "com.ponywolf.csv"
local gearData = csv.load("gear.csv")

-- Define module
local M = {}

local heroSheetOptions = { width = 20, height = 20, numFrames = 28 }
local gearSheetOptions = { width = 24, height = 24, numFrames = 100 }

local sqrt = math.sqrt
local function dist(dx, dy)
  return sqrt(dx * dx + dy * dy)
end

local equipment = require "scene.game.animations"

function M.new()
  -- Get the current scene
  local scene = composer.getScene("scene.game")

  -- load frame animation
  local heroSheetOptions = { width = 20, height = 20, numFrames = 28 }
  local heroSheet = graphics.newImageSheet("gfx/hero.png", heroSheetOptions)
  local sequences = require("scene.game.animations").fast

  -- load weapons
  local gearSheet = graphics.newImageSheet("gfx/gear.png", gearSheetOptions)

  -- build hero's view
  local instance = display.newGroup()
  instance.sprite = display.newSprite(heroSheet, sequences)
  instance:insert(instance.sprite)
  instance.sprite.anchorY = 1
  instance.sprite:translate(0,8)
  instance.sprite:play()
  fx.breath(instance.sprite)

  -- vars
  instance.name = "hero" -- need to set these because
  instance.phase = "idle"
  instance.room = 0 -- no room yet
  instance.action, instance.params = nil, nil

  -- equipment/spells
  slots["attack"] = slots["attack"] or "rusty"
  slots["defend"] = slots["defend"] or "woody"
  slots["left"] = slots["left"] or "2x"
  slots["right"] = slots["right"] or "reshuffle"

  --shadow
  local shadow = display.newImage(instance, "gfx/shadow.png")
  shadow:translate(0,7)
  shadow:toBack()
  shadow.alpha = 0.5

  -- footsteps
  if instance.footprints then
    fx.newFootprint(instance, { parent = scene.world })
  end

  -- put emotes on the hud layer
  instance.emote = emotes.new(instance, 0, -30)
  scene.hud:insert(instance.emote)

  local function idle()
    for _,slot in pairs ({"attack", "defend"}) do
      if instance[slot] then
        transition.cancel(instance[slot])
        local offset = equipment[slot][instance.equipSequence]
        if offset then
          instance[slot].x = offset.x
          instance[slot].y = offset.y
          instance[slot].xScale = offset.flipped and -1 or 1
          instance[slot].rotation = 0
          if offset.behind then
            instance[slot]:toBack()
          else
            instance[slot]:toFront()
          end
          if offset.dark then
            instance[slot]:setFillColor(0.5)
          else
            instance[slot]:setFillColor(1)
          end
        end
        local intensity, speed
        if instance.sequence:find("idle") then
          intensity, speed = 0.02, 266
        else
          intensity, speed = 0.033,133
        end
        fx.float(instance[slot], intensity, speed)
      end
    end
  end

  -- movement
  local directions = { up = "North", down = "South", left = "West", right = "East" }
  local movement = { up = { y = -1 }, down = { y = 1 }, left = { x = -1 }, right = { x = 1 } }

  function instance:move(direction)
    -- set walking animation
    local newSequence
    local dir = directions[direction]
    local points = scene.points
    local ox, oy = self.mx, self.my

    if dir and self.phase == "idle" then
      self.phase = "moving"
      newSequence = "walk" .. dir
      self:setSequence(newSequence)
      -- move
      local x, y = self.x, self.y
      local mx, my = self.mx, self.my
      mx = mx + (movement[direction].x or 0)
      my = my + (movement[direction].y or 0)

      if not scene.map.inside[mx][my] then -- blocked
        mx, my = self.mx, self.my
      else -- monster there?

      end

      local nx, ny = scene.map.mapToContent(mx,my)

      -- back to idle
      local function canceled()
        newSequence = self.sequence:gsub("walk", "idle")
        self:setSequence(newSequence)
        self.phase = "idle"
      end

      local function finished()
        self.mx, self.my = mx, my -- for sure set these
        newSequence = self.sequence:gsub("walk", "idle")
        self:setSequence(newSequence)
        --pfx.popText(self, self.mx .. "-" .. self.my)
        -- let enemies move
        self.phase = "waiting"
        scene:moved()
      end

      if not (mx == ox and my == oy) then -- we found a move
        --slide
        local time = 166
        newSequence = self.sequence:gsub("walk", "idle")
        transition.to(self, { x = nx, y = ny, time = time, transition = easing.outQuad, onComplete = finished })
      else
        canceled()
      end
    end
  end

  local buttonMap = app.buttonMap

  local function key(event)
    local phase, name = event.phase, buttonMap[event.keyName] or event.keyName

    --print(name,phase)
    --if device.isMobile then return true end

    -- suspend key on menu's being open
--    if scene.character:isOpen() or scene.menu:isOpen() or composer.getSceneName("overlay") then
--      return false
--    end

    -- suspend key on any movement
--    if transition.isTransitioning("movement") then
--      return false
--    end

    if phase == "up" then
      if name == "buttonA" and scene.isDialog then
        Runtime:dispatchEvent({name = "dialog", phase = "close"})
        return true
      elseif instance.phase == "dealt" then -- we can stay
        local cards = scene.hand.numChildren
        if cards == 1 then
          if name == "buttonA" or name == "space" then
            move("stay")
          elseif name == "buttonY" or name == "x" then
            move("hit", scene.hand[1])
          end
        elseif cards == 2 then
          if name == "buttonA" or name == "space"  then
            move("stay")
          elseif name == "buttonX" or name == "z" then
            move("hit", scene.hand[1])
          elseif name == "buttonB" or name == "c" then
            move("hit", scene.hand[2])
          end
        elseif cards == 3 then
          if name == "buttonA" or name == "space" then
            move("stay")
          elseif name == "buttonX" or name == "z" then
            move("hit", scene.hand[1])
          elseif name == "buttonY" or name == "x" then
            move("hit", scene.hand[2])
          elseif name == "buttonB" or name == "c" then
            move("hit", scene.hand[3])
          end
        end
        return true
      else
        -- moved keys to enterFrame()
      end
    end
  end

  local function ui(event)
    local phase, name = event.phase, event.buttonName
    if phase == "released" and directions[name] then
      instance:move(name)
      return true
    end
  end

  local function room(event)
    -- draw the pertainant points on the map
    print(event.phase, event.room)
    local phase = event.phase
    local num = event.room
    local rm = scene.map.room[num]

    if phase == "entered" and rm then

    elseif phase == "hallway" then
      -- hmmm?
    elseif phase == "exited" then

    end

  end

  function instance:setSequence(sequence)
    if not (self.sequence == sequence) or self.sequence == nil then
      self.sequence = sequence or self.sequence or "idleSouth"
      self.equipSequence = self.sequence:gsub("walk",""):gsub("idle",""):lower()
      self.sprite:setSequence(self.sequence)
      instance.sprite:play()
      idle()
    end
  end
  instance:setSequence()

  function instance:face(obj) -- face toward an object
    if obj and obj.x and obj.y then
      if math.abs(obj.x-self.x) >= math.abs(obj.y-self.y) then
        if obj.x > self.x then
          self:setSequence("idleEast")
        else
          self:setSequence("idleWest")
        end
      else
        if obj.y > self.y then
          self:setSequence("idleSouth")
        else
          self:setSequence("idleNorth")
        end
      end
    end
  end

-- settings.session.hp = 8 -- test for hurt play
  function instance:getStats()
    self.maxhp = settings.session.maxhp or 20 -- starting values or session values
    self.hp = settings.session.hp or self.maxhp
    self.maxMana = settings.session.maxMana or 20
    self.mana = settings.session.mana or self.maxMana
    self.coin = settings.session.coin or 0
    self.shield = settings.session.shield or 0
    self.sword = settings.session.sword or 1
    self.xp = settings.session.xp or 0
    self.level = settings.session.level or 1
  end
  instance:getStats()

  function instance:updateStats()
    settings.session.maxhp = self.maxhp
    settings.session.hp = self.hp
    settings.session.maxMana = self.maxMana
    settings.session.mana = self.mana
    settings.session.coin = self.coin
    settings.session.shield = self.shield
    settings.session.sword = self.sword
    settings.session.xp = self.xp
    settings.session.level = self.level
    if scene.hudLeft and scene.hudRight then
      scene.hudLeft:setHealth(self.hp, self.maxhp)
      scene.hudLeft:setMana(self.mana, self.maxMana)
      scene.hudLeft:setCoin(self.coin)
      scene.hudRight:setDefend(self.shield)
      scene.hudRight:setAttack(self.sword)
      scene.hudRight:setXP(self.level, self.xp, self.level * self.level * 10) -- hack for next level
      scene.heroStats:setHealth(self.hp, self.maxhp)
      scene.heroStats:setMana(self.mana, self.maxMana)
    end
  end

-- all the actions the hero can do, open chests, open doors, etc.
  function instance:execute(action, params)
    params = params or {}
    scene.tapHelp.isVisible = false
    if action == "exit" then
      settings.lastLocation = settings.lastLocation or {}
      settings.lastLocation[scene.map] = {x = self.x, y = self.y }
      local function loadWorld()
        composer.gotoScene("scene.refresh", { params = params })
      end
      fx.irisOut(loadWorld)
    elseif action == "floor" then -- we finished a floor
      scene:nextFloor()
    elseif action == "reset" then -- reset the world
      scene:resetWorld()
    elseif action == "open" then
      local chest = params.target
      local function itemDrop()
        -- Extract items from other (chest in this case)
        local chances = { "coin", "coin", "coin", "coin", "item", "item", "item" }
        local loot = items.new(chances[math.random(#chances)], chest)
        snd:play("sword")
      end
      itemDrop()
      transition.to(self, { time = 133,
          iterations = math.random(1,3),
          onRepeat = itemDrop,
        })
      chest:open()
    elseif action == "item" then
      local data = params.data or {}
      print("Using item", params.data.id)
      -- route item to libitems.action()
      -- but pass it though any bonuses that may exist
      items.action(data.action, data)
      snd:play("potion")
      self:updateStats()
    elseif action == "coin" then
      print("Adding coin(s)...")
      self.coin = self.coin + (params.num or 1)
      snd:play("coin")
      self:updateStats()
    elseif action == "heal" then
      self:heal()
      snd:play("heal")
    elseif action == "mana" then
      self:addMana()
      snd:play("heal")
    elseif action == "dialog" then
      Runtime:dispatchEvent({name = "dialog", text = params.text, background = params.background })
    end
  end

  function instance:addXP(num)
    num = num or 1
    pfx.popText(self, "+XP")
    self.xp = self.xp + num
    self:updateStats()
  end

  function instance:canMana(num)
    if self.mana < self.maxMana then
      return true
    end
    print("Can't mana!")
  end

  function instance:addMana(num)
    if not self:canMana() then
      print("No need for mana!")
      return false
    else
      if num == "full" then
        pfx.popText(self, "FULL MANA")
        num = 99999
      else
        num = tonumber(num) or 1
        pfx.popText(self, "+" .. num)
      end
      self.mana = math.min(self.maxMana, self.mana + num)
      self:updateStats()
      return true
    end
  end

  function instance:useMana(num)
    num = tonumber(num) or 1
    if self.mana >= num then
      self.mana = math.max(0, self.mana - num)
      instance:updateStats()
      return true
    else
      return false
    end
  end

  function instance:hurt(num)
    num = tonumber(num) or 1
    fx.flash(self.sprite)
    pfx.popText(self, "-" .. num)
    self.hp = math.max(0, self.hp - num)
    instance:updateStats()
    if self.hp <= 0 then
      self:die()
      return true
    else
      return false
    end
  end

  function instance:canHeal(num)
    if self.hp < self.maxhp then
      return true
    end
    print("Can't heal!")
  end

  function instance:heal(num)
    if not self:canHeal() then
      print("No need to heal!")
      return false
    else
      if num == "full" then
        pfx.popText(self, "FULL HEAL")
        num = 99999
      else
        num = tonumber(num) or 1
        pfx.popText(self, "+" .. num)
      end
      self.hp = math.min(self.maxhp, self.hp + num)
      self:updateStats()
      return true
    end
  end

  function instance:hide()
    self.isVisible = false
  end

  function instance:show()
    self.isVisible = true
  end

  function instance:die()
    self.action, self.params = nil, nil
    local function postDeath()
      scene:reset()
      composer.gotoScene("scene.refresh", { params = { map = "town" }})
    end
    scene:stats("close")
    local dead = pfx.victoryText("DEATH!", postDeath)
    snap(dead)
    scene.hud:insert(dead)
  end

  local last = {}
  last.attack, last.defend = 1,22
  function instance:equip(slot, num)
    slot = slot or "attack"
    slot = slot == "attack" and slot or "defend"
    num = num or last[slot]
    last[slot] = num
    print("Equipping", num, "in slot", slot)
    if self[slot] then
      display.remove(self[slot])
    end
    self[slot] = display.newImage(gearSheet, num)
    self:insert(self[slot])
    self[slot].x, self[slot].y = 0,0
    self[slot]:toFront()
    idle()
  end

  function instance:buff(kind)
    print("Buffing:", kind)
    if kind == "2x" then
      self.attackBonus = (self.attackBonus or 1) * 2
      pfx.popText(self, self.attackBonus .. "X Next Hit!")
    end
  end

  function instance:swing(count)
    print("Swinging", instance.equipSequence)
    local function attacked()
      if self.attacking and self.attacking.isDead then
        scene:toggle("win")
        self.attacking = nil
      else -- fight again? Since everything is paused, just show the menu again
        scene:toggle("choose")
      end
      idle()
    end

    local function drop()
      -- Extract items from other
      -- Figure out what loot to drop
      local chances = { "coin", "coin", "coin", "coin", "coin", "coin", "coin", "coin", "item", "item", "item", "gear" }
      local loot = items.new(chances[math.random(#chances)], self.attacking)
      snd:play("sword")
      snd:play("coin")
    end

    if self.attack and self.attack.translate then
      local swing = equipment.attack[instance.equipSequence]
      transition.cancel(self.attack)
      self.attack:translate(swing.sx, swing.sy)
      self.attack.rotation = swing.sa
      drop()
      transition.to(self.attack, { time = 133, y = swing.ey or 0, x = swing.ex or 0,
          iterations = count,
          rotation = equipment.attack[instance.equipSequence].ea,
          transition = easing.inOutQuad,
          onRepeat = drop,
          onComplete = attacked,
        })
    end
  end

  function instance:fight(handRank)
    local attacks, damage = attacks.basicAttack(handRank)
--    attacks = 25
--    damage = 666
    if damage > 0 then
      damage = damage * gearData:find(slots["attack"]).damage
      self:swing(attacks)
      self.attacking:hurt(damage * (self.attackBonus or 1))
    else
      pfx.popText(self, "MISSED!")
      self.attacking:fight()
    end
    self.attackBonus = 1
  end

  local lastPhase
  local function enterFrame()
    -- do this every frame
    shadow.isVisible = instance.isVisible

    if instance.phase ~= lastPhase then
      --print("Phase change", lastPhase, "to", instance.phase)
      lastPhase = instance.phase
    end

    if instance.phase == "idle" then -- we can move
      local moved = app.key["up"] or app.key["down"] or app.key["left"] or app.key["right"]
      if app.key["up"] then
        instance:move("up")
      elseif app.key["down"] then
        instance:move("down")
      elseif app.key["left"] then
        instance:move("left")
      elseif app.key["right"] then
        instance:move("right")
      end
    end

    -- set map x,y
    instance.mx, instance.my = math.floor(0.5+instance.x/16),math.floor(0.5+instance.y/16)
    -- find the room
    local newRoom = scene.map.findRoom(instance.mx, instance.my) or 0
    if newRoom ~= instance.room then -- we've entered a new room
      if instance.room ~= 0 then
        room({phase = "exited", room = instance.room})
      end
      if newRoom == 0 then
        room({phase = "hallway", room = 0 }) -- hallway
      else
        room({phase = "entered", room = newRoom})
      end
      instance.room = newRoom
    end
    -- don't show all points
    local vision = 72
    for i = 1, scene.nav.numChildren do
      local nav = scene.nav[i]
      nav.isVisible = dist(instance.x - nav.x, instance.y - nav.y) < vision
    end
  end

  function instance:finalize()
    -- On remove, cleanup instance, or call directly for non-visual
    Runtime:removeEventListener("enterFrame", enterFrame)
    Runtime:removeEventListener("key", key)
    Runtime:removeEventListener("ui", ui)
  end

-- Add a finalize listener (for display objects only, comment out for non-visual)
  instance:addEventListener("finalize")

-- Add our enterFrame listener
  Runtime:addEventListener("enterFrame", enterFrame)

-- Add our joystick listeners
  Runtime:addEventListener("key", key)
  Runtime:addEventListener("ui", ui)

  instance:equip("attack")
  instance:equip("defend")
  idle()

  return instance
end

return M