local M = {}

local composer = require "composer"
local json = require "json"

local snd = require "com.ponywolf.ponysound"
local settings = require "com.ponywolf.settings"
local stats = require "com.ponywolf.ponystats"
local csv = require "com.ponywolf.csv"
local fx = require "com.ponywolf.ponyfx"

local itemData = csv.load("items.csv")
local gearData = csv.load("gear.csv")
local manager = require "scene.inventory.manager"

local itemSheetOptions = { width = 20, height = 20, numFrames = 144 }
local itemSheet = graphics.newImageSheet("gfx/items.png", itemSheetOptions)

local gearSheetOptions = { width = 24, height = 24, numFrames = 100 }
local gearSheet = graphics.newImageSheet("gfx/gear.png", gearSheetOptions)

local coinSheetOptions = { width = 16, height = 16, numFrames = 27 }
local coinSheet = graphics.newImageSheet("gfx/animated.png", coinSheetOptions)
local sequences = require("scene.game.animations").coin

local distanceToHero = 24
local sqrt = math.sqrt
local function len(dx, dy)
  return sqrt(dx * dx + dy * dy)
end

local function generate(options)
  -- Get applicable items
  -- TODO generate based on combinations of monster value, slot, and fixed rarity
end

local function findByName(name)
  return itemData:find("name", name)
end

local function getRandom(kind)
  if kind == "gear" then
    return gearData:find(gearData.ids[math.random(#gearData.ids)])
  else
    return itemData:find(itemData.ids[math.random(#itemData.ids)])
  end
end

function M.get(floor, kind)
  -- what kind?
  local noJunk = kind == "noJunk"

  -- find an item that works
  floor = floor or 1
  local scene = composer.getScene("scene.game")
  local timeout, item, chance, isJunk, isFloor = 1 -- try 100 times, 100% chance of getting it
  repeat
    item = getRandom(kind)
    --print(floor, json.prettify(item))
    local first, last = item.startFloor or 1, item.endFloor or 999999
    -- selections
    chance = math.random() < (item.chance or 1)
    isJunk = noJunk and (item.action == "junk" or item.action == "poison" or item.action == "gold")
    isFloor = (floor <= last) and (floor >= first)
    timeout = timeout + 1
  until timeout > 99 or (chance and isFloor and not isJunk and item.enabled)
  print ("You found a ", item.name)
  item.kind = kind
  return item
end

function M.new(id, object)
  local scene = composer.getScene("scene.game")
  --choose item to drop
  id = id or "item"
  print("Looking for:", id)
  local item, floor = {}, settings.floor
  if id == "item" or id == "gear" or id == "noJunk" then
    item = M.get(floor, id)
  else
    item = itemData:find(id)
  end

  -- visual
  local instance = display.newGroup()

  if item.kind == "gear" then
    instance.sprite = display.newImage(instance, gearSheet, item.icon)
  elseif item.icon == "coin" then
    instance.sprite = display.newSprite(instance, coinSheet, sequences)
    instance.sprite:setSequence("spin")
    instance.sprite:play()
  else
    instance.sprite = display.newImage(instance, itemSheet, item.icon)
  end

  --shadow
  local shadow = display.newImage(instance, "gfx/shadow.png")
  shadow.xScale, shadow.yScale = 0.75, 0.75
  fx.breath(shadow)
  shadow.alpha = 0.5

  object.parent:insert(instance)
  instance:toBack()
  instance.x, instance.y = object.x, object.y

  function instance:drop()
    local strength = 16
    local angle = math.rad(math.random(0,360))
    local dx, dy = math.cos(angle) * strength, math.sin(angle) * strength
    local sprite = self.sprite
    sprite.y = -24
    local function float()
      fx.float(sprite)
    end
    transition.to(instance, { x=self.x + dx, y=self.y + dy, transition=easing.outQuad, time=133 })
    transition.to(sprite, { delay = 133, y=-8,  transition=easing.outBounce, time=500, onComplete = float })
  end
  instance:drop()

  instance.frameCount = 0
  local function enterFrame()
    -- shadow
    shadow.isVisible = instance.isVisible

    -- Do this every frame
    instance.frameCount = instance.frameCount + 1

    -- Move toward hero unless you are gear
    local dx, dy = instance.x - scene.hero.x, instance.y - scene.hero.y
    if len(dx, dy) <= distanceToHero then
      instance.x, instance.y = (instance.x + scene.hero.x) / 2, (instance.y + scene.hero.y) / 2
    end

    -- if you are within 8px pickup
    if len(dx, dy) < 12 and instance.frameCount > 66 then
      scene.hero:execute("item", { data = item })
      display.remove(instance)
    end
  end

  function instance:finalize()
    -- On remove, cleanup instance, or call directly for non-visual
    display.remove(self.shadow)
    Runtime:removeEventListener("enterFrame", enterFrame)
  end

  -- Add a finalize listener (for display objects only, comment out for non-visual)
  instance:addEventListener("finalize")

  -- Add our enterFrame listener
  Runtime:addEventListener("enterFrame", enterFrame)

  return instance
end

-- what various item types do
function M.action(action, data)
  local scene = composer.getScene("scene.game")
  local world = scene.world
  local hero = scene.hero

  print("Hero action:", action)
  if action == "heal" then
    hero:heal(1)
  elseif action == "health" then
    manager.add("health")
  elseif action == "mana" then
    manager.add("mana")
  elseif action == "food" then
    manager.add("food")
  elseif action == "cure" then
    if hero.isPoisoned then
      hero:execute("cure")
    else -- drop
      Runtime:dispatchEvent({name = "status", text = "You don't need to be cured!"})
    end
  elseif action == "poison" then
    hero:execute("poison")
  elseif action == "xp" then
    hero:addXP()
  elseif action == "coin" then
    hero:execute("coin")
  elseif action == "junk" then
    Runtime:dispatchEvent({name = "status", text = "Worthless junk"})
  end

end

return M